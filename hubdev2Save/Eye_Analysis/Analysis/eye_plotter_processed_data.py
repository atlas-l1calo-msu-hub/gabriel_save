# -*- coding: utf-8 -*-
"""
Author: Gabriel Moreau
Inputs:
    Eye diagram data as csv file, as provided by Pawel and aquired using Xilinx
    ILA Diagnostic Component.
    The collumns in the input file are described by the following header:
    Sample in Buffer	Sample in Window	TRIGGER	state	drpAddr	samples	errors	horz	vert

    add date and data to pdf 
    
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import csv
import datetime
from fpdf import FPDF

# Read data file and store data in list
raw_data = []
eye_data = []
processed_data = []
time = [-32, -28, -24, -20, -16, -12, -8, -4, 0, 4, 8, 12, 16, 20, 24, 28, 32]
temp_data = []
voltage = []
count = 0
error = 0
bus_width = 40 # Form Pawel
#pre_scale = 4
ber = 0

input_file_name = "Data_sets/HTM3/HTM3_prescale0_RX9_run3.csv"

#
# Intake data
with open(input_file_name, newline='') as csvfile:
    reader = csv.reader(csvfile) # change contents to floats
    for row in reader: # each row is a list
        raw_data.append(row)

voltage_position_counter = 0
position_counter = 0
# Process data
for i in range(len(raw_data)-1):
    temp = [] # clear list
    for k in range(10):
        s = raw_data[i][k]
        if k <= 2: # These values are already in decimal
            temp.append(int(s))
        if k > 2: # Convert these hex values to decimal
            temp.append(int(s,16))
    if temp[4] == 335: # 0x14f was identified as being the drp address coresponding
        # to a fully updated sample, 337 for a ROD 
        if temp[7] >= 2**10: # 10 bit two's complement conversions
            temp[7] -= 2**11
        if temp[8] >= 127:
            temp[8] = -1*(temp[8]-128)
        processed_data.append(temp)
        pre_scale = temp[9]
        count = temp[5]*bus_width*2**(1+pre_scale) # Bus width = 40; prescale = 4
        error = temp[6]
        # Comment out the following two lines to not replace the data points 0 with
        # a minimum BER of 1/maximum counts
        if error == 0.0:
            error = 1.0
        position_counter += 1
        ber = error/count
        temp_data.append(ber)
        if position_counter == 17:
            voltage_position_counter += 1
            if voltage_position_counter == 3:
                voltage.append(temp[8])
                voltage_position_counter = 0
            else:
                voltage.append("")
            position_counter = 0
            temp_data.reverse()
            eye_data.append(temp_data)
            #print(temp_data,"\n",row,"\n\n\n")
            temp_data = []

voltage[0] = 124
curr_time = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
out_file_name = "data_processed_" + curr_time + ".csv"
with open(out_file_name,'w', newline='') as newcsv:
    csvwriter = csv.writer(newcsv)
    #csvwriter.writerow(time)
    csvwriter.writerows(eye_data)



# Plot heat map of data
fig, ax = plt.subplots(figsize=(15,5))
color_map = "jet"
im = ax.imshow(eye_data, aspect=0.1,cmap=plt.cm.get_cmap(color_map),norm=matplotlib.colors.LogNorm())

ax.set_xticks(np.arange(len(time)))
ax.set_yticks(np.arange(len(voltage)))

ax.set_xticklabels(time)
ax.set_yticklabels(voltage)
#ax.yaxis.set_major_locator(plt.FixedLocator())

ax.set_xlabel("Time (Unit Interval)")
ax.set_ylabel("Voltage (Codes)")
ax.set_title("Eye Diagram")
plt.colorbar(im)
plt.tight_layout()
plt.savefig("eye_diagram.png")
plt.show()





# Variable definitions
threshold = 1.92*10**-7 # Threshold value for counting a value Bit Error Rate as satisfactory
opening_counter = 0
neighbor_counter = 0
horizontal_counter = 0
vertical_counter = 0
horizontal_list = [] # Store all calculated values to isolate maximum
vertical_list = [] # Same as above for vertical opening


data = eye_data
# Calculate open area
for i in range(len(data)):
    for j in range(len(data[0])):
        if data[i][j] <= threshold:
            opening_counter += 16
            # Check that neighboring cells are bellow the threshold as well
            # Assume that we are far enough from edges to add/substract 1 from index
            # and still be within range (this should be true for any sensible eye diagram)


# Calculate Horizontal Opening
for i in range(len(voltage)):
    horizontal_counter = 0
    for j in range(len(time)):
        if data[i][j] <= threshold:
            if ( (data[i-1][j] <= threshold) and (data[i+1][j])):
                horizontal_counter += 4 # The horizontal increment is 4 units
            # 4*4 = 16 units^2
        horizontal_list.append(horizontal_counter)
        
# Calculate vertical opening
# There is a horizontal width requirement for cells to count, 3 for full count, 2 for half count
for i in range(len(time)):
    vertical_counter = 0
    for j in range(len(voltage)):
        if data[j][i] <= threshold:
            # Check that cell left and right of this one are also valid
            if ( (data[j][i-1] <= threshold) and (data[j][i+1] <= threshold) ):
                vertical_counter += 4
        vertical_list.append(vertical_counter)
# Report largest value found for horizontal and vertical openings
horizontal_max = max(horizontal_list)
vertical_max = max(vertical_list)

area_total = 17*63*16 # Number of squares times area of a square
width_total = 17*4 # Number of time intervals times horizontal increment 
height_total = 63*4 # Number of voltage intervals times vertical increment
opening_percentage = opening_counter/area_total*100
horizontal_percentage = horizontal_max/width_total*100
vertical_percentage = vertical_max/height_total*100


# Output
print("Open Area: %s" % opening_counter)
print("Number of Cells: %i" %(int(opening_counter/16)))
print("Open Area Percentage: %2.2f %s " % (opening_percentage, "%"))
print("Horizontal Opening: %s  " % (horizontal_max))
print("Horizontal Percentage: %2.2f %s " % (horizontal_percentage, "%"))
print("Vertical Opening: %s  " % (vertical_max))
print("Vertical Percentage: %2.2f %s " % (vertical_percentage, "%"))


# Write to pdf
pdf = FPDF()
pdf.add_page()
pdf.set_font('Times', 'B', 16)
date = "date : " + datetime.datetime.now().strftime("%Y/%m/%d")
time_out = "time :" + datetime.datetime.now().strftime("%H:%M:%S")
title = "Eye Diagram Analysis"
pdf.cell(190, 10, title, 1, 0, 'C')
pdf.ln(10)
pdf.set_font('Times', '', 12)
pdf.cell(0, 10, date)
pdf.ln(5)
pdf.cell(0,10, time_out)
pdf.ln(5)
pdf.cell(0,10,"Raw data input file name: " + input_file_name)
pdf.ln(5)
pdf.cell(0,10,"Processed data output file name: " + out_file_name)
pdf.ln(5)
pdf.cell(0,10,"Open area: " + str(opening_counter))
pdf.ln(5)
pdf.cell(0,10,"Open area percentage: %2.2f %s " % (opening_percentage, "%"))
pdf.ln(5)
pdf.cell(0,10,"Horizontal opening: " + str(horizontal_max))
pdf.ln(5)
pdf.cell(0,10,"Horizontal opening percentage: %2.2f %s " % (horizontal_percentage, "%"))
pdf.ln(5)
pdf.cell(0,10,"Vertical opening: %s  " % (vertical_max))
pdf.ln(5)
pdf.cell(0,10,"Vertical opening percentage: %2.2f %s " % (vertical_percentage, "%"))
pdf.ln(5)
pdf.image('eye_diagram.png', 10, 75, 200)
analysis_file_name = "eye_analysis_"+curr_time+".pdf"
pdf.output(analysis_file_name, 'F')

print("\nData output file: " + out_file_name)
print("Analysis output file: "+ analysis_file_name)