# -*- coding: utf-8 -*-
"""
Author: Gabriel Moreau
Inputs:
    Eye diagram data as csv file, as provided by Pawel and aquired using Xilinx
    ILA Diagnostic Component.
    The collumns in the input file are described by the following header:
    Sample in Buffer	Sample in Window	TRIGGER	state	drpAddr	samples	errors	horz	vert

    add date and data to pdf 
    
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import csv
import datetime
from fpdf import FPDF


output_file_name_pdf = "Data_comp/HTM3/custom_app_HTM3_RX9.pdf"
input_file_name_base = "Data_sets/HTM3/HTM3_RX9_prescale0_run"


time = [-32, -28, -24, -20, -16, -12, -8, -4, 0, 4, 8, 12, 16, 20, 24, 28, 32]
voltage = [124, ' ', ' ', 112, ' ', ' ', 100, ' ', ' ', 88, ' ', ' ', 76, ' ', ' ', 64, ' ', ' ', 52, 
            ' ', ' ', 40, ' ', ' ', 28, ' ', ' ', 16, ' ', ' ', 4, ' ',' ', -8, ' ', ' ', -20, ' ', ' ', 
           -32, ' ', ' ', -44, ' ', ' ', -56, ' ', ' ', -68, ' ', ' ', -80, ' ', ' ', -92, 
           ' ', -104, ' ', ' ', -116, ' ', -124]


for l in range(1,4):
    print(l)
    # Read data file and store data in list
    raw_data = []
    eye_data = []
    processed_data = []
    temp_data = []
    count = 0
    error = 0
    bus_width = 40 # Form Pawel
    #pre_scale = 4
    ber = 0
    
    input_file_name = input_file_name_base + str(l) + ".csv"
    # Intake data
    with open(input_file_name, newline='') as csvfile:
        reader = csv.reader(csvfile) # change contents to floats
        for row in reader: # each row is a list
            raw_data.append(row)
    
    voltage_position_counter = 0
    position_counter = 0
    # Process data
    for i in range(len(raw_data)-1):
        temp = [] # clear list
        for k in range(10):
            s = raw_data[i][k]
            if k <= 2: # These values are already in decimal
                temp.append(int(s))
            if k > 2: # Convert these hex values to decimal
                temp.append(int(s,16))
        if temp[4] == 335: # 0x14f was identified as being the drp address coresponding
            # to a fully updated sample, 337 for a ROD 
            if temp[7] >= 2**10: # 10 bit two's complement conversions
                temp[7] -= 2**11
            if temp[8] >= 127:
                temp[8] = -1*(temp[8]-128)
            processed_data.append(temp)
            pre_scale = temp[9]
            count = temp[5]*bus_width*2**(1+pre_scale) # Bus width = 40; prescale = 4
            error = temp[6]
            # Comment out the following two lines to not replace the data points 0 with
            # a minimum BER of 1/maximum counts
            if error == 0.0:
                error = 1.0
            position_counter += 1
            ber = error/count
            temp_data.append(ber)
            if position_counter == 17:
                voltage_position_counter += 1
                if voltage_position_counter == 3:
                #    voltage.append(temp[8])
                    voltage_position_counter = 0
                #else:
                #    voltage.append("")
                position_counter = 0
                temp_data.reverse()
                eye_data.append(temp_data)
                #print(temp_data,"\n",row,"\n\n\n")
                temp_data = []
    
    curr_time = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    out_file_name = "data_processed_" + curr_time + ".csv"
    with open(out_file_name,'w', newline='') as newcsv:
        csvwriter = csv.writer(newcsv)
        #csvwriter.writerow(time)
        csvwriter.writerows(eye_data)
    
    # Plot heat map of data
    fig, ax = plt.subplots(figsize=(15,5))
    color_map = "jet"
    im = ax.imshow(eye_data, aspect=0.1,cmap=plt.cm.get_cmap(color_map),norm=matplotlib.colors.LogNorm())
    
    ax.set_xticks(np.arange(len(time)))
    ax.set_yticks(np.arange(len(voltage)))
    
    ax.set_xticklabels(time)
    ax.set_yticklabels(voltage)
    #ax.yaxis.set_major_locator(plt.FixedLocator())
    
    ax.set_xlabel("Time (Unit Interval)")
    ax.set_ylabel("Voltage (Codes)")
    ax.set_title("Eye Diagram Run " + str(l))
    plt.colorbar(im)
    plt.tight_layout()
    plot_name = "eye_diagram" + str(l) + ".png"
    plt.savefig(plot_name)
    plt.show()
    print(l)

print(voltage)
# Write to pdf
pdf = FPDF()
pdf.add_page()
pdf.set_font('Times', 'B', 16)
date = "date : " + datetime.datetime.now().strftime("%Y/%m/%d")
time_out = "time :" + datetime.datetime.now().strftime("%H:%M:%S")
title = "Eye Diagram Analysis"
pdf.cell(190, 10, title, 1, 0, 'C')
pdf.ln(10)
pdf.set_font('Times', '', 12)
pdf.cell(0, 10, date)
pdf.ln(5)
pdf.cell(0,10, time_out)
pdf.ln(5)
pdf.image('eye_diagram1.png', 10, 60, 200)
pdf.ln(5)
pdf.image('eye_diagram2.png', 10, 130, 200)
pdf.ln(5)
pdf.image('eye_diagram3.png', 10, 200, 200)
pdf.ln(5)
pdf.cell(0,10,"Raw data input file name: " + input_file_name)


#analysis_file_name_pdf = "Analyses/eye_analysis_"+curr_time+".pdf"
pdf.output(output_file_name_pdf, 'F')


print("\nInput File Name : " + input_file_name)
print("Plot output file name PDF: "+ output_file_name_pdf)

