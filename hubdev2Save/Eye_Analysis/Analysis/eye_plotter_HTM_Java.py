# -*- coding: utf-8 -*-
"""
Author: Gabriel Moreau
Inputs:
    Eye diagram data as csv file, as provided by Pawel and aquired using Xilinx
    ILA Diagnostic Component.
    The collumns in the input file are described by the following header:
    Sample in Buffer	Sample in Window	TRIGGER	state	drpAddr	samples	errors	horz	vert

    add date and data to pdf 
    
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import csv
import datetime
from fpdf import FPDF


output_file_name_pdf = "Data_comp/HTM14/java_app_HTM14_RX9.pdf"
input_file_name_base = "Data_sets/HTM14/HTM14_Eye_Scan_RX_9_run"

time = [-32, -28, -24, -20, -16, -12, -8, -4, 0, 4, 8, 12, 16, 20, 24, 28, 32]
voltage = [124, ' ', ' ', 112, ' ', ' ', 100, ' ', ' ', 88, ' ', ' ', 76, ' ', ' ', 64, ' ', ' ', 52, 
            ' ', ' ', 40, ' ', ' ', 28, ' ', ' ', 16, ' ', ' ', 4, ' ',' ', -8, ' ', ' ', -20, ' ', ' ', 
           -32, ' ', ' ', -44, ' ', ' ', -56, ' ', ' ', -68, ' ', ' ', -80, ' ', ' ', -92, 
           ' ', ' ', -104, ' ', ' ', -116, ' ', -124]

eye_data = []

for l in range(1,4):
    # Read data file and store data in list
    
    #curr_time = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    #out_file_name = "data_processed_" + curr_time + ".csv"
    input_file_name = input_file_name_base + str(l) + ".csv"
    print(input_file_name)
    with open(input_file_name, newline='') as csvfile:
        reader = csv.reader(csvfile) # change contents to floats
        line_counter = 1
        for row in reader: # each row is a list
            if( (line_counter >= 23) and (line_counter < 86) ):
                temp_list = list(map(float, row))
                eye_data.append(temp_list[1:])
            line_counter += 1
    # Plot heat map of data
    fig, ax = plt.subplots(figsize=(15,5))
    color_map = "jet"
    im = ax.imshow(eye_data, aspect=0.1,cmap=plt.cm.get_cmap(color_map),norm=matplotlib.colors.LogNorm())
    
    ax.set_xticks(np.arange(len(time)))
    ax.set_yticks(np.arange(len(voltage)))
    
    ax.set_xticklabels(time)
    ax.set_yticklabels(voltage)
    #ax.yaxis.set_major_locator(plt.FixedLocator())
    
    ax.set_xlabel("Time (Unit Interval)")
    ax.set_ylabel("Voltage (Codes)")
    ax.set_title("Eye Diagram Run " + str(l))
    plt.colorbar(im)
    plt.tight_layout()
    plot_name = "eye_diagram" + str(l) + ".png"
    plt.savefig(plot_name)
    plt.show()
    eye_data = []


# Write to pdf
pdf = FPDF()
pdf.add_page()
pdf.set_font('Times', 'B', 16)
date = "date : " + datetime.datetime.now().strftime("%Y/%m/%d")
time_out = "time :" + datetime.datetime.now().strftime("%H:%M:%S")
title = "Eye Diagram Analysis"
pdf.cell(190, 10, title, 1, 0, 'C')
pdf.ln(10)
pdf.set_font('Times', '', 12)
pdf.cell(0, 10, date)
pdf.ln(5)
pdf.cell(0,10, time_out)
pdf.ln(5)
pdf.image('eye_diagram1.png', 10, 60, 200)
pdf.ln(5)
pdf.image('eye_diagram2.png', 10, 130, 200)
pdf.ln(5)
pdf.image('eye_diagram3.png', 10, 200, 200)
pdf.ln(5)
pdf.cell(0,10,"Raw data input file name: " + input_file_name)


#analysis_file_name_pdf = "Analyses/eye_analysis_"+curr_time+".pdf"
pdf.output(output_file_name_pdf, 'F')


print("\nInput File Name : " + input_file_name)
print("Plot output file name PDF: "+ output_file_name_pdf)

