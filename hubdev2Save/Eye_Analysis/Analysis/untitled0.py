# -*- coding: utf-8 -*-
"""
Created on Sun Sep 20 12:03:53 2020

@author: gabri
"""

import numpy as np
import matplotlib.pyplot as plt



counter = 0
fig, axs = plt.subplots(4,figsize = (5,15))
for n_list in (10,1000,100000):
    x = [0]
    y = [0]
    for n in range(n_list):
        random_dir = np.random.randint(4)
        if random_dir == 0:
            x.append(x[-1]+1)
            y.append(y[-1])
        if random_dir == 1:
            x.append(x[-1]-1)
            y.append(y[-1])
        if random_dir == 2:
            x.append(x[-1])
            y.append(y[-1]+1)
        if random_dir == 3:
            x.append(x[-1])
            y.append(y[-1]-1)

    axs[counter].plot(x,y)
    axs[counter].set_title("Random walk with %d steps" % (n+1))
    counter += 1

x_list = []
y_list = []
for i in range(1000):
    x = [0]
    y = [0]
    for n in range(1000):
        random_dir = np.random.randint(4)
        if random_dir == 0:
            x.append(x[-1]+1)
            y.append(y[-1])
        if random_dir == 1:
            x.append(x[-1]-1)
            y.append(y[-1])
        if random_dir == 2:
            x.append(x[-1])
            y.append(y[-1]+1)
        if random_dir == 3:
            x.append(x[-1])
            y.append(y[-1]-1)
    x_list.append(x[-1])
    y_list.append(y[-1])
axs[3].set_title("Random walk end points")
axs[3].plot(x_list,y_list)
