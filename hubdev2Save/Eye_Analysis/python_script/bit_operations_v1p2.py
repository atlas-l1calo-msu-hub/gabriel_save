"""
Initial Rev: 22 May 2019
Current Rev:  26 June 2019

-Changed bitfield index. Now bit 0 is the final bit of the sequence
Added blip method for raising and lowering a bit
To add next: group subclass for mgt specifically that will set up bitfields
accordingly and have methods at the group level for common actions (i.e. mgt
reset, prbssel, etc)
"""

import time
import warnings


def bin_to_hex(binary_string):
    """
    Return : Hexadecimal string representation of given binary string
    """

    return hex(int(binary_string, 2)) 


def hex_to_bin(hex_string):
    """
    Return : Binary string representation of given hexadecimal string
    """
    return bin(int(hex_string, 16))


def to_binary(value):
    """
    Attempts to convert given value to a binary string.
    Works without issue if given value is a binary string or a hexadecimal
    string. If input is an int, long, or some non hex/binary string, it is
    treated as a decimal.
    Return : binary string representation of given value
    """

    if type(value) is not str:
        warnings.warn("Value '%s' is not of type 'str'."%(value) + 
                          "Converting to 'str'")
        value = str(value)

    if (len(value) < 2) or ((value[:2] != "0x") and (value[:2] != "0b")):

        warnings.warn("Value '%s' does not have an explicit "%(value) +
                          "base prefix '0b' or '0x'. Treating as decimal.")
        if not (u"%s"%(value)).isnumeric():
            raise ValueError("Failed to convert '%s' to binary."%(value))
        
        value_bin = bin(long(value))[2:]

    elif value[:2] == "0x":
        value_bin = hex_to_bin(value)[2:]

    elif value[:2] == "0b":
        value_bin = value[2:]
    else:
        raise ValueError("Failed to convert '%s' to binary."%(value))

    return "0b" + value_bin

def to_hex(value):

    binary_value = to_binary(value)
    decimal_value = int(binary_value, 2)
    return hex(decimal_value)

class Bitfield:

    def __init__(self, name, startbit, width, status="RW", register=None):
        """
        Object constructor. Bitfield objects should only be constructed using
        the add_bitfield method of the Register class.
        name     : name of bitfield
        startbit : starting bit of bitfield in parent 32-bit register
        width    : how many bits long the bitfield is
        status   : Read/write status of bitfield. Should be 'R', 'W', or 'RW'
        """        

        self.name = name
        self.startbit = startbit
        self.width = width
        self.status = status
        self.register = register
        self.hm = None


    def __str__(self):
        """
        Return : Name/Path of bitfield (i.e. 'device.group.register.bitfield') 
        """

        return "%s.%s"%(self.register, self.name)


    def __len__(self):
        """
        Return : width of bitfield in bits
        """

        return self.width

    def read(self, update_expected = True):
        """
        Read value of bitfield from register. If update_expected == true, also
        update expected value of bit field in parent register.
        Return : binary string of bitfield value i.e. '0b1101'
        """

        if "R" not in self.status.upper():
			raise TypeError("Cannot read from write-only bitfield '%s'."%self.name)

        reg_value_read_hex = self.register.read()

        reg_value_read_bin = to_binary(reg_value_read_hex)[2:].zfill(32)
        bitfield_value_read_bin = reg_value_read_bin[32-(self.startbit+self.width):
                                                     32-self.startbit]#

        if update_expected == True:
            self.update_expected("0b" + bitfield_value_read_bin)

        return "0b" + bitfield_value_read_bin
        
        
    def write(self, value, update_expected = True):
        """
        Write value to bitfield.
        value  : value to write to bitfield. Should be a binary or hexadecimal
                 string
        return : binary string representation of value written to bitfield
        """

        if "W" not in self.status.upper():
			raise TypeError("Cannot write to read-only bitfield '%s'."%self)

        value_bin = to_binary(value)[2:].zfill(self.width)

        if len(value_bin) > self.width:
            raise ValueError("Cannot write value '%s' to"%("0b"+value_bin) + 
                             "bitfield '%s' of width '%s'."%(self, self.width))

        startbit = self.startbit
        width = self.width

        reg_value_hex = self.register.read()
        reg_value_bin = to_binary(reg_value_hex)[2:].zfill(32)
        write_reg_value_bin = ("0b" + reg_value_bin[:32-(startbit+width)] 
                               + value_bin + reg_value_bin[32-startbit:])#

        self.register.write(write_reg_value_bin, update_expected = update_expected)	

        # Unsure whether I should leave this and update function as is, so that
        # only the register part is updated, or if I should update all write bits
        # in the expected value, since I do write something to the whole register.
        if update_expected == True:
            self.update_expected("0b" + value_bin, "W")

        return "0b" + write_reg_value_bin[2:][32-(startbit+width):32-startbit]#


    def write_plus(self, write_value, no_of_attempts = 5, update_expected = True):
        # Update Description
        """
        Write value to register, read back value, check if the value values
        read and written are the same. If not, try again until no_of_attempts
        is exceeded.
        value  : value to write to bitfield. Should be a binary or hexadecimal
                 string (binary is assumed if no prefix is given in string)
        return : binary string representation of value written (with correct width)
        """

        write_value_bin = to_binary(write_value)
        self.write(write_value_bin, update_expected)
        read_value_bin = to_binary(self.read(update_expected))
        attempts = 0
        
        if update_expected:
            while ( (not self.compare_to_exp(write_value_bin)) 
                                     and attempts < no_of_attempts):

                self.write(write_value, update_expected)
                self.read(update_expected)

                attempts += 1


            if not self.compare_to_exp(write_value_bin):     
                raise RuntimeError("Value '%s' was not succesfully "%(write_value) +
                                   "written to register '%s'. Expected "%(self) + 
                                   "value based on reads is '%s'."%(self.expected_value()))

        else:
            while (read_value_bin != write_value_bin) and attempts < no_of_attempts:
                write_value_bin = self.write(write_value_bin, update_expected)
                read_value_bin = to_binary(self.read(update_expected))
                attempts += 1
        
            if read_value_bin != attempted_write_value:
                raise RuntimeError("Value '%s' was not succesfully "%(write_value) +
                                   "written to register '%s'. Last value"%(self) + 
                                   "read was '%s'."%(read_value_bin))
        
        return write_value_bin


    def blip(self, wait_time = 0.25, update_expected = True):
        """
        Write all 0s then all 1s then all 0s to register.
        wait_time   : time in seconds to wait between writes
        return : tuple containing the values of the register after writing 0s 1s and 0s
                 (in that order)  
        """


        self.write("0b" + "0"*self.width, update_expected)
        time.sleep(wait_time)
        init = self.read()
        self.write("0b" + "1"*self.width, update_expected)
        time.sleep(wait_time)
        middle = self.read()
        self.write("0b" + "0"*self.width, update_expected)
        time.sleep(wait_time)
        final = self.read()

        return (init, middle, final)


    def update_expected(self, value, status="RW"):
        """
        Update expected value of register to given value
        value : new expected value of register (bin or hex string)
        """

        value_bin = to_binary(value)[2:].zfill(self.width)
        if len(value_bin) > self.width:
            raise ValueError("Cannot update expected value of bitfield '%s' "%(self) +
                             "of width '%s' to '%s'."%(self.width, "0b"+value))



        for index, char in enumerate(value_bin):
            if "R" in status.upper():
                if self.register.read_mask()[32-(self.startbit+self.width)+index] == "1":
                    self.register.exp_value[32-(self.startbit+self.width)+index] = char#
            if "W" in status.upper():
                if self.register.write_mask()[32-(self.startbit+self.width)+index] == "1":
                    self.register.exp_value[32-(self.startbit+self.width)+index] = char#


    def expected_value(self):
        """
        Get expected value of bitfield (based on previous writes and reads)
        Return: binary string of expected bitfield value i.e. '0b1101'
        """

        exp_reg_value_hex = self.register.expected_value()

        exp_reg_value_bin = to_binary(exp_reg_value_hex)[2:].zfill(32)

        exp_bitfield_value_bin = exp_reg_value_bin[32-(self.startbit+self.width):
                                                   32-self.startbit]#

        return "0b" + exp_bitfield_value_bin


    def compare_to_exp(self, value):
        """
        Compares given value to the readable portion of the registers expected
        value.
        return : True if readable bits of expected value match those of given
                 value, False otherwise.
        """

        exp = int(to_binary(self.expected_value()), 2)
        value_bin = int(to_binary(value), 2)

        return exp == value_bin


class Register:
    """
    
    """

    def __init__(self, name, group, status = "RW"):
        """
        Class constructor. Register objects should only be constructed using
        the add_register method of the Group class.
        name : name of register (should match the id of register node in XML file)
        """

        self.exp_value = ['0']*32 
        self.group = group
        self.name = name
        self.hm = group.hm
        self.status = status
        self.bitfield_dict = {}

    def __getitem__(self, key):
        """
        Return : bitfield object of register with given name (key).
        """
        if key not in self.bitfield_dict:
            raise KeyError("Register does not contain bitfield '%s'"%key)

        return self.bitfield_dict[key]


    def __iter__(self):
        """
        Return : iterable over bitfields in register (sorted by startbit)
        """

        bitfield_list = []

        for key in self.bitfield_dict:
            bitfield_list.append(self[key])
        
        bitfield_list.sort(key=lambda bitfield: bitfield.startbit)
        
        for bitfield in bitfield_list:
            yield bitfield


    def __str__(self):
        """
        Return : Name/Path of register (i.e. 'device.group.register') 
        """

        return "%s.%s"%(self.group, self.name)


    def read(self, update_expected = True):
        """
        Read value of 32-bit register. If update_expected == true, also
        update expected value of register.
        Return : hexadecimal string of register value i.e. '0x0FFA57BE'
        """
        
        try:
            value_read = self.get_uhal().read()
            self.hm.dispatch()

            value_read_hex = hex(value_read)

        except:
            raise UhalReadError("Error while reading from register '%s'."%(self))

        if update_expected == True:
            self.update_expected(value_read_hex, "R")

        return "0x" + value_read_hex[2:].zfill(8)



    def write(self, value, update_expected = True):
        """
        Write value to register.
        value  : value to write to bitfield. Should be a binary or hexadecimal
                 string (binary is assumed if no prefix is given in string)
        return : binary string representation of value written (with correct width)
        """
        value_bin = to_binary(value)[2:].zfill(32)

        for bitfield in self:
            startbit = bitfield.startbit
            width = bitfield.width

            read_bin_value = bitfield.read()
            write_bin_value = "0b" + value_bin[32-(startbit+width):32-startbit]#

            if "W" not in bitfield.status:
                if bitfield.read() != write_bin_value:
                    raise ValueError("Cannot write '%s' to read-only bitfield '%s' with value '%s'"
                                     %(write_bin_value, bitfield, read_bin_value))

        try:
            self.get_uhal().write(int(value_bin, 2))
            self.hm.dispatch()

        except Exception as e:
            raise UhalWriteError("Error while writing to register '%s':"%(self) + str(e))

        if update_expected == True:
            self.update_expected("0b" + value_bin, "W")

        return "0b" + value_bin



    def write_plus(self, write_value, no_of_attempts = 2, update_expected = True):
        """
        Write value to register, read back value, check if the value values
        read and written are the same. If not, try again until no_of_attempts
        is exceeded.
        value  : value to write to bitfield. Should be a binary or hexadecimal
                 string (binary is assumed if no prefix is given in string)
        return : binary string representation of value written (with correct width)
        """

        write_value_bin = to_binary(write_value)
        self.write(write_value_bin, update_expected)
        read_value_bin = to_binary(self.read(update_expected))
        attempts = 1    
        
        if update_expected:
            while ( (not self.compare_to_exp(write_value_bin, "W")) 
                                     and attempts < no_of_attempts):    
                self.write(write_value, update_expected)
                self.read(update_expected)
                attempts += 1
    
            if not self.compare_to_exp(write_value_bin, "W"):     
                raise RuntimeError("Value '%s' was not succesfully "%(write_value) +
                                   "written to register '%s'."%(self))

        else:
            while (read_value_bin != write_value_bin) and attempts < no_of_attempts:
                write_value_bin = self.write(write_value_bin, update_expected)
                read_value_bin = to_binary(self.read(update_expected))
                attempts += 1
        
            if read_value != attempted_write_value:
                raise RuntimeError("Value '%s' was not succesfully "%(write_value) +
                                   "written to register '%s'."%(self))
        
        return write_value_bin

        
    def add_bitfield(self, name, startbit, width, status="RW"):
        """
        Define a bitfield within the register with the given startbit and width.
        name     : name of bitfield
        startbit : starting position of bitfield in register
        width    : how many bits wide the register is
        status   : Read/Write status of the bitfield. Should be 'R', 'W', or 'RW'. 
        """

        if width < 1:
            raise ValueError("Width must be a positive integer")

        endbit = startbit + width - 1

        if (startbit < 0) or (endbit > 31):
            raise IndexError("Bitfield is out of bounds of 32-bit register.") 

        for bitfield in self:
            field_startbit = bitfield.startbit
            field_endbit = field_startbit + bitfield.width - 1
            field_name = bitfield.name

            if (((field_startbit <= startbit) and (startbit <= field_endbit)) or
                ((field_startbit <= endbit) and (endbit <= field_endbit))):
                
                raise ValueError("Bitfield '%s' overlaps with existing bitfield '%s'"
                                 %(name, field_name))

        self.bitfield_dict[name] = Bitfield(name, startbit, width, status, self)


    def update_expected(self, value, status="RW"):
        """
        Update expected value of register to given value
        value  : new expected value of register (bin or hex string)
        status : 
        """

        value_bin = to_binary(value)[2:].zfill(32)

        for index, char in enumerate(value_bin):
            if "R" in status.upper():
                if self.read_mask()[index] == "1":
                    self.exp_value[index] = char
            if "W" in status.upper():
                if self.write_mask()[index] == "1":
                    self.exp_value[index] = char


    def expected_value(self):
        """
        Get expected value of register (based on previous writes and reads)
        Return: hexadecimal string of expected register value i.e. '0x0FFA57BE'
        """ 

        return "0x" + hex(int("".join(self.exp_value), 2))[2:].zfill(8)

    def read_mask(self):
        """
        Return : Read mask of register. '1' indicates bit can be read from,
                 '0' indicates bit cannot be read from, and '-' indicates
                 undefined (no bitfield is defined there)
        """

        if (len(self.bitfield_dict) < 1) and ("R" in self.status.upper()):
            return '1'*32

        mask = ['-']*32
        
        for bitfield in self:
            startbit = bitfield.startbit
            width = bitfield.width
            if "R" in bitfield.status:
                mask[32-(startbit+width):32-startbit] = '1'*width#
            else: 
                mask[32-(startbit+width):32-startbit] = '0'*width#

        return "".join(mask)            


    def write_mask(self):
        """
        Return : Write mask of register. '1' indicates bit can be written to,
                 '0' indicates bit cannot be written to, and '-' indicates
                 undefined (no bitfield is defined there). If no bitfields are
                 defined, mask is based on RW status of register.
        """

        if (len(self.bitfield_dict) < 1) and ("W" in self.status.upper()):
            return '1'*32

        mask = ['-']*32
        
        for bitfield in self:

            startbit = bitfield.startbit
            width = bitfield.width
            if "W" in bitfield.status:
                mask[32-(startbit+width):32-startbit] = '1'*width#
            else: 
                mask[32-(startbit+width):32-startbit] = '0'*width#

        return "".join(mask)            


    def compare_to_exp(self, value, status="RW"):
        """
        Compares given value to the readable portion of the registers expected
        value.
        return : True if readable bits of expected value match those of given
                 value, False otherwise.
        """
        exp = to_binary(self.expected_value())
        value_bin = to_binary(value)

        if 'W' in status:
            for index, val in enumerate(self.write_mask()):
                if val == '1':
                    if exp[2:].zfill(32)[index] != value_bin[2:].zfill(32)[index]:
                        return False

        if 'R' in status:
            for index, val in enumerate(self.read_mask()):
                if val == '1':
                    if exp[2:].zfill(32)[index] != value_bin[2:].zfill(32)[index]:
                        return False

        return True


    def get_uhal(self):
        """
        Return : uhal node associated with this register
        """

        return self.group.get_uhal().getNode(self.name)


class Group:
    """

    """

    def __init__(self, name, device):
        """
        Class constructor. Group objects should only be constructed using
        the add_group method of the Device class.
        name : name of group (should match the id of a 'first-layer' node in XML file)
        """

        self.name = name
        self.device = device
        self.register_dict = {}
        self.hm = device.hm
        register_list = self.getNodes()

        for register in register_list:
            self.add_register(register)


    def __getitem__(self, key):
        """
        Return : Register object of group with given name (key).
        """

        if key not in self.register_dict.keys():
            raise KeyError("Group does not contain register '%s'."%key)

        return self.register_dict[key]


    def __iter__(self):
        """
        Return : iterable over registers of group (sorted by address)
        """

        reg_list = []

        for key in self.register_dict:
            reg_list.append(self[key])
        
        reg_list.sort(key=lambda reg: reg.get_uhal().getAddress())
        
        for reg in reg_list:
            yield reg


    def __str__(self):
        """
        Return : Name/Path of group (i.e. 'devicename.groupname') 
        """

        return "%s.%s"%(self.device, self.name)


    def add_register(self, register_name):
        """
        Adds register with given name to dictionary of registers within group
        register_name     : name of register (should match the id of a 'second-layer'
                            node in XML file)
        """

        self.register_dict[register_name] = Register(register_name, self)
        self.register_dict[register_name].group = self
        self.register_dict[register_name].hm = self.hm



    def getNodes(self):
        """
        Return : list of id's of subnodes of the node corresponding to
                 group object
        """

        node_list = self.get_uhal().getNodes()
        self.hm.dispatch()
        
        return node_list


    def get_uhal(self):
        """
        Return : uhal node associated with this group
        """

        return self.device.get_uhal().getNode(self.name)

class MGT_Channel(Group):
    """

    """

    def __init__(self, name, device):
        Group.__init__(self, name, device)

        self["common_loopback"].add_bitfield("loopback", 0, 3, "RW")
    
        ### Currently labeled R in proposal, but I believe should be "RW"
        self["common_reset"].add_bitfield("reset_master", 0, 1, "RW")
    
        self["common_status"].add_bitfield("init_done", 0, 1, "R")
        self["common_status"].add_bitfield("gtpowergood", 4, 1, "R")
        self["common_status"].add_bitfield("init_retry_ctr", 8, 1, "R")


        self["tx_setup"].add_bitfield("txprbssel", 0, 4, "RW")
        ############ Following bitfields are tentative / have uncertain widths
        #device[ch_id]["tx_setup"].add_bitfield("on/off", 0, 0, "RW")
        #device[ch_id]["tx_setup"].add_bitfield("polarity", 0, 0, "RW")
        self["tx_setup"].add_bitfield("swing", 16, 5, "RW")
        self["tx_setup"].add_bitfield("pre-emphasis", 21, 5, "RW")
        self["tx_setup"].add_bitfield("post-emphasis", 26, 5, "RW")
        #device[ch_id]["tx_setup"].add_bitfield("physics_vs_ibert_vs_memory", 0, 0, "RW")

        self["tx_reset"].add_bitfield("reset_tx_pll_and_datapath", 0, 1, "RW")
        self["tx_reset"].add_bitfield("reset_tx_datapath", 4, 1, "RW")

        self["tx_status"].add_bitfield("txpmaresetdone", 0, 1, "R")
        self["tx_status"].add_bitfield("reset_tx_done", 4, 1, "R")
        self["tx_status"].add_bitfield("buffbypass_tx_done", 8, 1, "R")
        self["tx_status"].add_bitfield("buffbypass_tx_error", 12, 1, "R")

        self["tx_force_err"].add_bitfield("txprbsforceerr", 0, 1, "RW")


        self["rx_setup"].add_bitfield("rxprbssel", 0, 4, "RW")
        ############ Following bitfields are tentative / have uncertain widths
        #device[ch_id]["rx_setup"].add_bitfield("polarity", 0, 1, "RW")
        self["rx_setup"].add_bitfield("DFE on/off", 8, 1, "RW")
        #device[ch_id]["rx_setup"].add_bitfield("DFE parameters", 0, 1, "RW")
        #device[ch_id]["rx_setup"].add_bitfield("physics_vs_ibert", 0, 1, "RW")
        #device[ch_id]["rx_setup"].add_bitfield("copy_to_memory", 0, 1, "RW")

        self["rx_reset"].add_bitfield("reset_rx_pll_and_datapath", 0, 1, "RW")
        self["rx_reset"].add_bitfield("reset_rx_datapath", 4, 1, "RW")
        self["rx_reset"].add_bitfield("rxprbscntreset", 8, 1, "RW")

        self["rx_status"].add_bitfield("rxpmaresetdone", 0, 1, "RW")
        self["rx_status"].add_bitfield("reset_rx_done", 4, 1, "RW")
        self["rx_status"].add_bitfield("buffbypass_rx_done", 8, 1, "RW")
        self["rx_status"].add_bitfield("buffbypass_rx_error", 12, 1, "RW")
        self["rx_status"].add_bitfield("rxprbserr_flg", 16, 1, "RW")
        self["rx_status"].add_bitfield("rxprbslocked", 20, 1, "RW")

        self["rx_count_err"].add_bitfield("rxprbserr_cnt", 0, 32, "R")

    def reset(self, wait_time = 0.5):

        return self["common_reset"]["reset_master"].blip(wait_time = wait_time)


    def set_txprbssel(self, value):

        return self["tx_setup"]["txprbssel"].write(value)


    def get_txprbssel(self):

        return self["tx_setup"]["txprbssel"].read()


    def set_rxprbssel(self, value):

        return self["rx_setup"]["rxprbssel"].write(value)

    
    def get_rxprbssel(self):

        return self["rx_setup"]["rxprbssel"].read()


    def get_init_done(self):

        return self["common_status"]["init_done"].read()


    def inject_error(self, wait_time = 0.5):

        return self["tx_force_err"]["txprbsforceerr"].blip(wait_time = wait_time)


    def reset_errors(self, wait_time = 0.5):

        return self["rx_reset"]["rxprbscntreset"].blip(wait_time = wait_time)


    def get_rxprbslocked(self):

        return self["rx_status"]["rxprbslocked"].read()


    def get_error_flag(self):

        return self["rx_status"]["rxprbserr_flg"].read()


    def get_error_count(self):

        return self["rx_count_err"]["rxprbserr_cnt"].read()



class Device:
    """

    """

    def __init__(self, uhal_device):
        """
        uhal_device : a uhal device object 
                      (i.e. uhal.ConnectionManager('filename').getDevice('devicename')
        """

        self.hm = uhal_device
        self.group_dict = {}
        group_list = self.getNodes()

        for group in group_list:
            self.add_group(group)


    def __getitem__(self, key):
        """
        Return : Group object of device with given name (key).
        """

        if key not in self.group_dict.keys():
            raise KeyError("Manager does not contain group '%s'."%key)

        return self.group_dict[key]


    def __iter__(self):
        """
        Return : iterable over groups of Device (sorted by address)
        """

        group_list = []

        for key in self.group_dict:
            group_list.append(self[key])
        
        group_list.sort(key=lambda group: group.get_uhal().getAddress())
        
        for group in group_list:
            yield group


    def __str__(self):
        """
        Return : Name of device
        """
        
        return "%s"%(self.get_uhal().id())


    def add_group(self, group_name):
        """
        Adds group with given name to dictionary of groups within device
        group_name : name of group (should match the id of a 'first-layer'
                                    node in XML file)
        """

        if "ch" in group_name:
            self.group_dict[group_name] = MGT_Channel(group_name, self)
        else:
            self.group_dict[group_name] = Group(group_name, self)
        self.group_dict[group_name].device = self
        self.group_dict[group_name].hm = self.hm

        
    def getNodes(self):
        """
        Return : list of id's of direct subnodes of the node corresponding to
                 device object (direct meaning one layer deep; i.e. not 
                 including any subnodes of subnodes)
        """

        subnode_list = self.get_uhal().getNodes()
        self.get_uhal().dispatch()

        direct_subnode_list = []
        for subnode in subnode_list:
            if '.' not in subnode:
                direct_subnode_list.append(subnode)

        return direct_subnode_list


    def get_uhal(self):
        """
        Return : uhal device associated with this object
        """
        
        return self.hm



class Log:

    def __init__(self, log_name):

        self.log_name = log_name
        self.write_buffer = ""

    def write(self, entry):

        self.write_buffer += entry


    def clear_write_buffer(self):

        self.write_buffer = ""        


    def flush(self, print_log=True):

        if self.write_buffer == "":
            return

        time_str = time.asctime()
        write_buffer_split = self.write_buffer.split("\n")
        write_str = ""
        i = 0
        while i < len(write_buffer_split):

            if i == 0:
                write_str += time_str + ": "
            elif (i == len(write_buffer_split) - 1) and write_buffer_split[i] == "":
                i += 1
                continue
            else:
                write_str += " "*(len(time_str) + 2)


            write_str += write_buffer_split[i] + "\n"
            i += 1

        log = open(self.log_name, "a+")
        log.write(write_str)
        log.close()
        if print_log == True:
            print(write_str)

        self.clear_write_buffer()

class UhalReadError(Exception):
    pass

class UhalWriteError(Exception):
    pass




