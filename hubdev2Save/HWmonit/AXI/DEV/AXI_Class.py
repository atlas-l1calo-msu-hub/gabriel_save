######################################################################
"""
Created: 05/28/2018
Coded by: Gabriel Moreau (moreauga@msu.edu)
Goal: Python class that allows access to the different registers on
      the self.I2C interface on the HUB modules. This module contains
      methods that read or write to the different devices via an I2C bus. The
      writing methods require as an argument the desired written
      value specified as a binary ( 0bXXXXXXXX) or hexadecimal (0xXX)
      number. Last updated: 10/15/2018

Notes:
      Verbosity: Level 0 and 1 = no output; level 2 = print the steps in R/W
      (to) the registers; level 3 = print steps and the analysis of
      the read/writen value(s) when applicable. Default is
      level 0, if an invalid value is received, a level 0 is assumed.

      IPBus: This class uses the IPBus sofware. Users should be sure
      to have correctly set up their program to work with IPBus. An
      IPBus ConnectionManager and Device be set up, and passed as
      arguments to the constructor.

	  PMBus: This class uses the PMBus convention for a transaction

IMPORTANT:
	
	Please read the Xilinx AXI Documentation pg090-axi-iic.pdf before editing this code:
		https://japan.xilinx.com/support/documentation/ip_documentation/axi_iic/v2_0/pg090-axi-iic.pdf


Version 2.0 
Future improvements: -Make more refined address table (similar to philip's)
that will allow bit by bit instead of byte access.
                     -Consider making the R/W methods private
                     -Add double checking mechanisms that verify succesful R/W operations
                     -Add method that configures AXI interface before use
                          instead of doing a soft reset
					 - in method: TX_FIFO_Write figure out why the dispatch() is commented out
"""
######################################################################

from time import sleep
import sys
class AXI_Interface:



	######################################################################
	###### Constructor; arguments: hw = instance of an IPBus ConnectionManager , 
	###### device ID as fount in the IPBus connection file, the I2C address of the
	###### component on the device that is being communicated with, and a verbosity level
	###### The hw argument can be constructed using the following 2 lines:
	###### 
	###### manager = uhal.ConnectionManager("file://ConnHub.xml")
	###### hw = manager.getDevice ( "HUB1" )
	
	###### where ConnHub.xml is an IPBus connection file, and HUB1 is the
	###### device being connected 

	def __init__( self, hw, device,slaveAddress, verbose = 0):
		self.hw = hw
		self.device = device
		self.slaveAddress = slaveAddress
		if verbose < 0:
			print "Invalid verbosity level (%s), cannot be negative, assuming verbosity level desired: 0" % (verbose)
			self.verbose = 0
		elif verbose > 4:
			print "Invalid verbosity level (%s), cannot be greater than 4, assuming verbosity level desired: 4" % (verbose)
			self.verbose = 4
		else:
			self.verbose = verbose
		if self.verbose > 1:
			print " [INFO] Instance of I2Ccontroler created for device {}".format(self.device)
		
		self.logFile = open("LOGFILE.txt", 'a')



	######################################################################
	###### This method performs a read sequence using the Dynamic
	###### Controller Logic Flow as described on p.36 of the Xilinx AXI 
	###### Documentation: https://japan.xilinx.com/support/documentation/ip_documentation/axi_iic/v2_0/pg090-axi-iic.pdf
	###### Arguments: the value
	###### of the Word to be read, the number of bytes to read (msgLen)
	###### and wether this is a random read (randRead = 1) or not
	###### (randRead = 0). Returns list of read values as binary
	###### ints. 

	def Dyn_Read(self, addIn, msgLen,randRead=1):
		# Sanity check of message lenght
		if msgLen < 1 :
			print " ERROR: Cannot read msg of lenght less than 1"
			sys.exit(" 1: Exiting, was in method: Dyn_Read from class: MiniPodControl")

		# Initialize Dynamic IIC access as described in p. 37 of Xilinx doc on the interface
		self.AXI_Init()
		
		# Check AXI controller ready
		i2cStatus = self.Check_i2c_ready()
		if i2cStatus == 1 :
			if self.verbose > 2:
				print " [INFO] AXI ready, Proceeding"
		else:
			print " [ERROR] AXI not ready, exiting"
			sys.exit(" 5: Exiting, was in method: Dyn_Read from class MiniPodControl")


		# Calculate four bytes to transmit, as described in the Xilinx documentation.
		# The first two bytes must be:
		#   - The start bit (0x01) and the I2C address shifted left by one bit and
		# with a 0 in the LSB (to signify a write).
		#   - The desired internal register to read: addIn
		# This performs a "Dummy" Write, a write that selects the desired internal
		# register without writting data to it. 
		# The second two bytes must be:
		#   - The  start bit (0x01) and the I2C address shifted left by one bit
		# and with a 0 in the LSB (to signify a write). This generates a Restart 
		# condition.
		#   - The start bit (0x02) and the message length in number of bytes.
		# This performs a read at the currently selected internal register
		# which was set to the addIn register by the previous dummy write.

		write_data_1 = 0x0100 + (self.slaveAddress << 1)  # Start bit + MiniPod address shifted 1 bit to the left, sets R/W bit to W
		write_data_2 = addIn   # Internal register address for data needed for dummy Write in the case of a random read
		read_data_1 = 0x0101 + (self.slaveAddress << 1)   # Start bit + MiniPod address shifted 1 bit to the left, sets R/W bit to R
		read_data_2 = 0x0200 + msgLen    # Stop bit + no of bytes

		# Wait for first byte to be transmitted before sending the second, repeat for the third
		# Display error message in the event of a timeout.
		
		if randRead: # perform dummy write to select the correct Internal register address
			# Transmit byte 1
			self.TX_FIFO_Write(write_data_1)
			# Transmit byte 2
			self.TX_FIFO_Write(write_data_2)

		# Perform read sequence by sending read bytes 1 and 2
		self.TX_FIFO_Write(read_data_1)
		self.TX_FIFO_Write(read_data_2)

		# Check that the transmit fifo is empty before reading 
		if self.TX_FIFO_EMPTY():
			if self.verbose > 2:
				print " [INFO] TX fifo emptied correctly"
		else:
			self.Soft_Reset()
			raise RuntimeError( " [ERROR] Handling exception, interrupting current run, restarting software.\n Was in method: Dyn_Read from class: Monitor")

		
		read_data = [] # List for storing read data
		
		if self.verbose > 2:
			print " [INFO] Reading RX FIFO"
		byteNumber = 0 # Index to track the byte that is currently being read

		# For each byte in the message, wait for the RX FIFO to receive data,
		# read it, then append it to the list of data to be returned to user.
		# Print an error message in the case of a timeout and exit.
		while msgLen > 0:

			if self.verbose > 2:
				print("\n [INFO] Checking for byte ", byteNumber, " in RX FIFO...")

			rx_fifo_data = self.DATA_IN_RX_FIFO() # Check that Recieve fifo has data in it

			if rx_fifo_data != 1:
				# Check that data has been received
				print "\nError! Timeout whilst waiting for data in RX FIFO! exiting"
				sys.exit("RX FIFO did not empty after waiting on it, was in method Dyn_Read of class AXI_Interface")

			data = self.RX_FIFO_Read() # Now read the data from Reveive fifo
			if self.verbose > 2:
				print " [INFO] Just read %d from Rx fifo." % int(data,2)
			read_data.append(int(data,2)) # Add byte to list read_data
			msgLen -= 1
			byteNumber += 1
		# Return list of data
		return read_data



	######################################################################
	###### This method performs a write sequence using the Dynamic
	###### Controller Logic Flow as described on p.36 of the Xilinx AXI 
	###### Documentation. Arguments: The address of the internal register to write to
	###### and an array containing the bytes to write. Returns 1 if successful

	def Dyn_Write(self, addIn, content = []):

		msgLen = len(content) # get number of bytes to transmit
		if msgLen  < 0 :
			print " ERROR: Cannot read msg of lenght less than 1"
			sys.exit(" 6: Exiting, was in method: Dyn_Write from class: MiniPodControl")

		# Initialize Dynamic IIC access as described in p. 37 of Xilinx doc on the interface
		self.AXI_Init()
		
		# Check AXI controller ready
		i2cStatus = self.Check_i2c_ready()
		if i2cStatus == 1 :
			if self.verbose > 2:
				print " [INFO] AXI ready, Proceeding"
		else:
			print " [ERROR] AXI not ready, exiting"
			sys.exit(" 7: Exiting, was in method: Dyn_Write from class MiniPodControl")



		# Calculate four bytes to transmit, as described in the Xilinx documentation.
		# The first two bytes must be:
		#   - The start bit (0x01) and the I2C address shifted left by one bit and
		# with a 0 in the LSB (to signify a write).
		#   - The desired internal register to read: addIn

		write_data_1 = 0x0100 + (self.slaveAddress << 1)  # Start bit + Device I2C address shifted 1 bit to the left, sets R/W bit to W
		write_data_2 = addIn   # Internal register address for data needed for dummy Write in the case of a random read

		# Transmit bytes
		self.TX_FIFO_Write(write_data_1)
		if not self.TX_FIFO_EMPTY():
			sys.exit(" Exit after writing byte write_data_1")
		self.TX_FIFO_Write(write_data_2)
		if not self.TX_FIFO_EMPTY():
			sys.exit(" Exit after writing byte write_data_2")
		
		byteNumber = 0 # Track the byte that is being written
		while msgLen > 0:

			if self.verbose > 2:
				print("\n [INFO] Sending byte", byteNumber, "to TX FIFO...")

			self.TX_FIFO_Write(content[byteNumber])
			tx_status = self.TX_FIFO_EMPTY() # Check tx fifo empty before writting another byte
			if tx_status != 1:
				print "\nError! Timeout whilst waiting for data in RX FIFO! Exiting"
				sys.exit(" 8: Exiting, was in method: Dyn_Write from class MiniPodControl")
			
			if self.verbose > 2:
				print " [INFO] Just wrote {} to bus.".format(content[byteNumber])
			msgLen -= 1
			byteNumber += 1
		# Return 1 when finished transmiting
		return 1

	# ******************************************************************************* #
	"""
	IMPORTANT: The remaining methods are used by the Dyn_Read and Dyn_Write
	methods to control the AXI interface. They should not be used in the context
	of an I2C transaction by the user.
	For descriptions of the registers of the AXI interface and their bit map
	please refer to the Xilinx AXI documentation: pg090-axi-iic.pdf
	"""
	# ******************************************************************************* #

	######################################################################
	###### This method reads the current value of the General Interrupt
	###### Enable Register and provides and analysis of the read value if
	###### asked by verbosity level being set to 3 or 4. Returns the GIE value
	###### (either 1 or 0). Arguments: self

	def GIE_Read(self):	


		# Read the GIE register
		if self.verbose > 2: print " [INFO] Reading GIE value"
		value_read =  self.hw.getNode(self.device).getNode("gie").read()
		self.hw.dispatch()
		value_bin = format(value_read, "#034b")
		if self.verbose > 2:
			print " GIE in hex  = ", hex(value_read)
			print " GIE in binary = ", value_bin
			print "  "
		# Read meanings found in file pg090-qxi-iic.pdf, provided by Xilinx
		if self.verbose == 4:
			print " Interpreting GIE Value :"
			if value_bin[2] == '1':
				print " 	GIE enabled: Unmasked self.IIC core interrupts are passed to processor"
			else:
				print " 	All Interrupts disabled; no interrupt (even if unmasked in IER) possible from self.IIC core"
		if self.verbose > 2 : print " "
		return value_bin[2]



	######################################################################
	###### This method writes a desired value to the General Interrupt
	###### Enable Register. Arguments: self, desired write value (mandatory)
	###### Recomended values are 0xF0000000 or 0x0 (according to Xilinx doc)

	def GIE_Write(self,value_write):

		
		if self.verbose > 2: print " [INFO] Writing %s to GIE" % format(value_write,"#034b")

		# Write value
		self.hw.getNode(self.device).getNode("gie").write(value_write)
		self.hw.dispatch()

		if self.verbose > 2 : print " Write attempted"
		if self.verbose > 2 : print " "



	######################################################################
	###### This method reads the current value of the Interrupt Status
	###### Register and provides and analysis of the read value if asked 
	###### by verbosity level being set to 3 or 4. Returns the ISR value in
	###### binary. Arguments: self

	def ISR_Read(self):

		# Read Interrupt Status Register
		if self.verbose > 2 : print " Reading ISR value"
		value_read =  self.hw.getNode(self.device).getNode("isr").read()
		self.hw.dispatch()
		value_bin = format(value_read,"#010b")
		if self.verbose > 2:
			print " ISR  = ", hex(value_read)
			print " ISR in binary = ", value_bin 
			print "  "
		
		# Interrupt meanings found in file pg090-qxi-iic.pdf, provided by Xilinx
		if self.verbose == 4:
			print " Interpreting ISR Value :"
			if value_bin == '0b11010000':
				print " All interrupts at default value"

				print " 	Transmit FIFO half empty"
				print " 	Not addressed as slave"
				print " 	I2C bus is NOT busy"
				print " 	Receive FIFO NOT Full"
				print " 	Transmit FIFO NOT Empty/Default Value"


			else:
				if value_bin[2] == '1' :
					print " 	Transmit FIFO half empty"
				if value_bin[2] == '0' :
					print " 	Transmit FIFO half full"
				if value_bin[3] == '1' :
					print " 	Not addressed as slave"
				if value_bin[4] == '1' :
					print " 	Addressed as slave"
				if value_bin[5] == '1' :
					print " 	I2C bus is NOT busy"
				if value_bin[5] == '0' :
					print " 	I2C bus IS busy"
				if value_bin[6] == '1' :
					print " 	Receive FIFO Full"
				if value_bin[6] == '0' :
					print " 	Receive FIFO NOT Full"
				if value_bin[7] == '1' :
					print " 	Transmit FIFO Empty"
				if value_bin[7] == '0' :
					print " 	Transmit FIFO NOT Empty/Default Value"
				if value_bin[8] == '1' :
					print " 	Transmit Error/Slave Transmit Complete"
				if value_bin[9] == '1' :
					print " 	Arbitration Lost"
		if self.verbose > 2 : print " "
		return value_bin



	######################################################################
	###### This method writes a desired value to the Interrupt Status
	###### Register. Arguments: desired write value

	def ISR_Write(self,value_write):

		
		if self.verbose > 2: print " Writing %s to ISR" % format(value_write,"#010b")

		# Write value
		self.hw.getNode(self.device).getNode("isr").write(value_write)
		self.hw.dispatch()

		if self.verbose > 2: print " Write attempted"
		if self.verbose > 2 : print " "
	# Add checking mechanism?



	######################################################################
	###### This method reads the current value of the Interrupt Enable
	###### Register and provides and analysis of the read value if asked 
	###### by verbosity level being set to 3 or 4. Returns the IER value in
	###### binary. 

	def IER_Read(self):

		# Read Interrupt Enable Register
		if self.verbose > 2 : print " Reading IER value"
		value_read =  self.hw.getNode(self.device).getNode("ier").read()
		self.hw.dispatch()
		value_bin = format(value_read,"#010b")
		if self.verbose > 2: 
			print " IER  = ", hex(value_read)
			print " IER in binary = ", value_bin 
			print "  "

		# Interrupt meanings found in file pg090-qxi-iic.pdf, provided by Xilinx
		if self.verbose == 4:
			print " Interpreting IER Value :"
			if value_bin == "0b11111111":
				print " All interrupts are enabled"
				print " 	ENABLED: Interrupt(7) - Transmit FIFO half empty"
				print " 	ENABLED: Interrupt(6) - Not addressed as slave"
				print " 	ENABLED: Interrupt(5) - Addressed as slave"
				print " 	ENABLED: Interrupt(4) - I2C bus is NOT busy"
				print " 	ENABLED: Interrupt(3) - Receive FIFO Full"
				print " 	ENABLED: Interrupt(2) - Transmit FIFO Empty"
				print " 	ENABLED: Interrupt(1) - Transmit Error/Slave Transmit Complete"
				print " 	ENABLED: Interrupt(0) - Arbitration Lost"
			elif value_bin == "0b00000000":
				print " All interrupts are disabled"
				print " 	DISABLED: Interrupt(7) - Transmit FIFO half empty"
				print " 	DISABLED: Interrupt(6) - Not addressed as slave"
				print " 	DISABLED: Interrupt(5) - Addressed as slave"
				print " 	DISABLED: Interrupt(4) - I2C bus is NOT busy"
				print " 	DISABLED: Interrupt(3) - Receive FIFO Full"
				print " 	DISABLED: Interrupt(2) - Transmit FIFO Empty"
				print " 	DISABLED: Interrupt(1) - Transmit Error/Slave Transmit Complete"
				print " 	DISABLED: Interrupt(0) - Arbitration Lost"
			else:

				# Enabled interrupts
				if value_bin[2] == '1' :
					print " 	ENABLED: Interrupt(7) - Transmit FIFO half empty"
				if value_bin[3] == '1' :
					print " 	ENABLED: Interrupt(6) - Not addressed as slave"
				if value_bin[4] == '1' :
					print " 	ENABLED: Interrupt(5) - Addressed as slave"
				if value_bin[5] == '1' :
					print " 	ENABLED: Interrupt(4) - I2C bus is NOT busy"
				if value_bin[6] == '1' :
					print " 	ENABLED: Interrupt(3) - Receive FIFO Full"
				if value_bin[7] == '1' :
					print " 	ENABLED: Interrupt(2) - Transmit FIFO Empty"
				if value_bin[8] == '1' :
					print " 	ENABLED: Interrupt(1) - Transmit Error/Slave Transmit Complete"
				if value_bin[9] == 1 :
					print " 	ENABLED: Interrupt(0) - Arbitration Lost"

				# Disabled interrupts:
				if value_bin[2] == '0':
					print " 	DISABLED: Interrupt(7) - Transmit FIFO half empty"
				if value_bin[3] == '0':
					print " 	DISABLED: Interrupt(6) - Not addressed as slave"
				if value_bin[4] == '0':
					print " 	DISABLED: Interrupt(5) - Addressed as slave"
				if value_bin[5] == '0':
					print " 	DISABLED: Interrupt(4) - I2C bus is NOT busy"
				if value_bin[6] == '0':
					print " 	DISABLED: Interrupt(3) - Receive FIFO Full"
				if value_bin[7] == '0':
					print " 	DISABLED: Interrupt(2) - Transmit FIFO Empty"
				if value_bin[8] == '0':
					print " 	DISABLED: Interrupt(1) - Transmit Error/Slave Transmit Complete"
				if value_bin[9] == '0':
					print " 	DISABLED: Interrupt(0) - Arbitration Lost"
		if self.verbose > 0 : print " "
		return value_bin



	######################################################################
	###### This method writes a desired value to the Interrupt Enable
	###### Register. Arguments: desired write value

	def IER_Write(self,value_write):

		
		# Write value to IER
		if self.verbose > 2: print " Writing %s to IER" % format(value_write,"#010b")
		self.hw.getNode(self.device).getNode("ier").write(value_write)
		self.hw.dispatch()

		if self.verbose > 2: print " Write attempted"
		if self.verbose > 2: print " "



	######################################################################
	###### This method writes a reset code to the Softer Reset Register
	###### to perform a soft reset of the AXI controler.
	def Soft_Reset(self):

		if self.verbose > 2: print "Attempting Soft reset"

		# Write reset code to SOFTR register
		RESET = 0xA
		self.hw.getNode(self.device).getNode("softr").write(RESET)
		self.hw.dispatch()

		if self.verbose > 2: print " Reset Attempted"
		if self.verbose > 2 : print " "



	######################################################################
	###### This method reads the current value of the Control Register
	###### and provides and analysis of the read value if asked by the
	###### level being set to 3 or 4. Returns the CR value in binary.

	def CR_Read(self):

		# Read Control Register
		if self.verbose > 2: print " Reading CR value"
		value_read =  self.hw.getNode(self.device).getNode("cr").read()
		self.hw.dispatch()
		value_bin = format(value_read, "#09b")
		if self.verbose > 2:
			print " CR  = ", hex(value_read)
			print " CR in binary = ", value_bin
			print "  "

		# Read meanings found in file pg090-qxi-iic.pdf, provided by Xilinx
		if self.verbose == 4:
			print " Interpreting CR Value :"
			if value_bin == '0b0000000':
				print " All registers at default value:"
				print " 	General call Disabled"
				print " 	No current repeated start"
				print " 	ACK bit: 0 - acknowledge"
				print " 	Transmit/Receive Mode Select: self.IIC receive"
				print " 	Master/Slave Mode Select: default value"
				print " 	Transmit FIFO Reset: normal operation"
				print " 	self.I2C controller reset and disabled"

			else:
				if value_bin[2] == '0':
					print " 	General call disabled"
				else: 
					print " 	General call enabled"
				if value_bin[3] == '0':
					print " 	No current repeated start"
				else:
					print " 	Generating repeated start"
				if value_bin[4] == '0':
					print " 	ACK bit: 0 - acknowledge"	
				else:
					print " 	ACK bit: 1 - not-acknowledge"
				if value_bin[5] == '0':
					print " 	Transmit/Receive Mode Select: self.IIC receive"
				else:
					print " 	Transmit/Receive Mode Select: self.IIC transmit"
				if value_bin[6] == '0':
					print " 	Master/Slave Mode Select: default value"
				else:
					print " 	Master/Slave Mode Select: Generating START condition, self.controller as Master"
				if value_bin[7] == '0':
					print " 	Transmit FIFO Reset: normal operation"
				else:
					print " 	Transmit FIFO Reset: reset the Tx FIFO"
				if value_bin[8] == '0':
					print " 	self.I2C controller reset and disabled"
				else:
					print " 	self.I2C enabled"
		if self.verbose > 2 : print " "
		return value_bin



	######################################################################
	###### This method writes a desired value to the Control Register.
	###### Arguments: desired write value

	def CR_Write(self,value_write):

		if self.verbose > 2: print " Writing %s to CR" % format(value_write,"#09b")
		self.hw.getNode(self.device).getNode("cr").write(value_write)
		self.hw.dispatch()
		if self.verbose > 2: print " Write attempted"
		if self.verbose > 2: print " "



	######################################################################
	###### This method reads the current value of the Status Register
	###### and provides and analysis of the read value if asked by the
	###### level being set to 3 or 4. Returns the SR value in binary.


	def SR_Read(self):
		
		# Read Status Register
		if self.verbose > 2: print " Reading SR value"
		value_read =  self.hw.getNode(self.device).getNode("sr").read()
		self.hw.dispatch()
		value_bin = format(value_read,'#010b')
		if self.verbose > 2: 
			print " SR  = ", hex(value_read)
			print " SR in binary = ", value_bin 
			print "  "

		# Read meanings found in file pg090-qxi-iic.pdf, provided by Xilinx
		if self.verbose == 4:
			print " Interpreting SR Value :"
			if value_bin == '0b11000000':
				print " AXI-IIC in default state"
				print " 	Transmit FIFO empty"
				print " 	Receive FIFO empty"
				print " 	Transmit FIFO not full"
				print " 	Default value; if self.addressed as slave then: Slave Read Write: Writing"
				print " 	Bus idle"
				print " 	self.NOT Addressed as slave"
				print " 	General call not received AND/OR Gen call not enabled on bit CR(6)"

			else:

				if value_bin[2] == '1' :
					print " 	Transmit FIFO empty"
				if value_bin[2] == '0' :
					print " 	Transmit FIFO NOT empty"
				if value_bin[3] == '1' :
					print " 	Receive FIFO empty"
				if value_bin[3] == '0' :
					print " 	Receive FIFO NOT empty"
				if value_bin[4] == '1' :
					print " 	Receive FIFO full (16 bytes)"
				if value_bin[5] == '1' :
					print " 	Transmit FIFO full (16 bytes)"
				if value_bin[5] == '0' :
					print " 	Transmit FIFO not full"
				if value_bin[6] == '1' :
					print " 	Slave Read Write: Master reading from slave"
				if value_bin[6] == '0' :
					print " 	Default value; Slave Read Write: Writing"
				if value_bin[7] == '1' :
					print " 	Bus busy"
				if value_bin[7] == '0' :
					print " 	Bus idle"
				if value_bin[8] == '1' :
					print " 	AXI Addressed as slave"
				if value_bin[8] == '0' :
					print " 	AXI Addressed as slave"
				if value_bin[9] == '1' :
					print " 	General call received and enabled"
		if self.verbose > 2: print " "
		return value_bin



	######################################################################
	###### This method writes a desired value to the TX_FIFO Register.
	###### Arguments: desired write value

	def TX_FIFO_Write(self,value_write):

		if self.verbose > 2: print " Writing %s to TX_FIFO" % format(value_write,"#010b")
		self.hw.getNode(self.device).getNode("tx_fifo").write(value_write)
		# What is this hw.dispatch for???
		#self.hw.dispatch()
		if self.verbose > 2: print " Write attempted"
		if self.verbose > 2: print " "



	######################################################################
	###### This method reads the current value of the Receive FIFO
	###### Register and prints it if requested by the level being set to
	###### 3 or 4. Returns the RX_FIFO value in binary.

	def RX_FIFO_Read(self):
		
		# Read RX_FIFO Register
		if self.verbose > 2: print " Reading Receive FIFO content"
		value_read =  self.hw.getNode(self.device).getNode("rx_fifo").read()
		self.hw.dispatch()
		value_bin = format(value_read,"#010b")

		# Display RX_FIFO content
		if self.verbose > 2:
			print " RX_FIFO content = ", hex(value_read)
			print " RX_FIFO content in binary = ", value_bin 
			print "  "

		if self.verbose > 2 : print " "
		return value_bin



	######################################################################
	###### This method writes a desired value to the TX_FIFO Register.
	###### Arguments: desired write value

	def ADR_Write(self,value_write):

		if self.verbose > 2: print " Writing 7 bit address %s to ADR" % format(value_write,"#010b")
		self.hw.getNode(self.device).getNode("adr").write(value_write)
		self.hw.dispatch()
		if self.verbose > 2: print " Write attempted"
		if self.verbose > 2: print " "



	###### Methods that could be written but are not useful:
	###### Slave address (ADR) read and write methods
	###### Slave address 10 bit read and write methods
	###### 



	######################################################################
	###### This method reads the current value of the Transmit FIFO
	###### Occupancy Register and provides and prints the read value if
	###### asked by the verbosity level being set to 3 or 4. Returns the 
	###### RX_FIFO_OCY value in binary.

	def TX_FIFO_OCY_Read(self):

		# Read TX_FIFO_OCY reg
		if self.verbose > 2: print " Reading Transmit FIFO Occupancy"

		value_read =  self.hw.getNode(self.device).getNode("tx_fifo_ocy").read()
		self.hw.dispatch()

		value_bin = format(value_read, "#06b")
		if self.verbose > 2: 
			print " Transmit FIFO ocupancy = ", hex(value_read)
			print " Transmit FIFO ocupancy in binary = ", value_bin
			print " "

		if self.verbose > 2 : print " "
		return value_bin



	######################################################################
	###### This method reads the current value of the Receive FIFO
	###### Occupancy Register and provides and analysis of the read value
	###### if asked by the level being set to 2. Returns the RX_FIFO_OCY value
	###### in binary.

	def RX_FIFO_OCY_Read(self):

		# Read RX_FIFO_OCY reg
		if self.verbose > 2: print " Reading Receive FIFO Occupancy"

		value_read =  self.hw.getNode(self.device).getNode("rx_fifo_ocy").read()
		self.hw.dispatch()

		value_bin = format(value_read, "#06b")
		if self.verbose > 2: 
			print " Receive FIFO ocupancy = ", hex(value_read)
			print " Receive FIFO ocupancy in binary = ", value_bin
			print " "

		if self.verbose > 2 : print " "
		return value_bin



	######################################################################
	###### This method reads the current value of the Receive FIFO
	###### Programmable Depth Interrupt Register and provides and analysis 
	###### of the read value if asked by the level being set to 2. Returns
	###### the RX_FIFO value in binary. Arguments:  
	###### (mandatory), verbosity level (default arg.)

	def RX_FIFO_PIRQ_Read(self):

		# Read RX_FIFO_PIRQ reg
		if self.verbose > 2: print " Reading RX_FIFO_PIRQ"

		value_read =  self.hw.getNode(self.device).getNode("rx_fifo_pirq").read()
		self.hw.dispatch()

		value_bin = format(value_read, "#06b")
		if self.verbose > 2: 
			print " Transmit RX_FIFO_PIRQ ocupancy = ", hex(value_read)
			print " Transmit RX_FIFO_PIRQ in binary = ", value_bin
			print " "

		if self.verbose > 2 : print " "
		return value_bin



	######################################################################
	###### This method writes a desired value of the Receive FIFO
	###### Programmable Depth Interrupt Register. Arguments:  
	###### (mandatory), desired write value (mandatory)  verbosity level 
	###### (default arg.)

	def RX_FIFO_PIRQ_Write(self,value_write):

		# Write to RX_FIFO_PIRQ reg
		if self.verbose > 2: print " Writing %s to RX_FIFO_PIRQ" % format(value_write, "#010b")
		self.hw.getNode(self.device).getNode("rx_fifo_pirq").write(value_write)
		self.hw.dispatch
		if self.verbose > 2: print " Write attempted"
		if self.verbose > 2 : print " "



	######
	###### Timing registers read and write methods
	###### 


	######################################################################
	###### This method writes a desired value of the Receive FIFO
	###### Programmable Depth Interrupt Register. Arguments:
	###### desired write value (mandatory)  verbosity level 
	###### (default arg.)

	def MPOD_Reset(self):

		# Write a 1 then a 0 to bits 4 and 5 of the hub_control register
		# this triggers the Reste and the Interrupt og the minipod interface
		resetCode1 = 0x30
		self.hw.getNode("hub").getNode("hub_control").write(resetCode1)
		self.hw.dispatch()
		out = self.hw.getNode("hub").getNode("hub_control").read()
		self.hw.dispatch()
		if self.verbose > 2 : print " [INFO] Reseting the MiniPod ", hex(out)
		resetCode2 = 0x0

		self.hw.getNode("hub").getNode("hub_control").write(resetCode2)
		self.hw.dispatch()
		out = self.hw.getNode("hub").getNode("hub_control").read()
		self.hw.dispatch()
		if self.verbose > 2 : print " [INFO] MiniPod reset attempted ", hex(out)




	def BUFFERS_Enable(self):
		# Write 1 to hub_control bits 16 and 15 to enable buffers
		# U1501, U1502 and U1503
		
		mask_I2C_Buf_DCDC_ENABLE = 0x00010000
		mask_I2C_Buf_FPGA_ENABLE = 0x00008000
		mask_I2C_Buf_ROD_ENABLE  = 0x00004000
		mask_I2C_Buf_ROD_DISABLE  = 0xFFFFBFFF
		mask_I2C_Buf_all_ENABLE  = mask_I2C_Buf_DCDC_ENABLE \
								| mask_I2C_Buf_FPGA_ENABLE \
								| mask_I2C_Buf_ROD_ENABLE
		read_tmp =  self.hw.getNode("hub").getNode("hub_control").read()
		self.hw.dispatch()

		write_control_value_tmp = eval (str(read_tmp))
		write_control_value_int = (write_control_value_tmp | mask_I2C_Buf_all_ENABLE) & mask_I2C_Buf_ROD_DISABLE
		write_control_value_hex = hex(write_control_value_int)

		self.hw.getNode("hub").getNode("hub_control").write(write_control_value_int)
		self.hw.dispatch()
		ctrl_read = self.hw.getNode("hub").getNode("hub_control").read()
		self.hw.dispatch()
		if self.verbose > 3:
			print " [INFO] Control reg is now: " + str(hex(ctrl_read))
		if self.verbose > 2:
			print " [INFO] Buffers U1501, U1502 and U1503 enabled"
		
		else:
			if self.verbose > 2:
				print " [INFO] The hub_control register bits 16, 15 and 14 were set correctly, Buffers U1501, U1502 and U1503 enabled"



	######################################################################
	###### This method performs a dummy write to an internal register
	###### of the MiniPod, necessary for random read. Arguments:
	###### the Word address which the user wants to read, it
	###### returns the read value in decimal

	"""
	IMPORTANT: This method is outdated, the use of method Dyn_Read is 
	heavily recomended over this method, it can only read one address at a time
	and should be used for DEBUGGING purposes ONLY.
	"""
	def Read_MPOD_Reg(self, addIn):

		# Wait necessary here to not get NACK from slave
		sleep(0.01)
		wordAdd = format(addIn,"#04x")
		while addIn < 0 or addIn > 255:
			print "Error: address must be positive and less than 256"
			addIn = input("Enter Word address: ")
			wordAdd = format(addIn,"#04x")
		if self.verbose > 2 : print " [INFO] Reading from word address = ", wordAdd
		wordAdd = int(addIn)
		# Write peripheral device address with R/W bit set to W to TX_FIFO
		slaveAddW = 0x50
		self.TX_FIFO_Write(slaveAddW)
		if self.verbose > 2 : print " [INFO] Wrote peripheral device address with R/W bit set to W to TX_FIFO"

		# Activate self.as master receiver and send Start signal
		startCode = 0b0001101
		self.CR_Write(startCode)
		if self.verbose > 2 :
			print "[INFO] Started the transfer of slave address"
			print "[INFO] Waiting for TX_FIFO empty interrupt"

		# Write word address to TX_FIFO
		self.TX_FIFO_Write(wordAdd)
		if self.verbose > 2 : print " [INFO] Wrote word address to TX_FIFO"
		isrVal = self.ISR_Read()
		while isrVal[7] != '1':
			isrVal = self.ISR_Read()
			sleep(0.001)
			if self.verbose > 2: print " [INFO] Waiting for TX FIFO to empty..."

		'''
		Now read the current register 
		'''
		if self.verbose > 2: print "\n\n\n\n\n [INFO] Starting Read sequence"

		# Write total message lenght minus 2 to RX_FIFO_PIRQ, MUST BE LESS THAN 0xE !
		msgLen = 0x0
		self.RX_FIFO_PIRQ_Write(msgLen)
		if self.verbose > 2: print " [INFO] Wrote total message lenght to RX_FIFO_PIRQ"

		# Set CR MSMS = 1; CR TX = 0; TXAK = 1 (not ACK); EN = 1
		configCode = 0b0110101
		self.CR_Write(configCode)
		if self.verbose > 2: print " [INFO] Sent repeated start"

		# Write peripheral device address to TX_FIFO
		slaveAddR = 0x51
		self.TX_FIFO_Write(slaveAddR)
		if self.verbose > 2 : print " [INFO] Wrote peripheral device address to TX_FIFO"

		# 

		isrVal = self.ISR_Read()
		timeOutCounter = 0
		while isrVal[6] != '1' and timeOutCounter < 100:
			if self.verbose > 2 : print " [INFO] Waiting for data..."
			isrVal = self.ISR_Read()
			sleep(0.001)
			timeOutCounter += 1
		if self.verbose > 2 : print " [INFO] Data received"
		self.ISR_Read()

		result = self.RX_FIFO_Read()
		if self.verbose > 0 :
			print "Register %s content: " % addIn
			print "Hex: ", hex(int(result,2))
			print "Binary: ", result
			print "Decimal: ", int(result,2)
		
		CRstatus = list(self.CR_Read())
		print 'CR STATUS: ',CRstatus
		CRstatus[6] = '0'
		newCR = "".join(CRstatus)
		print 'CR STATUS: ',newCR
		self.CR_Write(int(newCR,2))


		return result






	######################################################################
	###### This method checks if the TX_FIFO contents have been transmitted
	###### It returns 1 if the TX_FIFO is empty, and 0 if not. It reads 
	###### the Status Register to check if it is empty, and checks 10 times
	###### at roughly 0.1ms intervals if it is empty
	
	def TX_FIFO_EMPTY(self):

		# read the status register
		srVal = self.SR_Read()
		timeOutCounter = 0
		if self.verbose > 2 :
			print " [INFO] Waiting for data..."
		
		while srVal[2] != '1' and timeOutCounter < 10:
			srVal = self.SR_Read()
			sleep(0.0001)
			timeOutCounter += 1
			if timeOutCounter > 8:
				self.TX_FIFO_OCY_Read()
		
		if srVal[2] == '1':
			if self.verbose > 2:
				print " [INFO] TX FIFO empty"
			sleep(0.0005) # Wait because the contents might still be transmitting
			return 1
		else:
			if self.verbose > 2:
				print " [INFO] Error: Tx FIFO not empty after 10 tries"
			return 0



	######################################################################
	###### This method checks if the RX_FIFO has received data from the 
	###### transmitting device. It returns 1 if the RX_FIFO is not empty, 
	###### and 0 if it is. It reads the Status Register to check if it
	###### is empty, and checks 10 times at roughly 1ms intervals
	###### In the future add: Check with the ISR as well

	def DATA_IN_RX_FIFO(self):
		
		srVal = self.SR_Read()
		timeOutCounter = 0
		if self.verbose > 2:
			print " [INFO] Waiting to receive data in RX FIFO"
		while srVal[3] != '0' and timeOutCounter < 10:
			srVal = self.SR_Read()
			sleep(0.01)
			timeOutCounter += 1
			
		if srVal[3] == '0':
			if self.verbose > 2 :
				print " [INFO] Some data was received and is in RX FIFO"
			return 1
		else:
			if self.verbose > 2:
				print " Error: No data was received in RX FIFO"
			return 0



	######################################################################
	###### This method checks if the AXI-IIC controller is ready for a
	###### transaction. It checks that the TX and RX FIFOs are empty, that
	###### the ISR and CR registers are in their default states, and reads
	###### the SR, indicating if it is in its default state or not.
	###### All binary values hard coded here are found in the Xilinx documentation
	###### Returns 1 if the interface is ready, 0 if it is not.

	def Check_i2c_ready(self):
		retry_counter = isrReady = crReady = srReady = 0
		# Save the value of self.verbose, in case it needs to be temporarily changed
		
		while(retry_counter <9):
			
			srVal = self.SR_Read() # Read Status register
			isrVal = self.ISR_Read()
			crVal = self.CR_Read()
			# If ISR not at default, toggle bit by bit
			if isrVal == '0b11010000' or isrVal == "0b11010010":
				# The ISR is already at default
				isrReady = 1
			else:
				if self.verbose > 2:
					print "ISR not at default; toggling bits not at default value"
					self.logFile.write(" ISR not at default; toggling bits not at default value\n")
					print(" [INFO] ISR val before reset: "+str(isrVal))
				toggleCode = '0b'
				# toggle the ISR register bit by bit
				if isrVal[2] != '1':
					toggleCode += '1'
				else:
					toggleCode += '0'
				if isrVal[3] != '1':
					toggleCode += '1'
				else:
					toggleCode += '0'
				if isrVal[4] != '0':
					toggleCode += '1'
				else:
					toggleCode += '0'
				if isrVal[5] != '1' :
					toggleCode += '1'
				else:
					toggleCode += '0'
				if isrVal[6] != '0':
					toggleCode += '1'
				else:
					toggleCode += '0'
				if isrVal[7] != '0':
					toggleCode += '1'
				else:
					toggleCode += '0'
				if isrVal[8] != '0':
					toggleCode += '1'
				else:
					toggleCode += '0'
				if isrVal[9] != '0':
					toggleCode += '1'
					print " [ERROR] Arbitration Lost ISR"
					self.logFile.write("Arbitration lost as indicated by ISR\n")
				else:
					toggleCode += '0'
				self.ISR_Write(int(toggleCode,2)) # Write the constructed toggle code
				# Check the toggles were successful
				isrVal = self.ISR_Read()
				if isrVal == '0b11010000':
					# ISR now at default value, set ISR flag to 1
					isrReady = 1 # If the execution of this method made it here, the ISR is ready
				else:
					print " [Warning] ISR reset unsuccesful"
					# ISR still not at default value, set ISR flag to 0
					#self.logFile.write("ISR reset unsuccesful\n")
					isrReady = 0


			# If control register is not at default, write default value to it
			if crVal == '0b0000001': 
				# Control Register ready
				crReady = 1
			else:
				if self.verbose > 2:
					print " [INFO] CR not at default, reseting it"
				self.CR_Write(0b0000001)
				# Check that it worked
				crVal = self.CR_Read()
				if crVal != '0b0000001':
					print " [WARNING] Control Register value invalid after reset attempt"
					crReady = 0
					#self.logFile.write(" Control Register value invalid after reset attempt\n")
				else:
					crReady = 1 # If the execution of this method made it here, the CR is ready
					#self.logFile.write(" Control Register reset successful\n")
					print(" Control Register reset successful\n")

			
			# If Status Register not a default, report it and exit
			if srVal != '0b11000000':
				if self.verbose > 2:
					print " [INFO] Status register not a default value."
				# Check TX FIFO is empty
				if srVal[2] != '1':
					if self.verbose > 2:
						print " [ERROR] Tx fifo not empty"
						print " [INFO] Attempt empty Tx Fifo ..."
					self.CR_Write(0x2)
					self.CR_Write(0x1)
					if self.verbose > 2:
						print " [INFO] Empty Tx fifo was attempted, checking success"
					srVal = self.SR_Read()
					if srVal[2] == '1':
						if self.verbose > 2:
							print " [INFO] Empty Tx fifo was successful"
							print " [INFO] Re-initializing AXI"
						self.AXI_Init()
					else:
						if self.verbose > 2:
							print " [ERROR] Empty Tx fifo was unsuccessful"
							print " [INFO] Attempting soft reset"
						self.Soft_Reset()
						srVal = self.SR_Read()
						if srVal[2] != '1':
							print " [WARNING] Soft Reset could not empty Tx fifo"
				# Check Rx FIFO is empty
				if srVal[3] != '1':
					if self.verbose > 2:
						print " [ERROR] Rx fifo not empty"
						print " [INFO] Attempt empty Rx Fifo ..."
						print " [INFO] Attempting soft reset"
					self.Soft_Reset()
					srVal = self.SR_Read()
					if srVal[3] != '1':
						print " [WARNING] Soft Reset could not empty Rx fifo"
					else:
						if self.verbose > 2:
							print " [INFO] Reset successful, re-initializing AXI"
						self.AXI_Init()
			srVal = self.SR_Read()
			if srVal == '0b11000000':
				# Status register successfully reset
				srReady = 1
			else:
				#print( " [WARNING] Status Register still not at default value after reset attempt")
				#print( " [INFO] The Status Register is at value: " + str(srVal))
				#self.logFile.write("Status Register still not at default value after reset attempt\nThe value of the Status Register is: "+str(srVal) + "\n")
				self.SR_Read()
				srReady = 0
			
			# Terminate if AXI ready
			if isrReady and crReady and srReady:
				if retry_counter > 0:
					outStr = " It took " + str(retry_counter) + " tries to get past i2c problems\n"
					self.logFile.write(outStr)
					print(outStr)
				if self.verbose > 2:
					print " [INFO] Verified that AXI is ready"
				return 1
			# Try again otherwise, after waiting for 10 ms:
			sleep(0.01)
			retry_counter +=1
			# Try a soft reset to remedy problem:
			print(" [INFO] Attempting soft reset")
			self.logFile.write(" [INFO] Attempting soft reset\n")
			self.Soft_Reset()
		
		# Terminate if this loop has reached 10 unsuccessful attempts
		if retry_counter == 9:
			print(" [ERROR] was not able to check that i2c bus was ready")
			self.logFile.write(" [ERROR] was not able to check that i2c bus was ready\n")
			if isrReady != 1: 
				print(" Interrupt Status Register not at default after 10 tries")
				self.logFile.write(" Interrupt Status Register not at default after 10 tries\n")
			if crReady != 1: 
				print(" Control Register not at default after 10 tries")
				self.logFile.write(" Control Register not at default after 10 tries\n")
			if srReady != 1: 
				print(" Status Register not at default after 10 tries")
				self.logFile.write(" Status Register not at default after 10 tries\n")
			sys.exit(" Exiting due to I2C bus not ready after 10 tries or 100 ms")
			self.logFile.write(" Exiting due to I2C bus not ready after 10 tries or 100 ms\n")


	######################################################################
	###### This method initializes the AXI-IIC controller as described in
	###### the Xilinx doc about the AXI interface. It sets the RX_FIFO to
	###### maximum by writing 0X0F to the RX_FIFO_PIRQ register, reseting
	###### the TX_FIFO and enabling the AXI, removing the TX_FIFO reset,
	###### disabling the general call. Does not return anything.
	###### (Could change this to return 1 if successful, how to check?)


	def AXI_Init(self):
		if self.verbose > 2:
			print " [INFO] Initializing AXI controler"
		self.RX_FIFO_PIRQ_Write(0x0F) # set RX_FIFO depth to max
		self.CR_Write(0x02) # TX_FIFO Reset
		self.CR_Write(0x01) # AXI enable, remove reset, disable gen call



######################################################################
'''

END OF FILE

'''	
######################################################################
