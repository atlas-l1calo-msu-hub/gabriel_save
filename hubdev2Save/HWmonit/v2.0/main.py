from source.Monitor import MiniPodMonitor
from source.Monitor import PowerModule
from source.Monitor import SysMonitor
from source.ConsolApp import ConsoleOutput
import os  # for manipulating directories
import sys
import uhal
from time import sleep

def check_dir():
	'''
	Simple function that checks that the starting directory is
	the home directory, so that IPBus can find Connection and 
	Address file can be found successfully
	'''
	
	if os.getcwd()[-4:] == "v2.0":
		if verbose > 2:
			print " Already in home dir."
	elif os.getcwd()[-3] == "source":
		os.chdir('../')
	elif os.getcwd()[-7:] == "Reports":
		os.chdir('../')
	else:
		#print " [ERROR] Could not find home directory to store output of monitor, exiting"
		#print " [INFO] Was in directory : ", os.getcwd()
		sys.exit(" Directory error")
	
def call_mpods(hw):
	'''
	Function that initiates the MiniPod monitoring
	'''
	if verbose > 1: print ("\n"+"-"*10+"Aquiring data from receiver miniPod"+"-"*10+"\n")
	RX_DATA = MiniPodMonitor(hw,"recvr_mpod",verbose).return_data()
	check_dir()
	if verbose > 1: print("\n"+ "-"*10+"-"*10+"Aquiring data from transmit minipod"+"-"*10+"\n")
	TX_DATA = MiniPodMonitor(hw,"trans_mpod",verbose).return_data()
	# Check that we are in home directory

	return [RX_DATA, TX_DATA]
	
def call_sysmon(hw,device="sysmon",device_type="HUB"):
	"""
	Function that initiates the Sysmon monitoring
	Return a dictionary which holds the Sysmon data
	Default behavior is to aquire the Sysmon data from a HUB card, using the hw ipbus object
	"""
	return SysMonitor(hw,device,device_type,verbose,fileOut).return_data()


def call_dcdc(hw,card_type="HUB"):
	check_dir()
	#print("\n"+"-"*10+"Aquiring data from DCDC power supplies"+"-"*10+"\n" )
	DCDC_DATA = PowerModule(hw,"hub_dcdc",verbose,card_type).return_data()
	return DCDC_DATA





# Create instance of Monitor and start data aquisition
if Hub_1_dcdc == 1 :
	data_dcdc_hub1 = call_dcdc(hw_Hub1,card_type="HUB")
if Rod_1_dcdc == 1:
	data_dcdc_rod1 = call_dcdc(hw_Hub1,card_type="ROD")
if Hub_1_mpods == 1:
	data_mpods_hub1 = call_mpods(hw_Hub1)
if Hub_2_dcdc == 1:
	data_dcdc_hub2 = call_dcdc(hw_Hub2,card_type="HUB")
if Rod_2_dcdc == 1:
	data_dcdc_rod2 = call_dcdc(hw_Hub2,card_type="ROD")
if Hub_2_mpods == 1:
	data_mpods_hub2 = call_mpods(hw_Hub2)
if Hub_1_sysmon == 1:
	sysmon_hub1 = call_sysmon(hw_Hub1)
if Hub_2_sysmon == 1:
	sysmon_hub2 = call_sysmon(hw_Hub2)

HTM_mpod_list = [] # list for storing HTM data read by software
HTM_sysmon_list = []
# Loop through list of HTM hw managers:
for htmHw,htmName in HTM_hw_list:
	HTM_mpod_list.append([call_mpods(htmHw),htmName])
	HTM_sysmon_list.append([call_sysmon(htmHw,"sysmon","HTM"),htmName])
# Create instance of ConsolApp, and start data output, as well as the system monitor
if verbose > 0 or fileOut > 0:
	if Hub_1_dcdc == 1:
		ConsoleOutput("HUB1", "hub_dcdc", data_dcdc_hub1, verbose , fileOut)
	if Rod_1_dcdc == 1:
		ConsoleOutput("ROD1", "hub_dcdc", data_dcdc_rod1, verbose, fileOut)
	if Hub_1_mpods == 1:
		ConsoleOutput("HUB1", "mini_pods", data_mpods_hub1, verbose, fileOut)
	if Hub_2_dcdc == 1:
		ConsoleOutput("HUB2", "hub_dcdc", data_dcdc_hub2, verbose, fileOut)
	if Hub_2_mpods == 1:
		ConsoleOutput("HUB2", "mini_pods", data_mpods_hub2, verbose, fileOut)
	for htmData, htmName in HTM_mpod_list:
		ConsoleOutput(htmName, "mini_pods", htmData, verbose, fileOut)
	if Hub_1_sysmon == 1:
		ConsoleOutput("HUB1", "sysmon", sysmon_hub1, verbose, fileOut)
	if Hub_2_sysmon == 1:
		ConsoleOutput("HUB2", "sysmon", sysmon_hub2, verbose, fileOut)
