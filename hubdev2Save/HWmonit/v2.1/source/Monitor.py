######################################################################
"""

Created: 05/24/2018
Coded by: Gabriel Moreau (moreauga@msu.edu)
Goal: Test access to MiniPods on HUB1 by the I2C bus
Last updated: 05/30/2018
Current approach: attempt to read 2 bytes from MiniPods
"""
######################################################################
# -*-coding: utf-8 -*-
import uhal # For IPBus
import sys # For exits and such
from time import sleep # To allow I2C time to set up
import datetime # For naming output files
import os  # to switch to the Reports directory
from collections import OrderedDict
import math as mth
# Import AXI controler
sys.path.append("/home/hubuser/PyProg/HWmonit/AXI/OLD_PRO")
from AXI_Class import AXI_Interface # I2C controller for device MiniPods



class i2cDevices:
	'''
	Superclass for the different devices on the I2C bus, to be inherited by 
	each of the device monitoring classes, it contains useful methods that
	are used by multiple devices
	'''

	def bytes_to_int(self,byte_list):
		'''
		Method that converts the list of bytes returned by a read operation to an integer
		arguments: byte_list: list of individual bytes read from AXI-I2C
		returns: an int, where the first item byte of the list is interpreted as the LSB
		'''
		
		# Initialise
		value_sum = 0

		# sum over the list, where lower indicies represent more significant bits
		for index, item in enumerate(byte_list):
			value_sum += item * 2 **(index*8)
		if self.verbose > 2 :
			print " [INFO] Converted byte_list from AXI to integer format"
		return value_sum





	def twos_comp_to_int(self,value, num_bits=16):
		'''
		Method to calculate a signed int from an integer representing a twos
		complement value. The integer is assumed to be fixed-width, so the
		number of bits of the number must be specified to ensure the MSB is
		correctly interpreted when calculating negative numbers.

		- Arguments: value	- The twos complement value to convert
		num_bits - The number of bits the integer is intended
			to represent.
		- Returns a signed integer
		'''
		# Twos complement calculation breaks for value = 0, so return
		# 0 immediately if this is the case
		if value == 0:
			return 0

		# Check value is within range. Must be less than 2^(num_bits) and
		# cannot be negative
		if value < 0:
			raise ValueError("Twos complement integer cannot be negative")
			return 0
		if value >= (2 ** num_bits):
			raise ValueError("Value exceeds specified number of bits")
			return 0

		# Check if negative bit set. If it is:
		#	1) Subtract negative bit from number
		#	2) Invert all lower bits (by exclusiv or-ing with mask)
		#	3) Add one to result
		if value & (2 ** (num_bits - 1)):
			value = -(((value - (2 ** (num_bits - 1))) ^((2 ** (num_bits - 1)) - 1)) + 1)

		return value


	def get_decimal(self, value, mantissa_length,
					twos_complement=1, num_bits=16):
		'''
		Method to calculate a floating point value from an integer representing
		the mantissa and exponent of a floating point number. The total number
		of bits of the integer must be specified to allow the mantissa and
		exponent to be correctly calculated. The mantissa is assumed to be
		located in the LSBs and its length must be specified. The exponent
		is then asseumed to be located in any remaining bits (aligned to the
		MSB).

		If twos_complement is set to 1, both the mantissa and exponenet are
		assumed to be in twos complement format and are calculated by calling
		twos_comp_to_int

			- Arguments: value		   - The int to convert to floating point
						 mantissa_length - The length of the mantissa (in bits)
										   (assumed aligned to LSB)
						 twos_complement - Mantissa and exponent are treated
										   as twos complement integers if set
						 num_bits		- The total number of bits the
										   floating point number is represented
										   by
			- Returns the floating point number represented by the mantissa
			  and exponent.
		'''
		# Assumes mantissa is in lower value bits and that exponent makes
		# up any higher bits.
		mantissa = value % (2 ** mantissa_length)
		exponent = value / (2 ** mantissa_length)

		# Calcualte twos complmenet values of mantissa and exponent if required
		if twos_complement:
			mantissa = self.twos_comp_to_int(mantissa, mantissa_length)
			exponent = self.twos_comp_to_int(exponent,
											 num_bits - mantissa_length)

		# Calculate and return decimal value
		return mantissa * 2.0 ** exponent




class MiniPodMonitor(i2cDevices):	
	'''
	This class need only be initiated, it will generate a monitoring file, as well as
	printing out details to the command line if verbosity is set to 2 and relevant 
	steps the program takes if verbosity is set to 1.
	The user should be prompted for the desired verbosity level.
	Should eventually also choose the device (HUB1/HUB2)
	'''
	


	def __init__(self, hw, device, verbose):
		'''
		Class constructor, this sets up IPBus and creates and instance of the
		MiniPodControl class to allow i2c transactions with the miniPods. It 
		then calls a method to create a csv file, and then calls all of the
		monitoring methods to write the relevant monitoring data to the file
		and to the terminal if requested by setting the verbosity to 1 or more
		'''
		self.rx_status_map = {
					 2: {0: "DATA_NOT_READY"},

					 9: {0: "RX_LOS_08",
						 1: "RX_LOS_09",
						 2: "RX_LOS_10",
						 3: "RX_LOS_11"},

					 10: {0: "RX_LOS_00",
						  1: "RX_LOS_01",
						  2: "RX_LOS_02",
						  3: "RX_LOS_03",
						  4: "RX_LOS_04",
						  5: "RX_LOS_05",
						  6: "RX_LOS_06",
						  7: "RX_LOS_07"},

					 13: {6: "LOW_TEMP_ALARM",
						  7: "HIGH_TEMP_ALARM"},

					 14: {2: "LOW_2V5_ALARM",
						  3: "HIGH_2V5_ALARM",
						  6: "LOW_3V3_ALARM",
						  7: "HIGH_3V3_ALARM"},

					 22: {2: "CHAN_10_LOW_RX_PWR_ALARM",
						  3: "CHAN_10_HIGH_RX_PWR_ALARM",
						  6: "CHAN_11_LOW_RX_PWR_ALARM",
						  7: "CHAN_11_HIGH_RX_PWR_ALARM"},

					 23: {2: "CHAN_08_LOW_RX_PWR_ALARM",
						  3: "CHAN_08_HIGH_RX_PWR_ALARM",
						  6: "CHAN_09_LOW_RX_PWR_ALARM",
						  7: "CHAN_09_HIGH_RX_PWR_ALARM"},

					 24: {2: "CHAN_06_LOW_RX_PWR_ALARM",
						  3: "CHAN_06_HIGH_RX_PWR_ALARM",
						  6: "CHAN_07_LOW_RX_PWR_ALARM",
						  7: "CHAN_07_HIGH_RX_PWR_ALARM"},

					 25: {2: "CHAN_04_LOW_RX_PWR_ALARM",
						  3: "CHAN_04_HIGH_RX_PWR_ALARM",
						  6: "CHAN_05_LOW_RX_PWR_ALARM",
						  7: "CHAN_05_HIGH_RX_PWR_ALARM"},

					 26: {2: "CHAN_02_LOW_RX_PWR_ALARM",
						  3: "CHAN_02_HIGH_RX_PWR_ALARM",
						  6: "CHAN_03_LOW_RX_PWR_ALARM",
						  7: "CHAN_03_HIGH_RX_PWR_ALARM"},

					 27: {2: "CHAN_00_LOW_RX_PWR_ALARM",
						  3: "CHAN_00_HIGH_RX_PWR_ALARM",
						  6: "CHAN_01_LOW_RX_PWR_ALARM",
						  7: "CHAN_01_HIGH_RX_PWR_ALARM"}
						}

		self.tx_status_map = {
					 2: {0: "DATA_NOT_READY"},

					 9: {0: "TX_LOS_08",
						 1: "TX_LOS_09",
						 2: "TX_LOS_10",
						 3: "TX_LOS_11"},

					 10: {0: "TX_LOS_00",
						  1: "TX_LOS_01",
						  2: "TX_LOS_02",
						  3: "TX_LOS_03",
						  4: "TX_LOS_04",
						  5: "TX_LOS_05",
						  6: "TX_LOS_06",
						  7: "TX_LOS_07"},

					 11: {0: "TX_FAULT_08",
						  1: "TX_FAULT_09",
						  2: "TX_FAULT_10",
						  3: "TX_FAULT_11"},

					 12: {0: "TX_FAULT_00",
						  1: "TX_FAULT_01",
						  2: "TX_FAULT_02",
						  3: "TX_FAULT_03",
						  4: "TX_FAULT_04",
						  5: "TX_FAULT_05",
						  6: "TX_FAULT_06",
						  7: "TX_FAULT_07"},

					 13: {6: "LOW_TEMP_ALARM",
						  7: "HIGH_TEMP_ALARM"},

					 14: {2: "LOW_2V5_ALARM",
						  3: "HIGH_2V5_ALARM",
						  6: "LOW_3V3_ALARM",
						  7: "HIGH_3V3_ALARM"},

					 16: {2: "CHAN_10_LOW_TX_BIAS_ALARM",
						  3: "CHAN_10_HIGH_TX_BIAS_ALARM",
						  6: "CHAN_11_LOW_TX_BIAS_ALARM",
						  7: "CHAN_11_HIGH_TX_BIAS_ALARM"},

					 17: {2: "CHAN_08_LOW_TX_BIAS_ALARM",
						  3: "CHAN_08_HIGH_TX_BIAS_ALARM",
						  6: "CHAN_09_LOW_TX_BIAS_ALARM",
						  7: "CHAN_09_HIGH_TX_BIAS_ALARM"},

					 18: {2: "CHAN_06_LOW_TX_BIAS_ALARM",
						  3: "CHAN_06_HIGH_TX_BIAS_ALARM",
						  6: "CHAN_07_LOW_TX_BIAS_ALARM",
						  7: "CHAN_07_HIGH_TX_BIAS_ALARM"},

					 19: {2: "CHAN_04_LOW_TX_BIAS_ALARM",
						  3: "CHAN_04_HIGH_TX_BIAS_ALARM",
						  6: "CHAN_05_LOW_TX_BIAS_ALARM",
						  7: "CHAN_05_HIGH_TX_BIAS_ALARM"},

					 20: {2: "CHAN_02_LOW_TX_BIAS_ALARM",
						  3: "CHAN_02_HIGH_TX_BIAS_ALARM",
						  6: "CHAN_03_LOW_TX_BIAS_ALARM",
						  7: "CHAN_03_HIGH_TX_BIAS_ALARM"},

					 21: {2: "CHAN_00_LOW_TX_BIAS_ALARM",
						  3: "CHAN_00_HIGH_TX_BIAS_ALARM",
						  6: "CHAN_01_LOW_TX_BIAS_ALARM",
						  7: "CHAN_01_HIGH_TX_BIAS_ALARM"},

					 22: {2: "CHAN_10_LOW_TX_PWR_ALARM",
						  3: "CHAN_10_HIGH_TX_PWR_ALARM",
						  6: "CHAN_11_LOW_TX_PWR_ALARM",
						  7: "CHAN_11_HIGH_TX_PWR_ALARM"},

					 23: {2: "CHAN_08_LOW_TX_PWR_ALARM",
						  3: "CHAN_08_HIGH_TX_PWR_ALARM",
						  6: "CHAN_09_LOW_TX_PWR_ALARM",
						  7: "CHAN_09_HIGH_TX_PWR_ALARM"},

					 24: {2: "CHAN_06_LOW_TX_PWR_ALARM",
						  3: "CHAN_06_HIGH_TX_PWR_ALARM",
						  6: "CHAN_07_LOW_TX_PWR_ALARM",
						  7: "CHAN_07_HIGH_TX_PWR_ALARM"},

					 25: {2: "CHAN_04_LOW_TX_PWR_ALARM",
						  3: "CHAN_04_HIGH_TX_PWR_ALARM",
						  6: "CHAN_05_LOW_TX_PWR_ALARM",
						  7: "CHAN_05_HIGH_TX_PWR_ALARM"},

					 26: {2: "CHAN_02_LOW_TX_PWR_ALARM",
						  3: "CHAN_02_HIGH_TX_PWR_ALARM",
						  6: "CHAN_03_LOW_TX_PWR_ALARM",
						  7: "CHAN_03_HIGH_TX_PWR_ALARM"},

					 27: {2: "CHAN_00_LOW_TX_PWR_ALARM",
						  3: "CHAN_00_HIGH_TX_PWR_ALARM",
						  6: "CHAN_01_LOW_TX_PWR_ALARM",
						  7: "CHAN_01_HIGH_TX_PWR_ALARM"}
						  }


		# Descriptions are used to automatically generate error messages by
		# matching with dictionary keys from read_status() method.


		# Dictionaries for storing raw data
		self.RX_MPOD = OrderedDict()
		self.TX_MPOD = OrderedDict()
		
		self.hw = hw
		self.device = device
		self.verbose = verbose
		if self.device == "trans_mpod":
			slaveAddress = 0x28
			self.deviceName = "transMpod"
		elif self.device == "recvr_mpod":
			slaveAddress = 0x30
			self.deviceName = "recvrMpod"
		else:
			print " [ERROR] invalid mpod type received by MiniPod Monitor constructor"
			sys.exit(" Exiting")
		# Create MiniPodControl instance
		self.bus = AXI_Interface(self.hw,self.device,slaveAddress,self.verbose)
		if self.verbose > 1:
			print " [INFO] Created AXI_Interface instance\n [INFO] Launching report writing method"
		self.get_report()

	def return_data(self):
		'''
		Method that returns the dictionaries in which the monitoring
		data is stored for use by other modules.
		'''
		if self.device == "recvr_mpod":
			return self.RX_MPOD
		elif self.device == "trans_mpod":
			return self.TX_MPOD


	def get_data(self, byteAdd, lenght , page = 0,raw = 0):
		'''
		Method to aquire a or a set of the miniPod register(s), the 0x00 upper
		memory page is selected by default.
		arguments : allData: set to 1 if the full 256 bytes are desired; page: set to 0x00 or 0x01
		depending on desired page, default is 0x00; raw: if set to 0 the method returns an int,
		if set to 1 it returns the list it receives from the AXI controler
		returns: integer reflecting the read value if raw = 0, the raw list of bytes if raw = 1
		'''
		if byteAdd + lenght > 0x7F:
			self.select_page(page)
		if self.verbose > 1:
			print "Getting data from AXI"
		dataIn = []
		# Read does not span over both registers, break into 2 
		if ((byteAdd + lenght > 0x7F) and (byteAdd <= 0x7F)):
			lenght1 = 0x80 - byteAdd
			lenght2 = lenght - 0x80
			dataIn1 = self.bus.Dyn_Read(byteAdd, lenght1)
			dataIn2 = self.bus.Dyn_Read(0x80, lenght2)
			dataIn.extend(dataIn1)
			dataIn.extend(dataIn2)

		else:
			dataIn = self.bus.Dyn_Read(byteAdd, lenght)
		if self.verbose > 2:
			print " The data was:"
			for value in range(0,len(dataIn)):
				print value, dataIn[value]

		if not raw:
			return self.bytes_to_int(dataIn[::-1])
		else:
			return dataIn



	def get_temp(self):
		'''
		Method that aquires the temperature bytes from the dataIn dictionary
		performs a 2's compliment on the MSB, converts the LSB into fracions of
		degrees, and prints the result to the terminal if verbosity > 0
		Arguments: none
		returns: 1 if successful
		'''
		if self.verbose > 1:
			print " [INFO] Reading MiniPod internal temperature"
		# First perform signed two's compliment on MSB
		inTemp = self.get_data(28,1)
		internalTempMSB = self.twos_comp_to_int(inTemp,8) # width is 8 bits as temp is coded on whole byte
		# Get the LSB
		internalTempLSB = self.get_data(29,1)
		# Convert to degrees C, the LSB stores the decimals on a scale of 0/256 to 255/256. Found in Xilinx documentation
		tempInFloat = float(internalTempLSB)/256 + internalTempMSB 
		if self.deviceName == "transMpod":
			self.TX_MPOD["TEMP_IN_C"] = tempInFloat
		else:
			# If not TX, then RX minipod
			self.RX_MPOD["TEMP_IN_C"] = tempInFloat
		return 1



	def select_page(self,page):
		'''
		Method to select the upper memory page to read/write. A page number
		(0 or 1) is passed to the method, and is written to the page select.
		Subsequent operations then take place from this page

		- Arguments: page - the page number to select
					 relevance - which device type(s) the method is relevant to
		- Returns 1 if succesful, 0 otherwise
		'''
		selectedPage = [page]
		self.bus.Dyn_Write(0x7F, selectedPage)



	def get_voltage(self, power_supply):
		'''
		Method that reads the supply voltages on the MiniPod and writes it to the output file
		and to the screen if requested by the user
		arguments: The power supply to read, designated by it's voltage (2.5 or 3.3)
		returns: 1 if successful, 0 otherwise
		'''
		if self.verbose > 1:
			print " [INFO] Reading {} Vcc Monitor".format(power_supply)
		# 3.3 V
		if power_supply == 3.3:
			voltage = self.get_data(32,2)/10000.0 # Conversion to Volts
			if self.deviceName == "transMpod":
				self.TX_MPOD["3.3V"] = voltage
			else:
				# If not TX, then RX minipod
				self.RX_MPOD["3.3V"] = voltage

		elif power_supply == 2.5:
			# 2.5 V
			voltage = self.get_data(34,2)/10000.0
			if self.deviceName == "transMpod":
				self.TX_MPOD["2.5V"] = voltage
			else:
				# If not TX, then RX minipod
				self.RX_MPOD["2.5V"] = voltage
		else:
			if self.verbose > 1:
				print " [WARNING] Encountered invalid power supply voltage request : {} Vcc in method get_voltage from class MiniPodMonitor, will not read"
			return 0
		return 1



	def get_optical_power(self):
		'''
		
		Method to read the received/output optical power of a given channel.
		Can provide output units in either uW (default, dB=0) or in dBm (if
		dBm=1). Note that if optical power is 0, units default to uW.
		Arguments: none
		Returns: a list containing the optical power for the 12 channels in
		increasing order, in units of dB
		'''
		power_out_list = []
		for channel in range(0,12):
			MSB_address = 86 - channel * 2
			raw_power = self.get_data(MSB_address, 2)
			if raw_power == 0:
				message_print = "\n Optical power for channel number {0} is: 0 dBm or 0 \xC2\xB5W, is module connected?".format(str(channel))
				message_write = "\n Optical power for channel number {0} is: 0 dBm or 0 \xB5W, is module connected?".format(str(channel))
				optical_power_dBm = optical_power_micro_w = 0
			else:
					# Calculate power in dB if required (and if possible)
					optical_power_dBm = 10 * mth.log10(raw_power * 0.0001)
					# Power in uW
					optical_power_micro_w = raw_power * 0.1

			if self.deviceName == "transMpod":
				self.TX_MPOD["OPTICAL_POW_CHAN_"+str(channel)+"_dB"] = optical_power_dBm
				self.TX_MPOD["OPTICAL_POW_CHAN_"+str(channel)+"_uW"] = optical_power_micro_w
			else:
				# If not TX, then RX minipod
				self.RX_MPOD["OPTICAL_POW_CHAN_"+str(channel)+"_dB"] = optical_power_dBm
				self.RX_MPOD["OPTICAL_POW_CHAN_"+str(channel)+"_uW"] = optical_power_micro_w
			
		
		return 1



	def get_serial_num(self):
		'''
		Method to read the serial number of MiniPod device, returns ASCII string
		arguments: none
		returns : serial number as ascii string
		'''
		if self.verbose > 1:
			print " [INFO] Getting serial numer for device: {}".format(self.device)
		# Get the number, bytes 189 to 204
		serial_number = self.get_data(189, 16, page = 0, raw = 1)
		serial_out = ''.join(chr(character) for character in serial_number)	
		if self.deviceName == "transMpod":
			self.TX_MPOD["SERIAL_NUM"] = serial_out
		else:
			# If not TX, then RX minipod
			self.RX_MPOD["SERIAL_NUM"] = serial_out
		return serial_number


	
	def get_operating_time(self):
		'''
		Method that reads the elapsed operating time of the MiniPod
		and logs it to the output file and writes it to the screen if requested by the user
		arguments: none
		returns: the up time in units of days
		'''
		if self.verbose > 1:
			print " [INFO] Getting ellapsed time on device: {}".format(self.device)
		time_elapsed = 2.0 * self.get_data(88,2)
		time_elapsed_days = time_elapsed/24.0

		if self.deviceName == "transMpod":
			self.TX_MPOD["UP_TIME_HOURS"] = time_elapsed
			self.TX_MPOD["UP_TIME_DAYS"] = time_elapsed_days
		else:
			# If not TX, then RX minipod
			self.RX_MPOD["UP_TIME_HOURS"] = time_elapsed
			self.RX_MPOD["UP_TIME_DAYS"] = time_elapsed_days

		return time_elapsed_days



	def get_basics(self):
		'''
		Return status for bits stored in the dictionaries found in constructor
		'''
		status = {}
		if self.deviceName == "transMpod":
			status_map = self.tx_status_map
		else:
			status_map = self.rx_status_map

		# For each byte in status map, fetch its value over I2C. Add all
		# bits of interest to the status dict, keyed by their identifier,
		# then return completed dictionary
		for byte in status_map:
			status_byte = self.get_data(byte,1)

			for bit in status_map[byte]:
				status[status_map[byte][bit]] = ((status_byte & (2 ** bit)) / 2 ** bit)
		return status



	def get_wavelength(self, write=1):
		
		'''
		Method to read the nominal wavelength of the laser

		- Arguments: deviation - includes the tolerance in the units if set
		- Returns nominal wavelength (nm)
		'''
		# Get wavelength as float
		if self.verbose > 1 and write:
			print " [INFO] Reading nominal wavelength"
		wavelength = (self.get_data(134, 2))/20.0# Divide by 20. as the documentation describes the wavelenght as being encoded in the following manner: wavelength = value/20
		
		tolerance = self.get_data(136, 2, page=0) / 200.0# Divide by 200, as described in minipod internal register description.

		if write==1:
			if self.deviceName == "transMpod":
				self.TX_MPOD["NOM_WAVELENGTH"] = wavelength
				self.TX_MPOD["WAVELENGTH_TOLERANCE"] = tolerance
			else:
				# If not TX, then RX minipod
				self.RX_MPOD["NOM_WAVELENGTH"] = wavelength
				self.RX_MPOD["WAVELENGTH_TOLERANCE"] = tolerance/200.0 
		
		return wavelength



	def get_type(self):
		'''
		Method that checks what type of miniPod is being monitored
		Arguments: none
		Returns: A string: "TX" if tx miniPod; "RX" if rx miniPod; if error occurs: returns 0
		'''
		if self.verbose > 0:
			print " [INFO] Checking the type of the minipod"
		# Get the nominal wavelength
		wavelength = self.get_wavelength(write = 0)
		if wavelength == 845:
			message = "\n The miniPod was identified as being of type TX"
			if self.verbose > 1:
				print " [INFO] the miniPod is of type TX"
			return "TX"
		elif wavelength == 0:
			message = "\n The miniPod was identified as being of type RX"
			if self.verbose > 1:
				print " [INFO] the miniPod is of type RX"
			return "RX"
		else:
			message = "\n [WARNING] The type of miniPod was not identified correctly!"
			if self.verbose > 1:
				print " [WARNING] Invalid nominal wavelength was read, cannot identify MiniPod type"
			return 0
		self.out_file.write(message)


	def get_report(self):
		'''
   		Method that writes the report to the output file by calling the different reporting
		methods 
		'''
		# Check miniPod type
		mpod_type = self.get_type()
		if (self.device == "trans_mpod" and mpod_type == "TX") or (self.device == "recvr_mpod" and mpod_type == "RX"):
			message = "MiniPod type verified to be: {}".format(self.device)
			if self.verbose > 0 :
				print " [INFO] " + message
		else:
			print " [ERROR] MiniPod type could not be verified by method get_type() in class MiniPodControl"
			sys.exit(" Exiting")
		# get and log internal temperature
		curr_temp = self.get_temp()
		# get and log 2.5 Vcc monitor 
		self.get_voltage(2.5)
		# get and log 3.3 Vcc monitor
		self.get_voltage(3.3)
		# get serial number 
		self.get_serial_num()
		# get operating time
		self.get_operating_time()
		# get Tx light ouput monitor values for all 12 channels
		self.get_optical_power()
		# get the wavelength and check that the correct device name was given
		self.get_wavelength()
		# perform basic report and log all values
		if self.deviceName == "transMpod":
			self.TX_MPOD["BASIC_REPORT"] = self.get_basics()
		else:
			self.RX_MPOD["BASIC_REPORT"] = self.get_basics()

		print " [INFO] Done!"	




class PowerModule(i2cDevices):
	'''
	Subclass of i2cDevices, generating an instance of this class will write
	a report of the desired power supplies and prints to the terminal if
	requested by the verbosity level being > 0.	
	'''
	

	# Power supply module dictinary associating device name with
	#I2C device
	modules = OrderedDict()
	
	
	def __init__(self, hw, device, verbose,device_type="HUB",in1=None,in2=None,in3=None,in4=None,in5=None,in6=None,in7=None):
		'''
		Constructor for class PowerModule
		'''
		specificDevice=[]
		specificDevice.append(in1)
		specificDevice.append(in2)
		specificDevice.append(in3)
		specificDevice.append(in4)
		specificDevice.append(in5)
		specificDevice.append(in6)
		specificDevice.append(in7)
		
		self.hw = hw
		self.deviceName = device
		self.verbose = verbose
		self.DATA_STORE = OrderedDict()
		
		if(device_type == "HUB"):
			### The following addresses are for the HUB power supplies
			self.modules['FPGA_CORE'] = 0x28
			self.modules['MGT_AVCC'] = 0x29
			self.modules['MGT_AVTT'] = 0x2A
			self.modules['SWCH_1V2'] = 0x2B
			self.modules['BULK_1V8'] = 0x2C
			self.modules['FAN_1V8'] = 0x2D
			self.modules['BULK_3V3'] = 0x2E

		if(device_type == "ROD"):
			### The following addresses are for the ROD power supplies
			self.modules['1V0'] = 0x1A
			self.modules['1V05'] = 0x1B
			self.modules['1V2'] = 0x1C
			self.modules['1V8'] = 0x1D
			self.modules['2V5'] = 0x1E
			self.modules['3V3'] = 0x1F

		# Check type of device
		if not self.deviceName == "hub_dcdc":
			print " [ERROR] invalid device type received by Power Module Monitor constructor"
			sys.exit(" Exiting")

		if self.verbose > 1:
			print " [INFO] Created AXI_Interface instance\n [INFO] Launching report writing method"
		if specificDevice[0]:
			i = 0
			print specificDevice
			for name in specificDevice:
				if name == None:
					break
				print "name : ", name
				self.deviceName = name
				if self.verbose > 0:
					print " ------ Report for device: {} ------".format(self.deviceName)
				print self.modules
				self.deviceAddress = self.modules[specificDevice[i]]
				self.bus = AXI_Interface(self.hw,"hub_dcdc",self.deviceAddress,self.verbose)
				self.bus.BUFFERS_Enable()
				self.get_report()
				i +=1
				if self.deviceName == "FPGA_CORE" : self.FPGA_CORE = self.DATA_STORE
				if self.deviceName == "MGT_AVCC" : self.MGT_AVCC = self.DATA_STORE
				if self.deviceName == "MGT_AVTT" : self.MGT_AVTT = self.DATA_STORE
				if self.deviceName == "SWCH_1V2" : self.SWCH_1V2 = self.DATA_STORE
				if self.deviceName == "BULK_1V8" : self.BULK_1V8 = self.DATA_STORE
				if self.deviceName == "FAN_1V8" : self.FAN_1V8 = self.DATA_STORE
				if self.deviceName == "BULK_3V3" : self.BULK_3V3 = self.DATA_STORE

		else:
			for name, address in self.modules.iteritems():
				self.deviceName = name
				self.deviceAddress = address
				# Create instance of AXI interface class, for I2C transactions
				self.bus = AXI_Interface(hw,"hub_dcdc",self.deviceAddress,self.verbose)
				# Enable and Disable the I2C buffers on the sensor bus as appropriate
				self.bus.BUFFERS_Enable()
				# Get the data from the power supply, and store it in the DATA_STORE dictionnary
				self.get_report()
				if self.deviceName == "FPGA_CORE" :
					self.DATA_STORE["DEVICE_NAME"] = "FPGA_CORE"
					self.FPGA_CORE = self.DATA_STORE.copy()
				if self.deviceName == "MGT_AVCC" :
					self.DATA_STORE["DEVICE_NAME"] = "MGT_AVCC"
					self.MGT_AVCC = self.DATA_STORE.copy()
				if self.deviceName == "MGT_AVTT" :
					self.DATA_STORE["DEVICE_NAME"] = "MGT_AVTT"
					self.MGT_AVTT = self.DATA_STORE.copy()
				if self.deviceName == "SWCH_1V2" :
					self.DATA_STORE["DEVICE_NAME"] = "SWCH_1V2"
					self.SWCH_1V2 = self.DATA_STORE.copy()
				if self.deviceName == "BULK_1V8" :
					self.DATA_STORE["DEVICE_NAME"] = "BULK_1V8"
					self.BULK_1V8 = self.DATA_STORE.copy()
				if self.deviceName == "FAN_1V8" :
					self.DATA_STORE["DEVICE_NAME"] = "FAN_1V8"
					self.FAN_1V8 = self.DATA_STORE.copy()
				if self.deviceName == "BULK_3V3" :
					self.DATA_STORE["DEVICE_NAME"] = "BULK_3V3"
					self.BULK_3V3 = self.DATA_STORE.copy()
		self.return_data()


	def return_data(self):
		out = [self.FPGA_CORE, self.MGT_AVCC, self.MGT_AVTT, self.SWCH_1V2, self.BULK_1V8, self.FAN_1V8, self.BULK_3V3]
		return out

			
	def get_data(self, byteAdd, lenght , raw = 0):
		'''
		Method to aquire a or a set of the miniPod register(s), the 0x00 upper
		memory page is selected by default.
		arguments : allData: set to 1 if the full 256 bytes are desired; page: set to 0x00 or 0x01
		depending on desired page, default is 0x00; raw: if set to 0 the method returns an int,
		if set to 1 it returns the list it receives from the AXI controler
		returns: integer reflecting the read value if raw = 0, the raw list of bytes if raw = 1
		'''
		dataIn = []
		dataIn = self.bus.Dyn_Read(byteAdd, lenght)
		if self.verbose > 2:
			print " The data was:"
			for value in range(0,len(dataIn)):
				print value, dataIn[value]
		
		if not raw:
			return self.bytes_to_int(dataIn)
		else:
			return dataIn


	def read_vIn(self, samples=1):
		'''
		Method that reads the input voltage of the current module using the READ_VIN command
		Arguments : samples - the number of readings to take
		returns: Float representing the measured output voltage
		'''
		vIn_meas = 0.
		if self.verbose > 1:
			print " Reading Input voltage"
		for i in range(samples):
			raw_data = self.get_data(0x88,2)
			vIn_meas += (self.get_decimal(raw_data,11))
		# Generate output and log to file
		mean_vIn = vIn_meas/samples
		self.DATA_STORE["V_IN"] = mean_vIn
		return mean_vIn


	def read_vout(self, samples=1):
		'''
		Method that reads the output voltage of the current module using the READ_VOUT command
		Arguments : samples - the number of readings to take
		returns: Float representing the measured output voltage
		'''
		vout_meas = 0.
		if self.verbose > 1:
			print " Reading Output voltage"
		for i in range(samples):
			raw_data = self.get_data(0x8B,2)
			
			vout_meas += (self.get_decimal(raw_data,16) * 2 ** -10)
		# Generate output and log to file
		mean_vout = vout_meas/samples
		self.DATA_STORE["V_OUT"] = mean_vout

		return mean_vout



	def read_iout(self, samples=1):
		'''
		Method that reads the output voltage of the current module using the READ_VOUT command
		Arguments : samples - the number of readings to take
		returns: Float representing the measured output voltage
		'''
		iout_meas = 0.
		
		if self.verbose > 1:
			print " Reading Output current"
		# Take measurements
		for i in range(samples):
			raw_data = self.get_data(0x8C,2)
			iout_meas += (self.get_decimal(raw_data,11))
		mean_iout = iout_meas/samples
		self.DATA_STORE["I_OUT"] = mean_iout

		return mean_iout



	def read_status(self):
		'''
		Method to read all status bits from a power module and return them
		in a dictionary. Each bit in the dictionary has a key identical to
		or very similar to the name given in the datasheet.

		Bits are calculated by converting the raw int into a string
		representation of the binary number, reversing it, then converting
		the value of the required bit to an int. The reverse is done, such that
		string indicies match bit numbers, to make things more readable.

		Keys in the returned status dictionary can be matched against keys
		in the descriptions dictionary to allow error messages to be easily
		generated.

		Note that it is assumed that a 1 in any status bit constitutes an
		error, hence the PGOOD signal is negated in the status registers.

		- Arguments: None
		- Returns dictionary of all status bits, keyed by datasheet name
		'''
		# Initialise dict to return
		status = {}

		# Get data from STATUS_VOUT: Overvoltage and undervoltage
		raw_data = self.get_data(0x7A, 1)
		binary_data = format(raw_data, '08b')[::-1]

		status['VOUT_OV'] = int(binary_data[7])
		status['VOUT_UV'] = int(binary_data[4])

		# Get data from STATUS_IOUT: Overcurrent fault and warning
		raw_data = self.get_data(0x7B, 1)
		binary_data = format(raw_data, '08b')[::-1]

		status['IOUT_OC'] = int(binary_data[7])
		status['IOUT_WARN'] = int(binary_data[5])

		# Get data from STATUS_TEMPERATURE: Overtemperature fault and warning
		raw_data = self.get_data(0x7D, 1)
		binary_data = format(raw_data, '08b')[::-1]

		status['OT_FAULT'] = int(binary_data[7])
		status['OT_WARN'] = int(binary_data[6])

		# Get data from STATUS_CML: Invalid command/data, packet error and
		# communications fault
		raw_data = self.get_data(0x7E, 1)
		binary_data = format(raw_data, '08b')[::-1]

		status['INV_COM'] = int(binary_data[7])
		status['INV_DAT'] = int(binary_data[6])
		status['PEC_ERR'] = int(binary_data[5])
		status['COM_FLT'] = int(binary_data[1])

		# Get data from STATUS_WORD: Power not good and module off
		raw_data = self.get_data(0x79, 2)
		binary_data = format(raw_data, '016b')[::-1]

		status['!PGOOD'] = int(binary_data[11])
		status['OFF'] = binary_data[6]
		
		return status
		
		


	def get_report(self):
		'''
   		Method that aquires the data for a report for the output class
		'''
		# Read and check status 
		status = self.read_status()
		
		# Read and log input voltage
		self.read_vIn()
		# Read and log output voltage
		self.read_vout()
		# Read and log output current
		self.read_iout()	
		
		self.DATA_STORE["REPORT"] = status



class SysMonitor():
	
	"""

	Class that interacts with the HUB or HTM system monitor to get FPGA temperatures and voltages

	"""
	def __init__(self, hw, device="sysmon",device_type = "HUB", verbose = 0, fileOut = 0):
		# Initialize class atributes
		self.hw = hw # Hw IPBus object
		self.device = device # This is used by IPBus to choose the correct IPBus node
		# NOTE This self.device attribute depends on the ipbus register map (in xml file), which is determined by
		# the firmware
		self.device_type = device_type # Is the board a HUB, HTM or ROD?
		self.verbose = verbose # Is terminal output desired? can be 0,1,2,3,4 depending on the amount of details desired
		self.fileOut = fileOut # Is file output desired? can be 0 (for no) or 1 (for yes)
		self.sysmon_DATA = OrderedDict() # Dictionary for passing Sysmon data to other modules (e.g. GUI)
		self.sysmon_DATA["DEVICE_TYPE"] = self.device_type
		# Start the sysmon and get report:
		self.callSysmon()
		# Return the dictionary to outside module
		self.return_data()
	
	def wrap_read(self,register):
		if self.verbose > 3:
			print "Reading register: ", register
		read_out = self.hw.getNode(self.device).getNode(register).read()
		self.hw.dispatch()
		if self.verbose > 2:
			print "Read register: ",register
		return int(read_out)

	
	def callSysmon(self):
	
		if self.device_type == "HUB":
	
			tempRead = self.wrap_read("temp_m")
			tempOut_m = (float(int(tempRead))/2**16)*501.3743-273.6777 # Conversion formula from ADC to deg C, from Xilinx doc
			tempRead = self.wrap_read("temp_0")
			tempOut_0 = (float(int(tempRead))/2**16)*503.975-273.15 # Likewise, ADC to deg C
			vIn = self.wrap_read("vccint_m")
			vIn_m = float(int(vIn))*3/2**16 # Conversion formula from ADC to Volts, found in Xilinx doc
			vIn = self.wrap_read("vccint_0")
			vIn_0 = float(int(vIn))*3/2**16
			vAux = self.wrap_read("vccaux_m")
			vAux_m = float(int(vAux))*3/2**16
			vAux = self.wrap_read("vccaux_0")
			vAux_0 = float(int(vAux))*3/2**16
			vBra = self.wrap_read("vbram_m")
			vBra_m = float(int(vBra))*3/2**16
			vBra = self.wrap_read("vbram_0")
			vBra_0 = float(int(vBra))*3/2**16
			
			# Add slave values to sysmon_DATA dictionary, the master values are added further bellow.
			
			self.sysmon_DATA["temp_0"] = tempOut_0
			self.sysmon_DATA["vIn_0"] = vIn_0
			self.sysmon_DATA["vAux_0"] = vAux_0
			self.sysmon_DATA["vBra_0"] = vBra_0
			
			
		if self.device_type == "HTM":
			
			tempRead = self.wrap_read("temp_m")
			tempOut_m = tempRead/(2**4)*503.975/4096-273.15
			vIn = self.wrap_read("vccint_m")
			vIn_m = float(int(vIn))*3/2**4/4096
			vAux = self.wrap_read("vccaux_m")
			vAux_m = float(int(vAux))*3/2**4/4096
			vBra = self.wrap_read("vbram_m")
			vBra_m = float(int(vBra))*3/2**4/4096
			# Note: the HTM sysmon does not have a slave_0
		
		# Add master sysmon values, this will be the HUB master values if the HUB sysmon is currently being read
		# or the HTM values if the HTM sysmon is being read.
		self.sysmon_DATA["temp_m"] = tempOut_m
		self.sysmon_DATA["vIn_m"] = vIn_m
		self.sysmon_DATA["vAux_m"] = vAux_m
		self.sysmon_DATA["vBra_m"] = vBra_m


	def return_data(self):
		return self.sysmon_DATA
