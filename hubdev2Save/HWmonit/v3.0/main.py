from source.Monitor import MiniPodMonitor
from source.Monitor import PowerModule
from source.Monitor import SysMonitor
from source.ConsolApp import ConsoleOutput
import os  # for manipulating directories
import sys
import uhal
from time import sleep
def check_dir():
	'''
	Simple function that checks that the starting directory is
	the home directory, so that IPBus can find Connection and 
	Address file can be found successfully
	'''
	
	if os.getcwd()[-4:] == "v3.0":
		if verbose > 2:
			print " Already in home dir."
	elif os.getcwd()[-3] == "source":
		os.chdir('../')
	elif os.getcwd()[-7:] == "Reports":
		os.chdir('../')
	else:
		#print " [ERROR] Could not find home directory to store output of monitor, exiting"
		#print " [INFO] Was in directory : ", os.getcwd()
		sys.exit(" Directory error")
	
def call_mpods(hw):
	'''
	Function that initiates the MiniPod monitoring
	'''
	if verbose > 1: print ("\n"+"-"*10+"Aquiring data from receiver miniPod"+"-"*10+"\n")
	RX_DATA = MiniPodMonitor(hw,"recvr_mpod",verbose).return_data()
	check_dir()
	if verbose > 1: print("\n"+ "-"*10+"-"*10+"Aquiring data from transmit minipod"+"-"*10+"\n")
	TX_DATA = MiniPodMonitor(hw,"trans_mpod",verbose).return_data()
	# Check that we are in home directory

	return [RX_DATA, TX_DATA]
	
def call_sysmon(hw,device="sysmon",device_type="HUB"):
	"""
	Function that initiates the Sysmon monitoring
	Return a dictionary which holds the Sysmon data
	Default behavior is to aquire the Sysmon data from a HUB card, using the hw ipbus object
	"""
	return SysMonitor(hw,device,device_type,verbose,fileOut).return_data()


def call_dcdc(hw,card_type="HUB"):
	check_dir()
	#print("\n"+"-"*10+"Aquiring data from DCDC power supplies"+"-"*10+"\n" )
	DCDC_DATA = PowerModule(hw,"hub_dcdc",verbose,card_type).return_data()
	return DCDC_DATA





# Create instance of Monitor and start data aquisition
# Slot based IPs
if config_settings["Hub_1_dcdc"] == 1 :
	data_dcdc_hub1 = call_dcdc(hw_Hub1,card_type="HUB")
if config_settings["Rod_1_dcdc"] == 1:
	data_dcdc_rod1 = call_dcdc(hw_Hub1,card_type="ROD")
if config_settings["Hub_1_mpods"] == 1:
	data_mpods_hub1 = call_mpods(hw_Hub1)
if config_settings["Hub_2_dcdc"] == 1:
	data_dcdc_hub2 = call_dcdc(hw_Hub2,card_type="HUB")
if config_settings["Rod_2_dcdc"] == 1:
	data_dcdc_rod2 = call_dcdc(hw_Hub2,card_type="ROD")
if config_settings["Hub_2_mpods"] == 1:
	data_mpods_hub2 = call_mpods(hw_Hub2)
if config_settings["Hub_1_sysmon"] == 1:
	sysmon_hub1 = call_sysmon(hw_Hub1)
if config_settings["Hub_2_sysmon"] == 1:
	sysmon_hub2 = call_sysmon(hw_Hub2)

# SN based IPs
Hub_dcdc_data = OrderedDict()
Hub_mpod_data = OrderedDict()
Rod_dcdc_data = OrderedDict()
Hub_sysmon_data = OrderedDict()
# Loop of hub slot based IPs and SN based IPs
for i in range(0,31):
	# Loop over DCDC reading requests
	if i < 10:
		hub_name = "HUB_SN0" + str(i)
	else:
		hub_name = "HUB_SN" + str(i)
	if( config_settings["Hub_SN_" + str(i) +  "_dcdc"] == 1):
		Hub_dcdc_data[hub_name] = call_dcdc( Hub_hw_dict[hub_name] ,card_type="HUB")
	if( config_settings["Hub_SN_" + str(i) + "_mpods"] == 1):
		Hub_mpod_data[hub_name] = call_mpods( Hub_hw_dict[hub_name])
	if( config_settings["Rod_SN_" + str(i) + "_dcdc"] == 1):
		Rod_dcdc_data[hub_name] = call_dcdc( Hub_hw_dict[hub_name] ,card_type="ROD")
	if( config_settings["Hub_SN_" + str(i) + "_sysmon"] == 1):
		Hub_sysmon_data[hub_name] = call_sysmon( Hub_hw_dict[hub_name] )


HTM_mpod_data = OrderedDict() # Dictionary for storing HTM data read by software
HTM_sysmon_data = OrderedDict()
# Process Slot based HTM IPs
for i in range(3,14):
	htm_name = "Htm_" + str(i)
	if (config_settings[ htm_name ] == 1):
		HTM_mpod_data[ htm_name ] = call_mpods( HTM_hw_dict[ htm_name ] )
		HTM_sysmon_data[ htm_name ] = call_sysmon( HTM_hw_dict[ htm_name ],"sysmon","HTM" )
# Process SN based HTM IPs
for i in range(0,21):
	if i < 10:
		htm_name = "HTM_SN0" + str(i)
	else:
		htm_name = "HTM_SN" + str(i)
	if (config_settings[ htm_name ] == 1):
		HTM_mpod_data[ htm_name ] = call_mpods( HTM_hw_dict[ htm_name ] )
		HTM_sysmon_data[ htm_name ] = call_sysmon( HTM_hw_dict[ htm_name ],"sysmon","HTM" )
	
# Create instance of ConsolApp, and start data output, as well as the system monitor
if verbose > 0 or fileOut > 0:
	
	if config_settings["Hub_1_dcdc"] == 1:
		ConsoleOutput("HUB1", "hub_dcdc", data_dcdc_hub1, verbose , fileOut)
	if config_settings["Hub_1_mpods"] == 1:
		ConsoleOutput("HUB1", "mini_pods", data_mpods_hub1, verbose, fileOut)
	if config_settings["Rod_1_dcdc"] == 1:
		ConsoleOutput("ROD1", "hub_dcdc", data_dcdc_rod1, verbose, fileOut)
	if config_settings["Hub_1_sysmon"] == 1:
		ConsoleOutput("HUB1", "sysmon", sysmon_hub1, verbose, fileOut)
	if config_settings["Hub_2_dcdc"] == 1:
		ConsoleOutput("HUB2", "hub_dcdc", data_dcdc_hub2, verbose, fileOut)
	if config_settings["Hub_2_mpods"] == 1:
		ConsoleOutput("HUB2", "mini_pods", data_mpods_hub2, verbose, fileOut)
	if config_settings["Rod_2_dcdc"] == 1:
		ConsoleOutput("ROD2", "hub_dcdc", data_dcdc_rod2, verbose, fileOut)
	if config_settings["Hub_2_sysmon"] == 1:
		ConsoleOutput("HUB2", "sysmon", sysmon_hub2, verbose, fileOut)
	# Implement SN based IP Hub outputs
	for i in range(0,31):
		if i < 10:
			hub_name = "HUB_SN0" + str(i)
		else:
			hub_name = "HUB_SN" + str(i)
		if( config_settings["Hub_SN_" + str(i) + "_dcdc"] == 1):
			ConsoleOutput(hub_name, "hub_dcdc", Hub_dcdc_data[hub_name], verbose, fileOut)
		if( config_settings["Hub_SN_" + str(i) + "_mpods"] == 1):
			ConsoleOutput(hub_name, "mini_pods", Hub_mpod_data[hub_name], verbose, fileOut)
		if( config_settings["Rod_SN_" + str(i) + "_dcdc"] == 1):
			ConsoleOutput(hub_name, "hub_dcdc", Rod_dcdc_data[hub_name], verbose, fileOut)
		if( config_settings["Hub_SN_" + str(i) + "_sysmon"] == 1):
			ConsoleOutput(hub_name, "sysmon", Hub_sysmon_data[hub_name], verbose, fileOut)
	# Implement slot based HTM output
	for i in range(3,14):
		htm_name = "Htm_" + str(i)
		if config_settings[ htm_name ] == 1:
			ConsoleOutput(htm_name, "mini_pods", HTM_mpod_data[ htm_name ], verbose, fileOut)
			ConsoleOutput(htm_name, "sysmon", HTM_sysmon_data[ htm_name ], verbose, fileOut)

	# Implement SN based HTM output
	for i in range(0,21):
		if i < 10:
			htm_name = "HTM_SN0" + str(i)
		else:
			htm_name = "HTM_SN" + str(i)
		if config_settings[ htm_name ] == 1:
			ConsoleOutput(htm_name, "mini_pods", HTM_mpod_data[ htm_name ], verbose, fileOut)
			ConsoleOutput(htm_name, "sysmon", HTM_sysmon_data[ htm_name ], verbose, fileOut)

