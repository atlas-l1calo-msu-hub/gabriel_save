# Time out between consecutive runs ( in seconds ):
if time_out < 0:
	time_out = 2

config_settings = {
# Hub slot based addressing
# Hub 1
"Hub_1_dcdc" : 0,
"Hub_1_mpods" : 0,
"Rod_1_dcdc" : 0,
"Hub_1_sysmon" : 0,

# Hub 2
"Hub_2_dcdc" : 0,
"Hub_2_mpods" : 0,
"Rod_2_dcdc" : 0,
"Hub_2_sysmon" : 0,

# Hub serial number based addressing
# Hub SN00
"Hub_SN_0_dcdc" : 0,
"Hub_SN_0_mpods" : 0,
"Rod_SN_0_dcdc" : 0,
"Hub_SN_0_sysmon" : 0,

# Hub SN01
"Hub_SN_1_dcdc" : 0,
"Hub_SN_1_mpods" : 0,
"Rod_SN_1_dcdc" : 0,
"Hub_SN_1_sysmon" : 0,

# Hub SN02
"Hub_SN_2_dcdc" : 0,
"Hub_SN_2_mpods" : 0,
"Rod_SN_2_dcdc" : 0,
"Hub_SN_2_sysmon" : 0,

# Hub SN03
"Hub_SN_3_dcdc" : 0,
"Hub_SN_3_mpods" : 0,
"Rod_SN_3_dcdc" : 0,
"Hub_SN_3_sysmon" : 0,

# Hub SN04
"Hub_SN_4_dcdc" : 1,
"Hub_SN_4_mpods" : 1,
"Rod_SN_4_dcdc" : 0,
"Hub_SN_4_sysmon" : 1,

# Hub SN05
"Hub_SN_5_dcdc" : 0,
"Hub_SN_5_mpods" : 0,
"Rod_SN_5_dcdc" : 0,
"Hub_SN_5_sysmon" : 0,

# Hub SN06
"Hub_SN_6_dcdc" : 0,
"Hub_SN_6_mpods" : 0,
"Rod_SN_6_dcdc" : 0,
"Hub_SN_6_sysmon" : 0,

# Hub SN07
"Hub_SN_7_dcdc" : 0,
"Hub_SN_7_mpods" : 0,
"Rod_SN_7_dcdc" : 0,
"Hub_SN_7_sysmon" : 0,

# Hub SN08
"Hub_SN_8_dcdc" : 0,
"Hub_SN_8_mpods" : 0,
"Rod_SN_8_dcdc" : 0,
"Hub_SN_8_sysmon" : 0,

# Hub SN09
"Hub_SN_9_dcdc" : 0,
"Hub_SN_9_mpods" : 0,
"Rod_SN_9_dcdc" : 0,
"Hub_SN_9_sysmon" : 0,

# Hub SN10
"Hub_SN_10_dcdc" : 0,
"Hub_SN_10_mpods" : 0,
"Rod_SN_10_dcdc" : 0,
"Hub_SN_10_sysmon" : 0,

# Hub SN11
"Hub_SN_11_dcdc" : 0,
"Hub_SN_11_mpods" : 0,
"Rod_SN_11_dcdc" : 0,
"Hub_SN_11_sysmon" : 0,

# Hub SN12
"Hub_SN_12_dcdc" : 0,
"Hub_SN_12_mpods" : 0,
"Rod_SN_12_dcdc" : 0,
"Hub_SN_12_sysmon" : 0,

# Hub SN13
"Hub_SN_13_dcdc" : 0,
"Hub_SN_13_mpods" :0,
"Rod_SN_13_dcdc" : 0,
"Hub_SN_13_sysmon" : 0,

# Hub SN14
"Hub_SN_14_dcdc" : 0,
"Hub_SN_14_mpods" : 0,
"Rod_SN_14_dcdc" : 0,
"Hub_SN_14_sysmon" : 0,

# Hub SN15
"Hub_SN_15_dcdc" : 0,
"Hub_SN_15_mpods" : 0,
"Rod_SN_15_dcdc" : 0,
"Hub_SN_15_sysmon" : 0,

# Hub SN16
"Hub_SN_16_dcdc" : 0,
"Hub_SN_16_mpods" : 0,
"Rod_SN_16_dcdc" : 0,
"Hub_SN_16_sysmon" : 0,

# Hub SN17
"Hub_SN_17_dcdc" : 0,
"Hub_SN_17_mpods" : 0,
"Rod_SN_17_dcdc" : 0,
"Hub_SN_17_sysmon" : 0,

# Hub SN18
"Hub_SN_18_dcdc" : 0,
"Hub_SN_18_mpods" : 0,
"Rod_SN_18_dcdc" : 0,
"Hub_SN_18_sysmon" : 0,

# Hub SN19
"Hub_SN_19_dcdc" : 0,
"Hub_SN_19_mpods" : 0,
"Rod_SN_19_dcdc" : 0,
"Hub_SN_19_sysmon" : 0,

# Hub SN20
"Hub_SN_20_dcdc" : 0,
"Hub_SN_20_mpods" : 0,
"Rod_SN_20_dcdc" : 0,
"Hub_SN_20_sysmon" : 0,

# Hub SN21
"Hub_SN_21_dcdc" : 0,
"Hub_SN_21_mpods" : 0,
"Rod_SN_21_dcdc" : 0,
"Hub_SN_21_sysmon" : 0,

# Hub SN22
"Hub_SN_22_dcdc" : 0,
"Hub_SN_22_mpods" : 0,
"Rod_SN_22_dcdc" : 0,
"Hub_SN_22_sysmon" : 0,

# Hub SN23
"Hub_SN_23_dcdc" : 0,
"Hub_SN_23_mpods" : 0,
"Rod_SN_23_dcdc" : 0,
"Hub_SN_23_sysmon" : 0,

# Hub SN24
"Hub_SN_24_dcdc" : 0,
"Hub_SN_24_mpods" : 0,
"Rod_SN_24_dcdc" : 0,
"Hub_SN_24_sysmon" : 0,

# Hub SN25
"Hub_SN_25_dcdc" : 0,
"Hub_SN_25_mpods" : 0,
"Rod_SN_25_dcdc" : 0,
"Hub_SN_25_sysmon" : 0,

# Hub SN26
"Hub_SN_26_dcdc" : 0,
"Hub_SN_26_mpods" : 0,
"Rod_SN_26_dcdc" : 0,
"Hub_SN_26_sysmon" : 0,

# Hub SN27
"Hub_SN_27_dcdc" : 0,
"Hub_SN_27_mpods" : 0,
"Rod_SN_27_dcdc" : 0,
"Hub_SN_27_sysmon" : 0,

# Hub SN28
"Hub_SN_28_dcdc" : 0,
"Hub_SN_28_mpods" : 0,
"Rod_SN_28_dcdc" : 0,
"Hub_SN_28_sysmon" : 0,

# Hub SN29
"Hub_SN_29_dcdc" : 0,
"Hub_SN_29_mpods" : 0,
"Rod_SN_29_dcdc" : 0,
"Hub_SN_29_sysmon" : 0,

# Hub SN30
"Hub_SN_30_dcdc" : 0,
"Hub_SN_30_mpods" : 0,
"Rod_SN_30_dcdc" : 0,
"Hub_SN_30_sysmon" : 0,



# HTMs
# Slot number based IPs
"Htm_3" : 0,
"Htm_4" : 0,
"Htm_5" : 0,
"Htm_6" : 0,
"Htm_7" : 0,
"Htm_8" : 0,
"Htm_9" : 0,
"Htm_10" : 0,
"Htm_11" : 0,
"Htm_12" : 0,
"Htm_13" : 0,

# Serial number based IPs
"HTM_SN00" : 0,
"HTM_SN01" : 0,
"HTM_SN02" : 0,
"HTM_SN03" : 0,
"HTM_SN04" : 0,
"HTM_SN05" : 0,
"HTM_SN06" : 0,
"HTM_SN07" : 0,
"HTM_SN08" : 0,
"HTM_SN09" : 0,
"HTM_SN10" : 0,
"HTM_SN11" : 0,
"HTM_SN12" : 0,
"HTM_SN13" : 0,
"HTM_SN14" : 0,
"HTM_SN15" : 0,
"HTM_SN16" : 0,
"HTM_SN17" : 0,
"HTM_SN18" : 0,
"HTM_SN19" : 0,
"HTM_SN20" : 0,

}

