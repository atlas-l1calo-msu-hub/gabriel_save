#! /bin/python2.7

#################################################################
"""
This program sets up the necessary ipbus objects, and runs the
monitoring software either a certain number of times, or 
indefinitely, as requested by the user.
"""
#################################################################

from time import sleep
import os
import sys
import getopt
import uhal
from time import sleep
import timeit 
from collections import OrderedDict


nTimes = -1
verbose = -1
fileOut = -1
no_of_runs_counter = 0
time_out = -1
infFlag = False
autoFlag = False

def usage():
	print(" Usage:  Monitor.py -h -i -o -v <verbosity level(0-4)> -r <number of runs (Integer)> -a")
	print("    -h| --help: Display this help message\n\
    -i| --inf: Request infinite run\n    -o| --out: Requests output file\n    -v| --verbose: Enter desired verbosity level (0 to 4)\n\
    -r| --runs: Enter desired number of runs (Integer > 0)\n    -d| --delay: Enter desired time delay between runs in seconds\n\
    -a| --auto: Automate execution of the script and generate a single output file (mostly for use with centralized testing)\n")
	print(" The verbosity scale is: ")
	print " 0 = Nothing to the screen, output to file if requested"
	print " 1 = monitoring information goes to the screen"
	print " 2 = Same as level 1 with the addition of information about the steps taken whilst writing the report"
	print " 3 = Same as level 2 with the addition of information about the steps taken whilst interacting with the I2C device"
	print " 4 = Debug level: Same as level 3 with the addition of information about the meaning of values\n read of the AXI registers whilst interacting with the I2C device"
try:
	opts, args = getopt.getopt(sys.argv[1:],"hiov:r:d:a",["help","indef","out","verbose=","runs=","delay=","auto"])
except getopt.GetoptError as err:
	# Print error message and exit
	print(" [ERROR] " + str(err) + "\n")
	usage()
	sys.exit(2)
for option,argument in opts:
	if( (option == "-h") or (option == "--help") ):
		usage()
		sys.exit()
	elif( (option == "-a") or (option == "--auto") ):
		print(" Automated run requested, output files will be parsed")
		autoFlag = True
		break
	elif( (option == "-i") or (option == "--indef") ):
		print(" Indefinite run requested, please use keyboard interrupt (ctrl+c) to end the run")
		infFlag = True # Tells software to run until interrupt is caught
	elif( (option == "-o") or (option == "--out") ):
		print(" Output files requested, data will be writen to files in the ./Reports/ directory")
		fileOut = 1
	elif( (option == "-v") or (option == "--verbose") ):
		try:
			verbose = abs(int(argument))
		except ValueError:
			# If the argument provided is not a number, ask for verbosity level again later
			print(" [WARNING] Invalid input for verbosity, will ask for user input")
			verbose = -1
		except:
			# Should never happen
			sys.exit(" [ERROR] Could not interpret user input for verbosity")
		if verbose >= 0:
			print(" Verbosity set to: " + str(verbose))
	elif( (option == "-r") or (option == "--runs") ):
		try:
			nTimes = abs(int(argument))
		except ValueError:
			# If not a number
			print(" [WARNING] Invalid input for number of runs requested, will ask for user input")
			nTimes = -1
		except:
			sys.exit(" [ERROR] Could not interpret user input for number of runs requested")
		if nTimes >= 0:
			infFlag = False
			print(" Number of runs requested: %d " % nTimes)
	elif( (option == "-d") or (option == "--delay") ):
		try:
			 time_out = abs(float(argument))
		except ValueError:
			print(" [WARNING] Invalid input for timeout between runs was entered, will use value defined in 'config.cmd' file")
			time_out = -1
		except:
			sys.exit(" [ERROR] Could not interpret user input for time delay between runs")
		if time_out > 0.0:
			print(" Time delay between runs requested: %1.3f seconds" % time_out)

if autoFlag:
	# set default values for auto run
	verbose = 0
	fileOut = 1
	nTimes = 1
	infFlag = False
	time_out = 0


if( (nTimes < 0) and (infFlag == False) ):
	print(" Please enter a number of times the software should run. type \"i/I\" for an inifinite amount of times")
	usrInput = raw_input("\n")
	try: 
		nTimes = int(usrInput)
		if nTimes < 0: raise(ValueError)
		infFlag = False
	except ValueError:
		nTimes = 0
		infFlag = True # Tell code to run indefinitely
		if not (usrInput in ("i","I")):
			sys.exit(" Sorry, invalid input, exiting")

def check_dir():
	'''
	Simple function that checks that the starting directory is
	the home directory, so that IPBus can find Connection and 
	Address file can be found successfully
	'''
	
	if os.getcwd()[-3:] == "DEV":
		if verbose > 2:
			print " Already in home dir."
	elif os.getcwd()[-3] == "source":
		os.chdir('../')
	elif os.getcwd()[-7:] == "Reports":
		os.chdir('../')
	else:
		print " [ERROR] Could not find home directory to store output of monitor, exiting"
		print " [INFO] Was in directory : ", os.getcwd()
		sys.exit(" Directory error")
check_dir()

# Get definitions from config file
time_out_flag = False
if time_out >= 0:
	time_out_save = time_out
	time_out_flag = True
execfile("config.cmd")
if time_out_flag:
	time_out = time_out_save
# Ask user for input:


while verbose < 0:
	print(" The verbosity scale is: ")
	print " 0 = Nothing to the screen, output to file if requested"
	print " 1 = monitoring information goes to the screen"
	print " 2 = Same as level 1 with the addition of information about the steps taken whilst writing the report"
	print " 3 = Same as level 2 with the addition of information about the steps taken whilst interacting with the I2C device"
	print " 4 = Debug level: Same as level 3 with the addition of information about the meaning of values\n read of the AXI registers whilst interacting with the I2C device"
	try:
		verbose = int(raw_input(" Enter desired verbosity level: "))
		break
	except ValueError:
		print " Non-number entered; please try again"

	if verbose < 0:
		print " Invalid verbosity level (negative), please try again"

if verbose > 4:
	print " Verbosity above 4 not valid, assuming verbosity = 4 desired"
	verbose = 4

if verbose > 0: print ' Checking for directory "./Reports" to store the reports\n generated by the monitor. '

if not os.path.exists("./Reports/"):
	os.makedirs("./Reports")
	if verbose > 0: print " Creating directory: ./Reports"
if verbose > 0: print '"./Reports" Directory is (now) present'

# Ask user if ouput files are desired
while(1):
	if (fileOut == 0 or fileOut ==1):
		break
	print(" Is writing monitoring data to an output file required?")
	try:
		fileOut = int(raw_input(" Enter 0 for NO file, 1 if file required: "))
	except ValueError:
		print " Non-number entered; please try again"
	if(fileOut !=0 or fileOut != 1):
		print(" [WARNING] Invalid value for file output parameter entered\n Please try again and enter 1 or 0")
# Setup IPBus
# Dictionary for use in main.py
Hub_hw_dict = OrderedDict()
# Process slot based HUB IPs requested by user
if(config_settings["Hub_1_dcdc"]==1 or config_settings["Hub_1_mpods"]==1 or config_settings["Rod_1_dcdc"]==1 or config_settings["Hub_1_sysmon"]==1):
	manager = uhal.ConnectionManager("file://ConnHub.xml")
	hw_Hub1 = manager.getDevice ( "HUB1" )
	Hub_hw_dict["hw_Hub1"] = hw_Hub1
	sleep(0.01)
	

if(config_settings["Hub_2_dcdc"]==1 or config_settings["Hub_2_mpods"]==1 or config_settings["Rod_2_dcdc"]==1 or config_settings["Hub_2_sysmon"]==1):
	manager = uhal.ConnectionManager("file://ConnHub.xml")
	hw_Hub2 = manager.getDevice ( "HUB2" )
	Hub_hw_dict["hw_Hub2"] =  hw_Hub2
	sleep(0.01)

# Process SN based HUB IPs
for i in range(0,31): # Currently 31 Hubs with SN ranging from 00 to 30
	hub_label_dcdc = "Hub_SN_" + str(i) + "_dcdc"
	hub_label_mpods = "Hub_SN_" + str(i) + "_mpods"
	rod_label_dcdc = "Rod_SN_" + str(i) + "_dcdc"
	hub_label_sysmon = "Hub_SN_" + str(i) + "_sysmon"

	if ( (config_settings[ hub_label_dcdc ] == 1) or (config_settings[ hub_label_mpods ] == 1) or (config_settings[ rod_label_dcdc ] == 1) or (config_settings[hub_label_sysmon] == 1) ):
		manager = uhal.ConnectionManager("file://ConnHub.xml")
		if i < 10:
			hub_name = "HUB_SN0" + str(i)
		else:
			hub_name = "HUB_SN" + str(i)
		Curr_hub_hw = manager.getDevice(hub_name)
		Hub_hw_dict[hub_name] = Curr_hub_hw
		sleep(0.01)


# Dictionary for use in main.py program:
HTM_hw_dict = OrderedDict()
# Process Slot based HTM IPs
for i in range(3,14):
	htm_label = "Htm_" + str(i)
	if (config_settings[ htm_label ] == 1):
		manager = uhal.ConnectionManager("file://ConnHub.xml")
		htm_name = "HTM" + str(i)
		Curr_htm_hw = manager.getDevice ( htm_name )
		HTM_hw_dict[ htm_label ] = Curr_htm_hw

# Process SN based HTM IPs
for i in range(0,21):
	if i < 10:
		htm_name = "HTM_SN0" + str(i)
	else:
		htm_name = "HTM_SN" + str(i)
	if (config_settings[ htm_name ] == 1):
		manager = uhal.ConnectionManager("file://ConnHub.xml")
		Curr_htm_hw = manager.getDevice ( htm_name )
		HTM_hw_dict[ htm_name ] = Curr_htm_hw


check_dir()
execfile("main.py")	
"""
while 1:
	check_dir()
	#execfile("main.py")
	
	try:	
		execfile("main.py")
	except KeyboardInterrupt:
		try:
			print( " Are you sure you want to exit? (ctrl+c to exit, press enter to resume)\n")
			x = raw_input()
		except KeyboardInterrupt:
			sys.exit(" Exiting due to user request")

	except:
		try:
			print(" [WARNING] Trying to recover from unknown error")
			check_dir()
			print(" [INFO] Attempting to run main.py a second time")
			execfile("main.py")
		except:
			print(" [ERROR] Reading card failed twice, exiting")
			print(" Please attempt running code with a different configuration in file \"config.cmd\"")
			sys.exit()	
	
	no_of_runs_counter += 1
	# Implement time out between runs:
	#print(" No of successful runs: %d" % no_of_runs_counter)
	check_dir()
	log_file = open("Runs_Tracker.txt","w")
	out_str = " No of successful runs: " + str(no_of_runs_counter) + "\n"
	log_file.write(out_str)
	if( (no_of_runs_counter >= nTimes) and not infFlag):
		sys.exit("Number of requested runs reached, exiting")
	try:
		sleep(time_out)
	except KeyboardInterrupt:
		try:
			print( " Are you sure you want to exit? (ctrl+c to exit, press enter to resume)\n")
			x = raw_input()
		except KeyboardInterrupt:
			sys.exit(" Exiting due to user request")
"""
