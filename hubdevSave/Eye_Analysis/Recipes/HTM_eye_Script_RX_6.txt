1. Configuration

set_property PROBES.FILE {/home/hubuser/FW_releases/HTM/testing/PreProduction/bitstream/v1.1/htm_run7.ltx} [get_hw_devices xc7z035_1]
set_property FULL_PROBES.FILE {/home/hubuser/FW_releases/HTM/testing/PreProduction/bitstream/v1.1/htm_run7.ltx} [get_hw_devices xc7z035_1]
set_property PROGRAM.FILE {/home/hubuser/FW_releases/HTM/testing/PreProduction/bitstream/v1.1/htm_run7.bit} [get_hw_devices xc7z035_1]
program_hw_devices [get_hw_devices xc7z035_1]



2. FPGA Refresh

set_property PROBES.FILE {/home/hubuser/FW_releases/HTM/testing/PreProduction/bitstream/v1.1/htm_run7.ltx} [get_hw_devices xc7z035_1]
set_property FULL_PROBES.FILE {/home/hubuser/FW_releases/HTM/testing/PreProduction/bitstream/v1.1/htm_run7.ltx} [get_hw_devices xc7z035_1]
refresh_hw_device [lindex [get_hw_devices xc7z035_1] 0]
display_hw_ila_data [ get_hw_ila_data hw_ila_data_1 -of_objects [get_hw_ilas -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/ilaEye"}]]
display_hw_ila_data [ get_hw_ila_data hw_ila_data_2 -of_objects [get_hw_ilas -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/ila_ShadowReg_gen.ila_ShadowReg_INST"}]]

display_hw_ila_data [ get_hw_ila_data hw_ila_data_3 -of_objects [get_hw_ilas -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[9].mgt_inst/EyeScan_gen.eyeScan_INST/ilaEye"}]]
display_hw_ila_data [ get_hw_ila_data hw_ila_data_4 -of_objects [get_hw_ilas -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[9].mgt_inst/ila_ShadowReg_gen.ila_ShadowReg_INST"}]]


3. Set the Prescale

set_property OUTPUT_VALUE 00 [get_hw_probes {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/prescale_1} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/drpCntrlIns"}]]
commit_hw_vio [get_hw_probes {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/prescale_1} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/drpCntrlIns"}]]

4. Set the trigger Condition

set_property TRIGGER_COMPARE_VALUE eq9'h03C [get_hw_probes {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/drpAddr} -of_objects [get_hw_ilas -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/ilaEye"}]]

set_property TRIGGER_COMPARE_VALUE eq9'h03C [get_hw_probes {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/drpAddr} -of_objects [get_hw_ilas -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/ilaEye"}]]
run_hw_ila [get_hw_ilas -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/ilaEye"}]

add_wave -into {hw_ila_data_1.wcfg} -radix hex { {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/drpAddr} {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/errors} {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/horz} {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/prescale} {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/samples} {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/state} {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/vert} }

set_property CONTROL.TRIGGER_POSITION 50 [get_hw_ilas -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/ilaEye"}]


4. Run the Python Script

/home/hubuser/Documents/Pawel_Work/IBERT_testing/IBERT_IPBus_Git/python prbssel_test_v1p7.py



5. Start the RUN

set_property CONTROL.DATA_DEPTH 8192 [get_hw_ilas -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/ilaEye"}]
set_property OUTPUT_VALUE 0 [get_hw_probes {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/go_vio} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/drpCntrlIns"}]]
commit_hw_vio [get_hw_probes {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/go_vio} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/drpCntrlIns"}]]
set_property OUTPUT_VALUE 1 [get_hw_probes {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/go_vio} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/drpCntrlIns"}]]
commit_hw_vio [get_hw_probes {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/go_vio} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/drpCntrlIns"}]]

6 Stop the RUN

set_property OUTPUT_VALUE 0 [get_hw_probes {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/go_vio} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/drpCntrlIns"}]]
commit_hw_vio [get_hw_probes {gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/go_vio} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/drpCntrlIns"}]]

7. Stop the ILA

wait_on_hw_ila -timeout 0 [get_hw_ilas -of_objects [get_hw_devices xc7z035_1] -filter {CELL_NAME=~"gen[6].mgt_inst/EyeScan_gen.eyeScan_INST/ilaEye"}] 

8. Dump the Data

write_hw_ila_data -csv_file -force {/home/hubuser/Documents/Gabriel_Work/Eye_Analysis/data/HTM14/iladata.csv} hw_ila_data_1


