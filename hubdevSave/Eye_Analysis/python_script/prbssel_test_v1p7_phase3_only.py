import uhal
import bit_operations_v1p2 as bit_operations
from time import sleep
import sys






if __name__ == "__main__":


    if len(sys.argv) < 2:
        log_name = "trash.txt"
    else:
        log_name = sys.argv[1]

    log = bit_operations.Log("Results/" + log_name)    
 
    manager = uhal.ConnectionManager("file://ConnHub.xml")

    hub1 = bit_operations.Device(manager.getDevice("HUB1"))
    hub2 = bit_operations.Device(manager.getDevice("HUB2"))
    htm3 = bit_operations.Device(manager.getDevice("HTM3"))
    htm4 = bit_operations.Device(manager.getDevice("HTM4"))
    htm5 = bit_operations.Device(manager.getDevice("HTM5"))
    htm6 = bit_operations.Device(manager.getDevice("HTM6"))
    htm7 = bit_operations.Device(manager.getDevice("HTM7"))
    htm8 = bit_operations.Device(manager.getDevice("HTM8"))
    htm9 = bit_operations.Device(manager.getDevice("HTM9"))
    htm10 = bit_operations.Device(manager.getDevice("HTM10"))
    htm11 = bit_operations.Device(manager.getDevice("HTM11"))
    htm12 = bit_operations.Device(manager.getDevice("HTM12"))
    htm13 = bit_operations.Device(manager.getDevice("HTM13"))
    htm14 = bit_operations.Device(manager.getDevice("HTM14"))

    device_dict = {"hub1":hub1, "hub2":hub2,
                   "htm3":htm3, "htm4":htm4, "htm5":htm5, "htm6":htm6, "htm7":htm7, "htm8":htm8,
                   "htm9":htm9, "htm10":htm10, "htm11":htm11, "htm12":htm12, "htm13":htm13,
                   "htm14":htm14,
                  }


    # Things to add: single channel addressing, command line arguments
    #                single actions
    
    hub_test_list = [
                 # All Hub Channels in decreasing order
                 ((hub1, "GT_ch79" ), (hub1, "GT_ch79", )),
                 ((hub1, "GT_ch78" ), (hub1, "GT_ch78", )),
                 ((hub1, "GT_ch77" ), (hub1, "GT_ch77", )),
                 ((hub1, "GT_ch76" ), (hub1, "GT_ch76", )),
                 ((hub1, "GT_ch75" ), (hub1, "GT_ch75", )),
                 ((hub1, "GT_ch74" ), (hub1, "GT_ch74", )),
                 ((hub1, "GT_ch73" ), (hub1, "GT_ch73", )),
                 ((hub1, "GT_ch72" ), (hub1, "GT_ch72", )),
                 ((hub1, "GT_ch71" ), (hub1, "GT_ch71", )),
                 ((hub1, "GT_ch70" ), (hub1, "GT_ch70", )),

                 ((hub1, "GT_ch69" ), (hub1, "GT_ch69", )),
                 ((hub1, "GT_ch68" ), (hub1, "GT_ch68", )),
                 ((hub1, "GT_ch67" ), (hub1, "GT_ch67", )),
                 ((hub1, "GT_ch66" ), (hub1, "GT_ch66", )),
                 ((hub1, "GT_ch65" ), (hub1, "GT_ch65", )),
                 ((hub1, "GT_ch64" ), (hub1, "GT_ch64", )),
                 ((hub1, "GT_ch63" ), (hub1, "GT_ch63", )),
                 ((hub1, "GT_ch62" ), (hub1, "GT_ch62", )),
                 ((hub1, "GT_ch61" ), (hub1, "GT_ch61", )),
                 ((hub1, "GT_ch60" ), (hub1, "GT_ch60", )),

                 ((hub1, "GT_ch59" ), (hub1, "GT_ch59", )),
                 ((hub1, "GT_ch58" ), (hub1, "GT_ch58", )),
                 ((hub1, "GT_ch57" ), (hub1, "GT_ch57", )),
                 ((hub1, "GT_ch56" ), (hub1, "GT_ch56", )),
                 ((hub1, "GT_ch55" ), (hub1, "GT_ch55", )),
                 ((hub1, "GT_ch54" ), (hub1, "GT_ch54", )),
                 ((hub1, "GT_ch53" ), (hub1, "GT_ch53", )),
                 ((hub1, "GT_ch52" ), (hub1, "GT_ch52", )),
                 ((hub1, "GT_ch51" ), (hub1, "GT_ch51", )),
                 ((hub1, "GT_ch50" ), (hub1, "GT_ch50", )),

                 ((hub1, "GT_ch49" ), (hub1, "GT_ch49", )),
                 ((hub1, "GT_ch48" ), (hub1, "GT_ch48", )),
                 ((hub1, "GT_ch47" ), (hub1, "GT_ch47", )),
                 ((hub1, "GT_ch46" ), (hub1, "GT_ch46", )),
                 ((hub1, "GT_ch45" ), (hub1, "GT_ch45", )),
                 ((hub1, "GT_ch44" ), (hub1, "GT_ch44", )),
                 ((hub1, "GT_ch43" ), (hub1, "GT_ch43", )),
                 ((hub1, "GT_ch42" ), (hub1, "GT_ch42", )),
                 ((hub1, "GT_ch41" ), (hub1, "GT_ch41", )),
                 ((hub1, "GT_ch40" ), (hub1, "GT_ch40", )),

                 ((hub1, "GT_ch39" ), (hub1, "GT_ch39", )),
                 ((hub1, "GT_ch38" ), (hub1, "GT_ch38", )),
                 ((hub1, "GT_ch37" ), (hub1, "GT_ch37", )),
                 ((hub1, "GT_ch36" ), (hub1, "GT_ch36", )),
                 ((hub1, "GT_ch35" ), (hub1, "GT_ch35", )),
                 ((hub1, "GT_ch34" ), (hub1, "GT_ch34", )),
                 ((hub1, "GT_ch33" ), (hub1, "GT_ch33", )),
                 ((hub1, "GT_ch32" ), (hub1, "GT_ch32", )),
                 ((hub1, "GT_ch31" ), (hub1, "GT_ch31", )),
                 ((hub1, "GT_ch30" ), (hub1, "GT_ch30", )),

                 ((hub1, "GT_ch29" ), (hub1, "GT_ch29", )),
                 ((hub1, "GT_ch28" ), (hub1, "GT_ch28", )),
                 ((hub1, "GT_ch27" ), (hub1, "GT_ch27", )),
                 ((hub1, "GT_ch26" ), (hub1, "GT_ch26", )),
                 ((hub1, "GT_ch25" ), (hub1, "GT_ch25", )),
                 ((hub1, "GT_ch24" ), (hub1, "GT_ch24", )),
                 ((hub1, "GT_ch23" ), (hub1, "GT_ch23", )),
                 ((hub1, "GT_ch22" ), (hub1, "GT_ch22", )),
                 ((hub1, "GT_ch21" ), (hub1, "GT_ch21", )),
                 ((hub1, "GT_ch20" ), (hub1, "GT_ch20", )),

                 ((hub1, "GT_ch19" ), (hub1, "GT_ch19", )),
                 ((hub1, "GT_ch18" ), (hub1, "GT_ch18", )),
                 ((hub1, "GT_ch17" ), (hub1, "GT_ch17", )),
                 ((hub1, "GT_ch16" ), (hub1, "GT_ch16", )),
                 ((hub1, "GT_ch15" ), (hub1, "GT_ch15", )),
                 ((hub1, "GT_ch14" ), (hub1, "GT_ch14", )),
                 ((hub1, "GT_ch13" ), (hub1, "GT_ch13", )),
                 ((hub1, "GT_ch12" ), (hub1, "GT_ch12", )),
                 ((hub1, "GT_ch11" ), (hub1, "GT_ch11", )),
                 ((hub1, "GT_ch10" ), (hub1, "GT_ch10", )),

                 ((hub1, "GT_ch9"  ), (hub1, "GT_ch9", )),
                 ((hub1, "GT_ch8"  ), (hub1, "GT_ch8", )),
                 ((hub1, "GT_ch7"  ), (hub1, "GT_ch7", )),
                 ((hub1, "GT_ch6"  ), (hub1, "GT_ch6", )),
                 ((hub1, "GT_ch5"  ), (hub1, "GT_ch5", )),
                 ((hub1, "GT_ch4"  ), (hub1, "GT_ch4", )),
                 ((hub1, "GT_ch3"  ), (hub1, "GT_ch3", )),
                 ((hub1, "GT_ch2"  ), (hub1, "GT_ch2", )),
                 ((hub1, "GT_ch1"  ), (hub1, "GT_ch1", )),
                 ((hub1, "GT_ch0"  ), (hub1, "GT_ch0", )),

                 ((hub2, "GT_ch79" ), (hub2, "GT_ch79", )),
                 ((hub2, "GT_ch78" ), (hub2, "GT_ch78", )),
                 ((hub2, "GT_ch77" ), (hub2, "GT_ch77", )),
                 ((hub2, "GT_ch76" ), (hub2, "GT_ch76", )),
                 ((hub2, "GT_ch75" ), (hub2, "GT_ch75", )),
                 ((hub2, "GT_ch74" ), (hub2, "GT_ch74", )),
                 ((hub2, "GT_ch73" ), (hub2, "GT_ch73", )),
                 ((hub2, "GT_ch72" ), (hub2, "GT_ch72", )),
                 ((hub2, "GT_ch71" ), (hub2, "GT_ch71", )),
                 ((hub2, "GT_ch70" ), (hub2, "GT_ch70", )),

                 ((hub2, "GT_ch69" ), (hub2, "GT_ch69", )),
                 ((hub2, "GT_ch68" ), (hub2, "GT_ch68", )),
                 ((hub2, "GT_ch67" ), (hub2, "GT_ch67", )),
                 ((hub2, "GT_ch66" ), (hub2, "GT_ch66", )),
                 ((hub2, "GT_ch65" ), (hub2, "GT_ch65", )),
                 ((hub2, "GT_ch64" ), (hub2, "GT_ch64", )),
                 ((hub2, "GT_ch63" ), (hub2, "GT_ch63", )),
                 ((hub2, "GT_ch62" ), (hub2, "GT_ch62", )),
                 ((hub2, "GT_ch61" ), (hub2, "GT_ch61", )),
                 ((hub2, "GT_ch60" ), (hub2, "GT_ch60", )),

                 ((hub2, "GT_ch59" ), (hub2, "GT_ch59", )),
                 ((hub2, "GT_ch58" ), (hub2, "GT_ch58", )),
                 ((hub2, "GT_ch57" ), (hub2, "GT_ch57", )),
                 ((hub2, "GT_ch56" ), (hub2, "GT_ch56", )),
                 ((hub2, "GT_ch55" ), (hub2, "GT_ch55", )),
                 ((hub2, "GT_ch54" ), (hub2, "GT_ch54", )),
                 ((hub2, "GT_ch53" ), (hub2, "GT_ch53", )),
                 ((hub2, "GT_ch52" ), (hub2, "GT_ch52", )),
                 ((hub2, "GT_ch51" ), (hub2, "GT_ch51", )),
                 ((hub2, "GT_ch50" ), (hub2, "GT_ch50", )),

                 ((hub2, "GT_ch49" ), (hub2, "GT_ch49", )),
                 ((hub2, "GT_ch48" ), (hub2, "GT_ch48", )),
                 ((hub2, "GT_ch47" ), (hub2, "GT_ch47", )),
                 ((hub2, "GT_ch46" ), (hub2, "GT_ch46", )),
                 ((hub2, "GT_ch45" ), (hub2, "GT_ch45", )),
                 ((hub2, "GT_ch44" ), (hub2, "GT_ch44", )),
                 ((hub2, "GT_ch43" ), (hub2, "GT_ch43", )),
                 ((hub2, "GT_ch42" ), (hub2, "GT_ch42", )),
                 ((hub2, "GT_ch41" ), (hub2, "GT_ch41", )),
                 ((hub2, "GT_ch40" ), (hub2, "GT_ch40", )),

                 ((hub2, "GT_ch39" ), (hub2, "GT_ch39", )),
                 ((hub2, "GT_ch38" ), (hub2, "GT_ch38", )),
                 ((hub2, "GT_ch37" ), (hub2, "GT_ch37", )),
                 ((hub2, "GT_ch36" ), (hub2, "GT_ch36", )),
                 ((hub2, "GT_ch35" ), (hub2, "GT_ch35", )),
                 ((hub2, "GT_ch34" ), (hub2, "GT_ch34", )),
                 ((hub2, "GT_ch33" ), (hub2, "GT_ch33", )),
                 ((hub2, "GT_ch32" ), (hub2, "GT_ch32", )),
                 ((hub2, "GT_ch31" ), (hub2, "GT_ch31", )),
                 ((hub2, "GT_ch30" ), (hub2, "GT_ch30", )),

                 ((hub2, "GT_ch29" ), (hub2, "GT_ch29", )),
                 ((hub2, "GT_ch28" ), (hub2, "GT_ch28", )),
                 ((hub2, "GT_ch27" ), (hub2, "GT_ch27", )),
                 ((hub2, "GT_ch26" ), (hub2, "GT_ch26", )),
                 ((hub2, "GT_ch25" ), (hub2, "GT_ch25", )),
                 ((hub2, "GT_ch24" ), (hub2, "GT_ch24", )),
                 ((hub2, "GT_ch23" ), (hub2, "GT_ch23", )),
                 ((hub2, "GT_ch22" ), (hub2, "GT_ch22", )),
                 ((hub2, "GT_ch21" ), (hub2, "GT_ch21", )),
                 ((hub2, "GT_ch20" ), (hub2, "GT_ch20", )),

                 ((hub2, "GT_ch19" ), (hub2, "GT_ch19", )),
                 ((hub2, "GT_ch18" ), (hub2, "GT_ch18", )),
                 ((hub2, "GT_ch17" ), (hub2, "GT_ch17", )),
                 ((hub2, "GT_ch16" ), (hub2, "GT_ch16", )),
                 ((hub2, "GT_ch15" ), (hub2, "GT_ch15", )),
                 ((hub2, "GT_ch14" ), (hub2, "GT_ch14", )),
                 ((hub2, "GT_ch13" ), (hub2, "GT_ch13", )),
                 ((hub2, "GT_ch12" ), (hub2, "GT_ch12", )),
                 ((hub2, "GT_ch11" ), (hub2, "GT_ch11", )),
                 ((hub2, "GT_ch10" ), (hub2, "GT_ch10", )),

                 ((hub2, "GT_ch9"  ), (hub2, "GT_ch9", )),
                 ((hub2, "GT_ch8"  ), (hub2, "GT_ch8", )),
                 ((hub2, "GT_ch7"  ), (hub2, "GT_ch7", )),
                 ((hub2, "GT_ch6"  ), (hub2, "GT_ch6", )),
                 ((hub2, "GT_ch5"  ), (hub2, "GT_ch5", )),
                 ((hub2, "GT_ch4"  ), (hub2, "GT_ch4", )),
                 ((hub2, "GT_ch3"  ), (hub2, "GT_ch3", )),
                 ((hub2, "GT_ch2"  ), (hub2, "GT_ch2", )),
                 ((hub2, "GT_ch1"  ), (hub2, "GT_ch1", )),
                 ((hub2, "GT_ch0"  ), (hub2, "GT_ch0", )),
                 ]

    htm_test_list = [
                 # HTM Channels in decreasing order
                 ((htm3, "GT_ch15"  ), (htm3,  "GT_ch15" )),  
                 ((htm3, "GT_ch14"  ), (htm3,  "GT_ch14" )),  
                 ((htm3, "GT_ch13"  ), (htm3,  "GT_ch13" )),  
                 ((htm3, "GT_ch12"  ), (htm3,  "GT_ch12" )),   
                 ((htm3, "GT_ch11"  ), (htm3,  "GT_ch11" )), 
                 ((htm3, "GT_ch10"  ), (htm3,  "GT_ch10" )), 
                 ((htm3, "GT_ch9"   ), (htm3,  "GT_ch9"  )), 
                 ((htm3, "GT_ch8"   ), (htm3,  "GT_ch8"  )), 
                 ((htm3, "GT_ch7"   ), (htm3,  "GT_ch7"  )), 
                 ((htm3, "GT_ch6"   ), (htm3,  "GT_ch6"  )), 
                 ((htm3, "GT_ch5"   ), (htm3,  "GT_ch5"  )), 
                 ((htm3, "GT_ch4"   ), (htm3,  "GT_ch4"  )), 
                 ((htm3, "GT_ch3"   ), (htm3,  "GT_ch3"  )), 
                 ((htm3, "GT_ch2"   ), (htm3,  "GT_ch2"  )), 
                 ((htm3, "GT_ch1"   ), (htm3,  "GT_ch1"  )), 
                 ((htm3, "GT_ch0"   ), (htm3,  "GT_ch0"  )), 

                 ((htm4, "GT_ch15"  ), (htm4,  "GT_ch15" )),  
                 ((htm4, "GT_ch14"  ), (htm4,  "GT_ch14" )),  
                 ((htm4, "GT_ch13"  ), (htm4,  "GT_ch13" )),  
                 ((htm4, "GT_ch12"  ), (htm4,  "GT_ch12" )),   
                 ((htm4, "GT_ch11"  ), (htm4,  "GT_ch11" )), 
                 ((htm4, "GT_ch10"  ), (htm4,  "GT_ch10" )), 
                 ((htm4, "GT_ch9"   ), (htm4,  "GT_ch9"  )), 
                 ((htm4, "GT_ch8"   ), (htm4,  "GT_ch8"  )), 
                 ((htm4, "GT_ch7"   ), (htm4,  "GT_ch7"  )), 
                 ((htm4, "GT_ch6"   ), (htm4,  "GT_ch6"  )), 
                 ((htm4, "GT_ch5"   ), (htm4,  "GT_ch5"  )), 
                 ((htm4, "GT_ch4"   ), (htm4,  "GT_ch4"  )), 
                 ((htm4, "GT_ch3"   ), (htm4,  "GT_ch3"  )), 
                 ((htm4, "GT_ch2"   ), (htm4,  "GT_ch2"  )), 
                 ((htm4, "GT_ch1"   ), (htm4,  "GT_ch1"  )), 
                 ((htm4, "GT_ch0"   ), (htm4,  "GT_ch0"  )), 

                 ((htm5, "GT_ch15"  ), (htm5,  "GT_ch15" )),  
                 ((htm5, "GT_ch14"  ), (htm5,  "GT_ch14" )),  
                 ((htm5, "GT_ch13"  ), (htm5,  "GT_ch13" )),  
                 ((htm5, "GT_ch12"  ), (htm5,  "GT_ch12" )),   
                 ((htm5, "GT_ch11"  ), (htm5,  "GT_ch11" )), 
                 ((htm5, "GT_ch10"  ), (htm5,  "GT_ch10" )), 
                 ((htm5, "GT_ch9"   ), (htm5,  "GT_ch9"  )), 
                 ((htm5, "GT_ch8"   ), (htm5,  "GT_ch8"  )), 
                 ((htm5, "GT_ch7"   ), (htm5,  "GT_ch7"  )), 
                 ((htm5, "GT_ch6"   ), (htm5,  "GT_ch6"  )), 
                 ((htm5, "GT_ch5"   ), (htm5,  "GT_ch5"  )), 
                 ((htm5, "GT_ch4"   ), (htm5,  "GT_ch4"  )), 
                 ((htm5, "GT_ch3"   ), (htm5,  "GT_ch3"  )), 
                 ((htm5, "GT_ch2"   ), (htm5,  "GT_ch2"  )), 
                 ((htm5, "GT_ch1"   ), (htm5,  "GT_ch1"  )), 
                 ((htm5, "GT_ch0"   ), (htm5,  "GT_ch0"  )), 

                 ((htm6, "GT_ch15"  ), (htm6,  "GT_ch15" )),  
                 ((htm6, "GT_ch14"  ), (htm6,  "GT_ch14" )),  
                 ((htm6, "GT_ch13"  ), (htm6,  "GT_ch13" )),  
                 ((htm6, "GT_ch12"  ), (htm6,  "GT_ch12" )),   
                 ((htm6, "GT_ch11"  ), (htm6,  "GT_ch11" )), 
                 ((htm6, "GT_ch10"  ), (htm6,  "GT_ch10" )), 
                 ((htm6, "GT_ch9"   ), (htm6,  "GT_ch9"  )), 
                 ((htm6, "GT_ch8"   ), (htm6,  "GT_ch8"  )), 
                 ((htm6, "GT_ch7"   ), (htm6,  "GT_ch7"  )), 
                 ((htm6, "GT_ch6"   ), (htm6,  "GT_ch6"  )), 
                 ((htm6, "GT_ch5"   ), (htm6,  "GT_ch5"  )), 
                 ((htm6, "GT_ch4"   ), (htm6,  "GT_ch4"  )), 
                 ((htm6, "GT_ch3"   ), (htm6,  "GT_ch3"  )), 
                 ((htm6, "GT_ch2"   ), (htm6,  "GT_ch2"  )), 
                 ((htm6, "GT_ch1"   ), (htm6,  "GT_ch1"  )), 
                 ((htm6, "GT_ch0"   ), (htm6,  "GT_ch0"  )), 

                 ((htm7, "GT_ch15"  ), (htm7,  "GT_ch15" )),  
                 ((htm7, "GT_ch14"  ), (htm7,  "GT_ch14" )),  
                 ((htm7, "GT_ch13"  ), (htm7,  "GT_ch13" )),  
                 ((htm7, "GT_ch12"  ), (htm7,  "GT_ch12" )),   
                 ((htm7, "GT_ch11"  ), (htm7,  "GT_ch11" )), 
                 ((htm7, "GT_ch10"  ), (htm7,  "GT_ch10" )), 
                 ((htm7, "GT_ch9"   ), (htm7,  "GT_ch9"  )), 
                 ((htm7, "GT_ch8"   ), (htm7,  "GT_ch8"  )), 
                 ((htm7, "GT_ch7"   ), (htm7,  "GT_ch7"  )), 
                 ((htm7, "GT_ch6"   ), (htm7,  "GT_ch6"  )), 
                 ((htm7, "GT_ch5"   ), (htm7,  "GT_ch5"  )), 
                 ((htm7, "GT_ch4"   ), (htm7,  "GT_ch4"  )), 
                 ((htm7, "GT_ch3"   ), (htm7,  "GT_ch3"  )), 
                 ((htm7, "GT_ch2"   ), (htm7,  "GT_ch2"  )), 
                 ((htm7, "GT_ch1"   ), (htm7,  "GT_ch1"  )), 
                 ((htm7, "GT_ch0"   ), (htm7,  "GT_ch0"  )), 

                 ((htm8, "GT_ch15"  ), (htm8,  "GT_ch15" )),  
                 ((htm8, "GT_ch14"  ), (htm8,  "GT_ch14" )),  
                 ((htm8, "GT_ch13"  ), (htm8,  "GT_ch13" )),  
                 ((htm8, "GT_ch12"  ), (htm8,  "GT_ch12" )),   
                 ((htm8, "GT_ch11"  ), (htm8,  "GT_ch11" )), 
                 ((htm8, "GT_ch10"  ), (htm8,  "GT_ch10" )), 
                 ((htm8, "GT_ch9"   ), (htm8,  "GT_ch9"  )), 
                 ((htm8, "GT_ch8"   ), (htm8,  "GT_ch8"  )), 
                 ((htm8, "GT_ch7"   ), (htm8,  "GT_ch7"  )), 
                 ((htm8, "GT_ch6"   ), (htm8,  "GT_ch6"  )), 
                 ((htm8, "GT_ch5"   ), (htm8,  "GT_ch5"  )), 
                 ((htm8, "GT_ch4"   ), (htm8,  "GT_ch4"  )), 
                 ((htm8, "GT_ch3"   ), (htm8,  "GT_ch3"  )), 
                 ((htm8, "GT_ch2"   ), (htm8,  "GT_ch2"  )), 
                 ((htm8, "GT_ch1"   ), (htm8,  "GT_ch1"  )), 
                 ((htm8, "GT_ch0"   ), (htm8,  "GT_ch0"  )), 

                 ((htm9, "GT_ch15"  ), (htm9,  "GT_ch15" )),  
                 ((htm9, "GT_ch14"  ), (htm9,  "GT_ch14" )),  
                 ((htm9, "GT_ch13"  ), (htm9,  "GT_ch13" )),  
                 ((htm9, "GT_ch12"  ), (htm9,  "GT_ch12" )),   
                 ((htm9, "GT_ch11"  ), (htm9,  "GT_ch11" )), 
                 ((htm9, "GT_ch10"  ), (htm9,  "GT_ch10" )), 
                 ((htm9, "GT_ch9"   ), (htm9,  "GT_ch9"  )), 
                 ((htm9, "GT_ch8"   ), (htm9,  "GT_ch8"  )), 
                 ((htm9, "GT_ch7"   ), (htm9,  "GT_ch7"  )), 
                 ((htm9, "GT_ch6"   ), (htm9,  "GT_ch6"  )), 
                 ((htm9, "GT_ch5"   ), (htm9,  "GT_ch5"  )), 
                 ((htm9, "GT_ch4"   ), (htm9,  "GT_ch4"  )), 
                 ((htm9, "GT_ch3"   ), (htm9,  "GT_ch3"  )), 
                 ((htm9, "GT_ch2"   ), (htm9,  "GT_ch2"  )), 
                 ((htm9, "GT_ch1"   ), (htm9,  "GT_ch1"  )), 
                 ((htm9, "GT_ch0"   ), (htm9,  "GT_ch0"  )), 

                 ((htm10, "GT_ch15" ), (htm10, "GT_ch15" )),  
                 ((htm10, "GT_ch14" ), (htm10, "GT_ch14" )),  
                 ((htm10, "GT_ch13" ), (htm10, "GT_ch13" )),  
                 ((htm10, "GT_ch12" ), (htm10, "GT_ch12" )),   
                 ((htm10, "GT_ch11" ), (htm10, "GT_ch11" )), 
                 ((htm10, "GT_ch10" ), (htm10, "GT_ch10" )), 
                 ((htm10, "GT_ch9"  ), (htm10, "GT_ch9"  )), 
                 ((htm10, "GT_ch8"  ), (htm10, "GT_ch8"  )), 
                 ((htm10, "GT_ch7"  ), (htm10, "GT_ch7"  )), 
                 ((htm10, "GT_ch6"  ), (htm10, "GT_ch6"  )), 
                 ((htm10, "GT_ch5"  ), (htm10, "GT_ch5"  )), 
                 ((htm10, "GT_ch4"  ), (htm10, "GT_ch4"  )), 
                 ((htm10, "GT_ch3"  ), (htm10, "GT_ch3"  )), 
                 ((htm10, "GT_ch2"  ), (htm10, "GT_ch2"  )), 
                 ((htm10, "GT_ch1"  ), (htm10, "GT_ch1"  )), 
                 ((htm10, "GT_ch0"  ), (htm10, "GT_ch0"  )), 

                 ((htm11, "GT_ch15" ), (htm11, "GT_ch15" )),  
                 ((htm11, "GT_ch14" ), (htm11, "GT_ch14" )),  
                 ((htm11, "GT_ch13" ), (htm11, "GT_ch13" )),  
                 ((htm11, "GT_ch12" ), (htm11, "GT_ch12" )),   
                 ((htm11, "GT_ch11" ), (htm11, "GT_ch11" )), 
                 ((htm11, "GT_ch10" ), (htm11, "GT_ch10" )), 
                 ((htm11, "GT_ch9"  ), (htm11, "GT_ch9"  )), 
                 ((htm11, "GT_ch8"  ), (htm11, "GT_ch8"  )), 
                 ((htm11, "GT_ch7"  ), (htm11, "GT_ch7"  )), 
                 ((htm11, "GT_ch6"  ), (htm11, "GT_ch6"  )), 
                 ((htm11, "GT_ch5"  ), (htm11, "GT_ch5"  )), 
                 ((htm11, "GT_ch4"  ), (htm11, "GT_ch4"  )), 
                 ((htm11, "GT_ch3"  ), (htm11, "GT_ch3"  )), 
                 ((htm11, "GT_ch2"  ), (htm11, "GT_ch2"  )), 
                 ((htm11, "GT_ch1"  ), (htm11, "GT_ch1"  )), 
                 ((htm11, "GT_ch0"  ), (htm11, "GT_ch0"  )), 

                 ((htm12, "GT_ch15" ), (htm12, "GT_ch15" )),  
                 ((htm12, "GT_ch14" ), (htm12, "GT_ch14" )),  
                 ((htm12, "GT_ch13" ), (htm12, "GT_ch13" )),  
                 ((htm12, "GT_ch12" ), (htm12, "GT_ch12" )),   
                 ((htm12, "GT_ch11" ), (htm12, "GT_ch11" )), 
                 ((htm12, "GT_ch10" ), (htm12, "GT_ch10" )), 
                 ((htm12, "GT_ch9"  ), (htm12, "GT_ch9"  )), 
                 ((htm12, "GT_ch8"  ), (htm12, "GT_ch8"  )), 
                 ((htm12, "GT_ch7"  ), (htm12, "GT_ch7"  )), 
                 ((htm12, "GT_ch6"  ), (htm12, "GT_ch6"  )), 
                 ((htm12, "GT_ch5"  ), (htm12, "GT_ch5"  )), 
                 ((htm12, "GT_ch4"  ), (htm12, "GT_ch4"  )), 
                 ((htm12, "GT_ch3"  ), (htm12, "GT_ch3"  )), 
                 ((htm12, "GT_ch2"  ), (htm12, "GT_ch2"  )), 
                 ((htm12, "GT_ch1"  ), (htm12, "GT_ch1"  )), 
                 ((htm12, "GT_ch0"  ), (htm12, "GT_ch0"  )), 

                 ((htm13, "GT_ch15" ), (htm13, "GT_ch15" )),  
                 ((htm13, "GT_ch14" ), (htm13, "GT_ch14" )),  
                 ((htm13, "GT_ch13" ), (htm13, "GT_ch13" )),  
                 ((htm13, "GT_ch12" ), (htm13, "GT_ch12" )),   
                 ((htm13, "GT_ch11" ), (htm13, "GT_ch11" )), 
                 ((htm13, "GT_ch10" ), (htm13, "GT_ch10" )), 
                 ((htm13, "GT_ch9"  ), (htm13, "GT_ch9"  )), 
                 ((htm13, "GT_ch8"  ), (htm13, "GT_ch8"  )), 
                 ((htm13, "GT_ch7"  ), (htm13, "GT_ch7"  )), 
                 ((htm13, "GT_ch6"  ), (htm13, "GT_ch6"  )), 
                 ((htm13, "GT_ch5"  ), (htm13, "GT_ch5"  )), 
                 ((htm13, "GT_ch4"  ), (htm13, "GT_ch4"  )), 
                 ((htm13, "GT_ch3"  ), (htm13, "GT_ch3"  )), 
                 ((htm13, "GT_ch2"  ), (htm13, "GT_ch2"  )), 
                 ((htm13, "GT_ch1"  ), (htm13, "GT_ch1"  )), 
                 ((htm13, "GT_ch0"  ), (htm13, "GT_ch0"  )), 

                 ((htm14, "GT_ch15" ), (htm14, "GT_ch15" )),  
                 ((htm14, "GT_ch14" ), (htm14, "GT_ch14" )),  
                 ((htm14, "GT_ch13" ), (htm14, "GT_ch13" )),  
                 ((htm14, "GT_ch12" ), (htm14, "GT_ch12" )),   
                 ((htm14, "GT_ch11" ), (htm14, "GT_ch11" )), 
                 ((htm14, "GT_ch10" ), (htm14, "GT_ch10" )), 
                 ((htm14, "GT_ch9"  ), (htm14, "GT_ch9"  )), 
                 ((htm14, "GT_ch8"  ), (htm14, "GT_ch8"  )), 
                 ((htm14, "GT_ch7"  ), (htm14, "GT_ch7"  )), 
                 ((htm14, "GT_ch6"  ), (htm14, "GT_ch6"  )), 
                 ((htm14, "GT_ch5"  ), (htm14, "GT_ch5"  )), 
                 ((htm14, "GT_ch4"  ), (htm14, "GT_ch4"  )), 
                 ((htm14, "GT_ch3"  ), (htm14, "GT_ch3"  )), 
                  ((htm14, "GT_ch2"  ), (htm14, "GT_ch2"  )), 
                 ((htm14, "GT_ch1"  ), (htm14, "GT_ch1"  )), 
                  ((htm14, "GT_ch0"  ), (htm14, "GT_ch0"  )), 
                ]

    test_list_paired = [

                 ## HTM 3 Connections
                 ## HTM 3 - Minipod Connections
                 #((htm3, "GT_ch15" ), (hub1, "GT_ch52", )),  
                 #((htm3, "GT_ch14" ), (hub1, "GT_ch53", )),  
                 #((htm3, "GT_ch13" ), (hub1, "GT_ch64", )),  
                 #((htm3, "GT_ch12" ), (hub1, "GT_ch50", )),   

                 ## HTM 3 - Hub 1 Connections
                 #((htm3, "GT_ch11" ), (hub1, "GT_ch48", )),  # Lane 1
                 #((htm3, "GT_ch10" ), (hub1, "GT_ch49", )),  # Lane 0
                 #((htm3, "GT_ch9"  ), (hub1, "GT_ch44", )),  # Lane 5
                 #((htm3, "GT_ch8"  ), (hub1, "GT_ch46", )),  # Lane 3 --
                 #((htm3, "GT_ch7"  ), (hub1, "GT_ch45", )),  # Lane 4 --
                 #((htm3, "GT_ch6"  ), (hub1, "GT_ch47", )),  # Lane 2

                 ## HTM 3 - Hub 2 Connections
                 #((htm3, "GT_ch5"  ), (hub2, "GT_ch48", )), # Lane 1
                 #((htm3, "GT_ch4"  ), (hub2, "GT_ch49", )), # Lane 0 --
                 #((htm3, "GT_ch3"  ), (hub2, "GT_ch44", )), # Lane 5 --
                 #((htm3, "GT_ch2"  ), (hub2, "GT_ch46", )), # Lane 3 --
                 #((htm3, "GT_ch1"  ), (hub2, "GT_ch45", )), # Lane 4 --
                 #((htm3, "GT_ch0"  ), (hub2, "GT_ch47", )), # Lane 2 --

                 ## HTM 4 Connections
                 ## HTM 4 - Minipod Connections
                 #((htm4, "GT_ch15" ), (hub1, "GT_ch52", )),  
                 #((htm4, "GT_ch14" ), (hub1, "GT_ch53", )),  
                 #((htm4, "GT_ch13" ), (hub1, "GT_ch64", )),  
                 #((htm4, "GT_ch12" ), (hub1, "GT_ch50", )),   

                 ## HTM 4 - Hub 1 Connections
                 #((htm4, "GT_ch11" ), (hub1, "GT_ch42", )),  # Lane 1
                 #((htm4, "GT_ch10" ), (hub1, "GT_ch43", )),  # Lane 0
                 #((htm4, "GT_ch9"  ), (hub1, "GT_ch54", )),  # Lane 5   
                 #((htm4, "GT_ch8"  ), (hub1, "GT_ch56", )),  # Lane 3 --
                 #((htm4, "GT_ch7"  ), (hub1, "GT_ch55", )),  # Lane 4 --
                 #((htm4, "GT_ch6"  ), (hub1, "GT_ch57", )),  # Lane 2

                 ## HTM 4 - Hub 2 Connections
                 #((htm4, "GT_ch5"  ), (hub2, "GT_ch42", )), # Lane 1
                 #((htm4, "GT_ch4"  ), (hub2, "GT_ch43", )), # Lane 0 --
                 #((htm4, "GT_ch3"  ), (hub2, "GT_ch54", )), # Lane 5 --
                 #((htm4, "GT_ch2"  ), (hub2, "GT_ch56", )), # Lane 3 --
                 #((htm4, "GT_ch1"  ), (hub2, "GT_ch55", )), # Lane 4 --
                 #((htm4, "GT_ch0"  ), (hub2, "GT_ch57", )), # Lane 2 --

                 ## HTM 5 Connections
                 ## HTM 5 - Minipod Connections
                 #((htm5, "GT_ch15" ), (hub1, "GT_ch52", )),  
                 #((htm5, "GT_ch14" ), (hub1, "GT_ch53", )),  
                 #((htm5, "GT_ch13" ), (hub1, "GT_ch64", )),  
                 #((htm5, "GT_ch12" ), (hub1, "GT_ch50", )),   

                 ## HTM 5 - Hub 1 Connections
                 #((htm5, "GT_ch11" ), (hub1, "GT_ch52", )),  # Lane 1 -- --
                 #((htm5, "GT_ch10" ), (hub1, "GT_ch53", )),  # Lane 0 -- --
                 #((htm5, "GT_ch9"  ), (hub1, "GT_ch64", )),  # Lane 5    --
                 #((htm5, "GT_ch8"  ), (hub1, "GT_ch50", )),  # Lane 3 -- --
                 #((htm5, "GT_ch7"  ), (hub1, "GT_ch65", )),  # Lane 4 -- --
                 #((htm5, "GT_ch6"  ), (hub1, "GT_ch51", )),  # Lane 2    

                 ## HTM 5 - Hub 2 Connections
                 #((htm5, "GT_ch5"  ), (hub2, "GT_ch52", )), # Lane 1 -- --
                 #((htm5, "GT_ch4"  ), (hub2, "GT_ch53", )), # Lane 0 -- --
                 #((htm5, "GT_ch3"  ), (hub2, "GT_ch64", )), # Lane 5 -- --
                 #((htm5, "GT_ch2"  ), (hub2, "GT_ch50", )), # Lane 3 -- --
                 #((htm5, "GT_ch1"  ), (hub2, "GT_ch65", )), # Lane 4 -- --
                 #((htm5, "GT_ch0"  ), (hub2, "GT_ch51", )), # Lane 2 -- --

                 ## HTM 6 Connections
                 ## HTM 6 - Minipod Connections
                 #((htm6, "GT_ch15" ), (hub1, "GT_ch52", )),  
                 #((htm6, "GT_ch14" ), (hub1, "GT_ch53", )),  
                 #((htm6, "GT_ch13" ), (hub1, "GT_ch64", )),  
                 #((htm6, "GT_ch12" ), (hub1, "GT_ch50", )),   

                 ## HTM 6 - Hub 1 Connections
                 #((htm6, "GT_ch11" ), (hub1, "GT_ch62", )),  # Lane 1
                 #((htm6, "GT_ch10" ), (hub1, "GT_ch63", )),  # Lane 0
                 #((htm6, "GT_ch9"  ), (hub1, "GT_ch58", )),  # Lane 5 --
                 #((htm6, "GT_ch8"  ), (hub1, "GT_ch60", )),  # Lane 3
                 #((htm6, "GT_ch7"  ), (hub1, "GT_ch59", )),  # Lane 4
                 #((htm6, "GT_ch6"  ), (hub1, "GT_ch61", )),  # Lane 2

                 ## HTM 6 - Hub 2 Connections
                 #((htm6, "GT_ch5"  ), (hub2, "GT_ch62", )), # Lane 1
                 #((htm6, "GT_ch4"  ), (hub2, "GT_ch63", )), # Lane 0
                 #((htm6, "GT_ch3"  ), (hub2, "GT_ch58", )), # Lane 5
                 #((htm6, "GT_ch2"  ), (hub2, "GT_ch60", )), # Lane 3
                 #((htm6, "GT_ch1"  ), (hub2, "GT_ch59", )), # Lane 4
                 #((htm6, "GT_ch0"  ), (hub2, "GT_ch61", )), # Lane 2

                 ## HTM 7 Connections
                 ## HTM 7 - Minipod Connections
                 #((htm7, "GT_ch15" ), (hub1, "GT_ch52", )),  
                 #((htm7, "GT_ch14" ), (hub1, "GT_ch53", )),  
                 #((htm7, "GT_ch13" ), (hub1, "GT_ch64", )),  
                 #((htm7, "GT_ch12" ), (hub1, "GT_ch50", )),   

                 ## HTM 7 - Hub 1 Connections
                 #((htm7, "GT_ch11" ), (hub1, "GT_ch74", )),  # Lane 1
                 #((htm7, "GT_ch10" ), (hub1, "GT_ch75", )),  # Lane 0
                 #((htm7, "GT_ch9"  ), (hub1, "GT_ch68", )),  # Lane 5 --
                 #((htm7, "GT_ch8"  ), (hub1, "GT_ch72", )),  # Lane 3
                 #((htm7, "GT_ch7"  ), (hub1, "GT_ch69", )),  # Lane 4
                 #((htm7, "GT_ch6"  ), (hub1, "GT_ch73", )),  # Lane 2

                 ## HTM 7 - Hub 2 Connections
                 #((htm7, "GT_ch5"  ), (hub2, "GT_ch74", )), # Lane 1
                 #((htm7, "GT_ch4"  ), (hub2, "GT_ch75", )), # Lane 0
                 #((htm7, "GT_ch3"  ), (hub2, "GT_ch68", )), # Lane 5
                 #((htm7, "GT_ch2"  ), (hub2, "GT_ch72", )), # Lane 3
                 #((htm7, "GT_ch1"  ), (hub2, "GT_ch69", )), # Lane 4
                 #((htm7, "GT_ch0"  ), (hub2, "GT_ch73", )), # Lane 2

                 ## HTM 8 Connections
                 ## HTM 8 - Minipod Connections
                 #((htm8, "GT_ch15" ), (hub1, "GT_ch52", )),  
                 #((htm8, "GT_ch14" ), (hub1, "GT_ch53", )),  
                 #((htm8, "GT_ch13" ), (hub1, "GT_ch64", )),  
                 #((htm8, "GT_ch12" ), (hub1, "GT_ch50", )),   

                 ## HTM 8 - Hub 1 Connections
                 #((htm8, "GT_ch11" ), (hub1, "GT_ch66", )),  # Lane 1 
                 #((htm8, "GT_ch10" ), (hub1, "GT_ch67", )),  # Lane 0
                 #((htm8, "GT_ch9"  ), (hub1, "GT_ch70", )),  # Lane 5
                 #((htm8, "GT_ch8"  ), (hub1, "GT_ch31", )),  # Lane 3
                 #((htm8, "GT_ch7"  ), (hub1, "GT_ch71", )),  # Lane 4
                 #((htm8, "GT_ch6"  ), (hub1, "GT_ch30", )),  # Lane 2

                 ## HTM 8 - Hub 2 Connections
                 #((htm8, "GT_ch5"  ), (hub2, "GT_ch66", )), # Lane 1
                 #((htm8, "GT_ch4"  ), (hub2, "GT_ch67", )), # Lane 0
                 #((htm8, "GT_ch3"  ), (hub2, "GT_ch70", )), # Lane 5
                 #((htm8, "GT_ch2"  ), (hub2, "GT_ch31", )), # Lane 3
                 #((htm8, "GT_ch1"  ), (hub2, "GT_ch71", )), # Lane 4
                 #((htm8, "GT_ch0"  ), (hub2, "GT_ch30", )), # Lane 2

                 ## HTM 9 Connections
                 ## HTM 9 - Minipod Connections
                 #((htm9, "GT_ch15" ), (hub1, "GT_ch52", )),  
                 #((htm9, "GT_ch14" ), (hub1, "GT_ch53", )),  
                 #((htm9, "GT_ch13" ), (hub1, "GT_ch64", )),  
                 #((htm9, "GT_ch12" ), (hub1, "GT_ch50", )),   

                 ## HTM 9 - Hub 1 Connections
                 #((htm9, "GT_ch11" ), (hub1, "GT_ch76", )),  # Lane 1
                 #((htm9, "GT_ch10" ), (hub1, "GT_ch77", )),  # Lane 0
                 #((htm9, "GT_ch9"  ), (hub1, "GT_ch35", )),  # Lane 5 --
                 #((htm9, "GT_ch8"  ), (hub1, "GT_ch33", )),  # Lane 3
                 #((htm9, "GT_ch7"  ), (hub1, "GT_ch34", )),  # Lane 4
                 #((htm9, "GT_ch6"  ), (hub1, "GT_ch32", )),  # Lane 2

                 ## HTM 9 - Hub 2 Connections
                 #((htm9, "GT_ch5"  ), (hub2, "GT_ch76", )), # Lane 1
                 #((htm9, "GT_ch4"  ), (hub2, "GT_ch77", )), # Lane 0
                 #((htm9, "GT_ch3"  ), (hub2, "GT_ch35", )), # Lane 5
                 #((htm9, "GT_ch2"  ), (hub2, "GT_ch33", )), # Lane 3
                 #((htm9, "GT_ch1"  ), (hub2, "GT_ch34", )), # Lane 4
                 #((htm9, "GT_ch0"  ), (hub2, "GT_ch32", )), # Lane 2

                 ## HTM 10 Connections
                 ## HTM 10 - Minipod Connections
                 #((htm10, "GT_ch15" ), (hub1, "GT_ch52", )),  
                 #((htm10, "GT_ch14" ), (hub1, "GT_ch53", )),  
                 #((htm10, "GT_ch13" ), (hub1, "GT_ch64", )),  
                 #((htm10, "GT_ch12" ), (hub1, "GT_ch50", )),   

                 ## HTM 10 - Hub 1 Connections
                 #((htm10, "GT_ch11" ), (hub1, "GT_ch37", )),  # Lane 1 
                 #((htm10, "GT_ch10" ), (hub1, "GT_ch36", )),  # Lane 0
                 #((htm10, "GT_ch9"  ), (hub1, "GT_ch23", )),  # Lane 5
                 #((htm10, "GT_ch8"  ), (hub1, "GT_ch39", )),  # Lane 3
                 #((htm10, "GT_ch7"  ), (hub1, "GT_ch22", )),  # Lane 4
                 #((htm10, "GT_ch6"  ), (hub1, "GT_ch38", )),  # Lane 2 --

                 ## HTM 10 - Hub 2 Connections
                 #((htm10, "GT_ch5"  ), (hub2, "GT_ch37", )), # Lane 1
                 #((htm10, "GT_ch4"  ), (hub2, "GT_ch36", )), # Lane 0
                 #((htm10, "GT_ch3"  ), (hub2, "GT_ch23", )), # Lane 5
                 #((htm10, "GT_ch2"  ), (hub2, "GT_ch39", )), # Lane 3
                 #((htm10, "GT_ch1"  ), (hub2, "GT_ch22", )), # Lane 4
                 #((htm10, "GT_ch0"  ), (hub2, "GT_ch38", )), # Lane 2 --

                 ## HTM 11 Connections
                 ## HTM 11 - Minipod Connections
                 #((htm11, "GT_ch15" ), (hub1, "GT_ch52", )),  
                 #((htm11, "GT_ch14" ), (hub1, "GT_ch53", )),  
                 #((htm11, "GT_ch13" ), (hub1, "GT_ch64", )),  
                 #((htm11, "GT_ch12" ), (hub1, "GT_ch50", )),   

                 ## HTM 11 - Hub 1 Connections
                 #((htm11, "GT_ch11" ), (hub1, "GT_ch25", )),  # Lane 1 
                 #((htm11, "GT_ch10" ), (hub1, "GT_ch24", )),  # Lane 0
                 #((htm11, "GT_ch9"  ), (hub1, "GT_ch29", )),  # Lane 5 --
                 #((htm11, "GT_ch8"  ), (hub1, "GT_ch27", )),  # Lane 3
                 #((htm11, "GT_ch7"  ), (hub1, "GT_ch28", )),  # Lane 4
                 #((htm11, "GT_ch6"  ), (hub1, "GT_ch26", )),  # Lane 2
                 #
                 ## HTM 11 - Hub 2 Connections
                 #((htm11, "GT_ch5"  ), (hub2, "GT_ch25", )), # Lane 1
                 #((htm11, "GT_ch4"  ), (hub2, "GT_ch24", )), # Lane 0
                 #((htm11, "GT_ch3"  ), (hub2, "GT_ch29", )), # Lane 5
                 #((htm11, "GT_ch2"  ), (hub2, "GT_ch27", )), # Lane 3
                 #((htm11, "GT_ch1"  ), (hub2, "GT_ch28", )), # Lane 4
                 #((htm11, "GT_ch0"  ), (hub2, "GT_ch26", )), # Lane 2

                 ## HTM 12 Connections
                 ## HTM 12 - Minipod Connections
                 #((htm12, "GT_ch15" ), (hub1, "GT_ch52", )),  
                 #((htm12, "GT_ch14" ), (hub1, "GT_ch53", )),  
                 #((htm12, "GT_ch13" ), (hub1, "GT_ch64", )),  
                 #((htm12, "GT_ch12" ), (hub1, "GT_ch50", )),   

                 ## HTM 12 - Hub 1 Connections
                 #((htm12, "GT_ch11" ), (hub1, "GT_ch15", )),  # Lane 1 
                 #((htm12, "GT_ch10" ), (hub1, "GT_ch14", )),  # Lane 0
                 #((htm12, "GT_ch9"  ), (hub1, "GT_ch19", )),  # Lane 5 --
                 #((htm12, "GT_ch8"  ), (hub1, "GT_ch17", )),  # Lane 3
                 #((htm12, "GT_ch7"  ), (hub1, "GT_ch18", )),  # Lane 4
                 #((htm12, "GT_ch6"  ), (hub1, "GT_ch16", )),  # Lane 2

                 ## HTM 12 - Hub 2 Connections
                 #((htm12, "GT_ch5"  ), (hub2, "GT_ch15", )), # Lane 1
                 #((htm12, "GT_ch4"  ), (hub2, "GT_ch14", )), # Lane 0
                 #((htm12, "GT_ch3"  ), (hub2, "GT_ch19", )), # Lane 5
                 #((htm12, "GT_ch2"  ), (hub2, "GT_ch17", )), # Lane 3
                 #((htm12, "GT_ch1"  ), (hub2, "GT_ch18", )), # Lane 4
                 #((htm12, "GT_ch0"  ), (hub2, "GT_ch16", )), # Lane 2

                 ## HTM 13 Connections
                 ## HTM 13 - Minipod Connections
                 #((htm13, "GT_ch15" ), (hub1, "GT_ch52", )),  
                 #((htm13, "GT_ch14" ), (hub1, "GT_ch53", )),  
                 #((htm13, "GT_ch13" ), (hub1, "GT_ch64", )),  
                 #((htm13, "GT_ch12" ), (hub1, "GT_ch50", )),   

                 ## HTM 13 - Hub 1 Connections
                 #((htm13, "GT_ch11" ), (hub1, "GT_ch21", )),  # Lane 1 
                 #((htm13, "GT_ch10" ), (hub1, "GT_ch20", )),  # Lane 0
                 #((htm13, "GT_ch9"  ), (hub1, "GT_ch7", )),  # Lane 5 --
                 #((htm13, "GT_ch8"  ), (hub1, "GT_ch5", )),  # Lane 3
                 #((htm13, "GT_ch7"  ), (hub1, "GT_ch6", )),  # Lane 4
                 #((htm13, "GT_ch6"  ), (hub1, "GT_ch4", )),  # Lane 2

                 ## HTM 13 - Hub 2 Connections
                 #((htm13, "GT_ch5"  ), (hub2, "GT_ch21", )), # Lane 1
                 #((htm13, "GT_ch4"  ), (hub2, "GT_ch20", )), # Lane 0
                 #((htm13, "GT_ch3"  ), (hub2, "GT_ch7", )), # Lane 5
                 #((htm13, "GT_ch2"  ), (hub2, "GT_ch5", )), # Lane 3
                 #((htm13, "GT_ch1"  ), (hub2, "GT_ch6", )), # Lane 4
                 #((htm13, "GT_ch0"  ), (hub2, "GT_ch4", )), # Lane 2

                 ## HTM 14 Connections
                 ## HTM 14 - Minipod Connections
                 #((htm14, "GT_ch15" ), (hub1, "GT_ch52", )),  
                 #((htm14, "GT_ch14" ), (hub1, "GT_ch53", )),  
                 #((htm14, "GT_ch13" ), (hub1, "GT_ch64", )),  
                 #((htm14, "GT_ch12" ), (hub1, "GT_ch50", )),   

                 ## HTM 14 - Hub 1 Connections
                 #((htm14, "GT_ch11" ), (hub1, "GT_ch9", )),  # Lane 1 
                 #((htm14, "GT_ch10" ), (hub1, "GT_ch8", )),  # Lane 0
                 #((htm14, "GT_ch9"  ), (hub1, "GT_ch13", )),  # Lane 5 --
                 #((htm14, "GT_ch8"  ), (hub1, "GT_ch11", )),  # Lane 3
                 #((htm14, "GT_ch7"  ), (hub1, "GT_ch12", )),  # Lane 4
                 #((htm14, "GT_ch6"  ), (hub1, "GT_ch10", )),  # Lane 2

                 ## HTM 14 - Hub 2 Connections
                 #((htm14, "GT_ch5"  ), (hub2, "GT_ch9", )), # Lane 1
                 #((htm14, "GT_ch4"  ), (hub2, "GT_ch8", )), # Lane 0
                 #((htm14, "GT_ch3"  ), (hub2, "GT_ch13", )), # Lane 5
                 #((htm14, "GT_ch2"  ), (hub2, "GT_ch11", )), # Lane 3
                 #((htm14, "GT_ch1"  ), (hub2, "GT_ch12", )), # Lane 4
                 #((htm14, "GT_ch0"  ), (hub2, "GT_ch10", )), # Lane 2

                 ## Connections between two hubs
                 #((hub1, "GT_ch52", ), (hub2, "GT_ch79", )),
                 #((hub1, "GT_ch54", ), (hub2, "GT_ch78", )),
                 #((hub1, "GT_ch60", ), (hub2, "GT_ch41", )),

                 #((hub2, "GT_ch52", ), (hub1, "GT_ch79", )),
                 #((hub2, "GT_ch54", ), (hub1, "GT_ch78", )),
                 #((hub2, "GT_ch60", ), (hub1, "GT_ch41", )),


                 # Combined Data to HTMs
                 
                 ## HTM 3
                 #((hub1, "GT_ch62"  ), (htm3, "GT_ch9", )),
                 #((hub2, "GT_ch62"  ), (htm3, "GT_ch6", )),

                 ## HTM 4
                 #((hub1, "GT_ch64"  ), (htm4, "GT_ch9", )),
                 #((hub2, "GT_ch64"  ), (htm4, "GT_ch6", )),

                 ## HTM 5
                 #((hub1, "GT_ch66"  ), (htm5, "GT_ch9", )),
                 #((hub2, "GT_ch66"  ), (htm5, "GT_ch6", )),

                 ## HTM 6
                 #((hub1, "GT_ch75"  ), (htm6, "GT_ch9", )),
                 #((hub2, "GT_ch75"  ), (htm6, "GT_ch6", )),

                 ## HTM 7
                 #((hub1, "GT_ch78"  ), (htm7, "GT_ch9", )),
                 #((hub2, "GT_ch78"  ), (htm7, "GT_ch6", )),

                 ## HTM 8
                 #((hub1, "GT_ch79"  ), (htm8, "GT_ch9", )),
                 #((hub2, "GT_ch79"  ), (htm8, "GT_ch6", )),

                 ## HTM 9
                 #((hub1, "GT_ch39"  ), (htm9, "GT_ch9", )),
                 #((hub2, "GT_ch39"  ), (htm9, "GT_ch6", )),

                 ## HTM 10
                 #((hub1, "GT_ch38"  ), (htm10, "GT_ch9", )),
                 #((hub2, "GT_ch38"  ), (htm10, "GT_ch6", )),

                 ## HTM 11
                 #((hub1, "GT_ch35"  ), (htm11, "GT_ch9", )),
                 #((hub2, "GT_ch35"  ), (htm11, "GT_ch6", )),

                 ## HTM 12
                 #((hub1, "GT_ch26"  ), (htm12, "GT_ch9", )),
                 #((hub2, "GT_ch26"  ), (htm12, "GT_ch6", )),

                 ## HTM 13
                 #((hub1, "GT_ch24"  ), (htm13, "GT_ch9", )),
                 #((hub2, "GT_ch24"  ), (htm13, "GT_ch6", )),

                 ## HTM 14
                 #((hub1, "GT_ch22"  ), (htm14, "GT_ch9", )),
                 #((hub2, "GT_ch22"  ), (htm14, "GT_ch6", )),

                 ## Connections between two hubs
                 #((hub1, "GT_ch52", ), (hub2, "GT_ch79", )),
                 #((hub1, "GT_ch54", ), (hub2, "GT_ch78", )),
                 #((hub1, "GT_ch60", ), (hub2, "GT_ch41", )),

                 #((hub2, "GT_ch52", ), (hub1, "GT_ch79", )),
                 #((hub2, "GT_ch54", ), (hub1, "GT_ch78", )),
                 #((hub2, "GT_ch60", ), (hub1, "GT_ch41", )),

                 ## ROD
                 #((hub1, "GT_ch16"  ), (htm3, "DUMMY", )), # Combined Data
                 #((hub1, "GT_ch18"  ), (htm3, "DUMMY", )), # Readout AL_1
                 #((hub1, "GT_ch20"  ), (htm3, "DUMMY", )), # Readout AL_0

                 #((hub2, "GT_ch16"  ), (htm3, "DUMMY", )), # Combined Data
                 #((hub2, "GT_ch18"  ), (htm3, "DUMMY", )), # Readout AL_1
                 #((hub2, "GT_ch20"  ), (htm3, "DUMMY", )), # Readout AL_0

                 #((hub2, "GT_ch20"  ), (hub1, "GT_ch3", )), # Readout Control from ROD

                 ## MINIPOD
                 #((hub1, "GT_ch14"  ), (hub1, "GT_ch1", )),
                 #((hub1, "GT_ch12"  ), (hub1, "GT_ch2", )),
                 #((hub1, "GT_ch10"  ), (hub1, "GT_ch40", )),
                 #((hub1, "GT_ch8"  ), (hub1, "GT_ch1", )),
                 #((hub1, "GT_ch6"  ), (hub1, "GT_ch1", )),
                 #((hub1, "GT_ch4"  ), (hub1, "GT_ch1", )),
                 #((hub1, "GT_ch2"  ), (hub1, "GT_ch1", )),
                 #((hub1, "GT_ch0"  ), (hub1, "GT_ch1", )),

                 #((hub2, "GT_ch14" ), (hub2, "GT_ch1", )),
                 #((hub2, "GT_ch12" ), (hub2, "GT_ch2", )),
                 #((hub2, "GT_ch10" ), (hub2, "GT_ch40", )),
                 #((hub2, "GT_ch8"  ), (hub2, "GT_ch1", )),
                 #((hub2, "GT_ch6"  ), (hub2, "GT_ch1", )),
                 #((hub2, "GT_ch4"  ), (hub2, "GT_ch1", )),
                 #((hub2, "GT_ch2"  ), (hub2, "GT_ch2", )),
                 #((hub2, "GT_ch0"  ), (hub2, "GT_ch1", )),
                ]

    ## PHASE 1
    #log.write((70*"=") + "\n\nPHASE 1\n\n" + (70*"=") + '\n')

    #for tup in htm_test_list:

    #    tx_dev = tup[0][0]
    #    tx_ch = tup[0][1]
    #    tx = tx_dev[tx_ch] 

    #    rx_dev = tup[1][0]
    #    rx_ch = tup[1][1]
    #    rx = rx_dev[rx_ch]

    #    log.write((70*"-") + "\nDevice: %s\nChannel: %s\n"%(tx_dev,tx_ch))

    #    pre_emphasis = tx["tx_setup"]["pre-emphasis"].write("0x0")
    #    log.write("Value of pre-emphasis after writing 0x0: %s\n"%pre_emphasis)
    #    post_emphasis =  tx["tx_setup"]["post-emphasis"].write("0x0")
    #    log.write("Value of post-emphasis after writing 0x0: %s\n"%post_emphasis)

    #    swing = tx["tx_setup"]["swing"].write("0xC")
    #    log.write("Value of swing after writing 0xC: %s\n"%swing)

    #    txprbs_val = "0x4"
    #    log.write("Value of txprbssel after writing value %s: %s\n"%(
    #               txprbs_val, tx.set_txprbssel(txprbs_val)))

    #    DFE_on_off = rx["rx_setup"]["DFE on/off"].write("0b1")
    #    log.write("Value of DFE on/off after writing 0b1: %s\n"%DFE_on_off)

    #    rxprbs_val = "0x4"
    #    log.write("Value of rxprbssel after writing value %s: %s\n"%(
    #               rxprbs_val, rx.set_rxprbssel(rxprbs_val)))

    #    log.write("Value of reset_master throughout reset: %s\n"%(rx.reset(wait_time = 0.5),))

    #    log.flush()

    #for tup in hub_test_list:

    #    tx_dev = tup[0][0]
    #    tx_ch = tup[0][1]
    #    tx = tx_dev[tx_ch] 

    #    rx_dev = tup[1][0]
    #    rx_ch = tup[1][1]
    #    rx = rx_dev[rx_ch]

    #    log.write((70*"-") + "\nDevice: %s\nChannel: %s\n"%(tx_dev,tx_ch))

    #    pre_emphasis = tx["tx_setup"]["pre-emphasis"].write("0x0")
    #    log.write("Value of pre-emphasis after writing 0x0: %s\n"%pre_emphasis)
    #    post_emphasis =  tx["tx_setup"]["post-emphasis"].write("0x0")
    #    log.write("Value of post-emphasis after writing 0x0: %s\n"%post_emphasis)

    #    if int(tx_ch[5:]) < 40:
    #        swing = tx["tx_setup"]["swing"].write("0xC")
    #        log.write("Value of swing after writing 0xC: %s\n"%swing)
    #    else:
    #        swing = tx["tx_setup"]["swing"].write("0b11000")
    #        log.write("Value of swing after writing 0b11000: %s\n"%swing)

    #    txprbs_val = "0x5"
    #    log.write("Value of txprbssel after writing value %s: %s\n"%(
    #               txprbs_val, tx.set_txprbssel(txprbs_val)))

    #    DFE_on_off = rx["rx_setup"]["DFE on/off"].write("0b1")
    #    log.write("Value of DFE on/off after writing 0b1: %s\n"%DFE_on_off)

    #    #if rx_ch[5:] == "3":
    #    #    rxprbs_val = "0x1"
    #    #else:
    #    #    rxprbs_val = "0x5"
    #    rxprbs_val = "0x5"
    #    log.write("Value of rxprbssel after writing value %s: %s\n"%(
    #               rxprbs_val, rx.set_rxprbssel(rxprbs_val)))

    #    log.write("Value of reset_master throughout reset: %s\n"%(rx.reset(wait_time = 0.5),))

    #    log.flush()

    ## PHASE 2
    #log.write((70*"=") + "\n\nPHASE 2\n\n" + (70*"=") + '\n')
    #for tup in htm_test_list:

    #    tx_dev = tup[0][0]
    #    tx_ch = tup[0][1]
    #    tx = tx_dev[tx_ch] 

    #    rx_dev = tup[1][0]
    #    rx_ch = tup[1][1]
    #    rx = rx_dev[rx_ch]

    #    log.write((70*"-") + "\nDevice: %s\nChannel: %s\n"%(tx_dev,tx_ch))

    #    init_done = tx.get_init_done()
    #    if init_done != "0b1":
    #        log.write("WARNING - Value of init_done: %s\n"%init_done)
    #    else:
    #        log.write("Value of init_done: %s\n"%init_done)

    #    log.write("Value of rxprbscntreset throughout reset: %s\n"%(
    #              rx.reset_errors(wait_time = 0.5),))

    #    #log.write("Value of error count: %s\n"%rx.get_error_count())
    #    log.write("Value of error count: %s\n"%rx["rx_count_err"]["rxprbserr_cnt"].read())

    #    log.flush()

    #for tup in hub_test_list:

    #    tx_dev = tup[0][0]
    #    tx_ch = tup[0][1]
    #    tx = tx_dev[tx_ch] 

    #    rx_dev = tup[1][0]
    #    rx_ch = tup[1][1]
    #    rx = rx_dev[rx_ch]

    #    log.write((70*"-") + "\nDevice: %s\nChannel: %s\n"%(tx_dev,tx_ch))

    #    init_done = tx.get_init_done()
    #    if init_done != "0b1":
    #        log.write("WARNING - Value of init_done: %s\n"%init_done)
    #    else:
    #        log.write("Value of init_done: %s\n"%init_done)

    #    rxprbslocked = rx.get_rxprbslocked()
    #    if rxprbslocked != "0b1":
    #        log.write("WARNING - Value of rxprbslocked: %s\n"%rxprbslocked)
    #    else:
    #        log.write("Value of rxprbslocked: %s\n"%rxprbslocked)

    #    log.write("Value of rxprbscntreset throughout reset: %s\n"%(
    #              rx.reset_errors(wait_time = 0.5),))

    #    rxprbslocked = rx.get_rxprbslocked()
    #    if rxprbslocked != "0b1":
    #        log.write("WARNING - Value of rxprbslocked: %s\n"%rxprbslocked)
    #    else:
    #        log.write("Value of rxprbslocked: %s\n"%rxprbslocked)

    #    #log.write("Value of error count: %s\n"%rx.get_error_count())
    #    log.write("Value of error count: %s\n"%rx["rx_count_err"]["rxprbserr_cnt"].read())

    #    log.flush()

    # PHASE 3
    log.write((70*"=") + "\n\nPHASE 3\n\n" + (70*"=" + '\n'))
    for tup in htm_test_list:

        tx_dev = tup[0][0]
        tx_ch = tup[0][1]
        tx = tx_dev[tx_ch] 

        rx_dev = tup[1][0]
        rx_ch = tup[1][1]
        rx = rx_dev[rx_ch]

        log.write((70*"-") + "\nDevice: %s\nChannel: %s\n"%(tx_dev,tx_ch))

        #log.write("Value of error count: %s\n"%rx.get_error_count())
        log.write("Value of error count: %s\n"%rx["rx_count_err"]["rxprbserr_cnt"].read())

        log.flush()

    for tup in hub_test_list:

        tx_dev = tup[0][0]
        tx_ch = tup[0][1]
        tx = tx_dev[tx_ch] 

        rx_dev = tup[1][0]
        rx_ch = tup[1][1]
        rx = rx_dev[rx_ch]

        log.write((70*"-") + "\nDevice: %s\nChannel: %s\n"%(tx_dev,tx_ch))

        #log.write("Value of error count: %s\n"%rx.get_error_count())
        log.write("Value of error count: %s\n"%rx["rx_count_err"]["rxprbserr_cnt"].read())

        #CHECK_RX_PRBSERR_FLG
        log.write("Value of rxprbserr_flg: %s\n"%rx.get_error_flag())

        log.flush()

def old_save():

    for tup in test_list:

        tx_dev = tup[0][0]
        tx_ch = tup[0][1]
        tx = tx_dev[tx_ch] 

        rx_dev = tup[1][0]
        rx_ch = tup[1][1]
        rx = rx_dev[rx_ch]




        if False: #Turn TX part on/off
            log.write((70*"-") + "\nDevice: %s\nChannel: %s\n"%(tx_dev,tx_ch))

            ## PRE/POST EMPHASIS
            log.write("Value of pre-emphasis: %s\n"%tx["tx_setup"]["pre-emphasis"].write("0x0"))
            log.write("Value of post-emphasis: %s\n"%tx["tx_setup"]["post-emphasis"].write("0x0"))
            log.write("Vakye of swing: %s\n"%tx["tx_setup"]["swing"].write("0xC"))

            #PRBSSEL_VAL
            #log.write("Value of txprbssel: %s\n"%tx.get_txprbssel())

            #txprbs_val = "0x5"
            #log.write("Value of txprbssel after writing value %s: %s\n"%(
            #           txprbs_val, tx.set_txprbssel(txprbs_val)))

            #MGT_RESET
            log.write("Value of reset_master throughout reset: %s\n"%(tx.reset(wait_time = 2),))

            #INIT_DONE
            log.write("Value of init_done: %s\n"%tx.get_init_done())

            ##FORCE_ERROR
            #log.write("Value of txprbsforceerr throughout injection: %s\n"%(tx.inject_error(),))

        if True: #Turn RX part on/off
            log.write((70*"-") + "\nDevice: %s\nChannel: %s\n"%(rx_dev,rx_ch))

            log.write("Value of DFE on/off: %s\n"%rx["rx_setup"]["DFE on/off"].write("0b1"))
            #log.write(rx["common_loopback"]["loopback"].read())

            #PRBSSEL_VAL
            log.write("Value of rxprbssel: %s\n"%rx.get_rxprbssel())

            #rxprbs_val = "0x5"
            #log.write("Value of rxprbssel after writing value %s: %s\n"%(
            #           rxprbs_val, rx.set_rxprbssel(rxprbs_val)))

            ##MGT_RESET
            #log.write("Value of reset_master throughout reset: %s\n"%(rx.reset(wait_time = 2),))

            ##RESET_ERROR_COUNT
            #log.write("Value of rxprbscntreset throughout reset: %s\n"%(
            #          rx.reset_errors(wait_time = 2),))
            #log.write(rx["rx_reset"]["rxprbscntreset"].write("0b0") + "\n")

            #if rx.get_init_done() != "0b1" or rx.get_rxprbslocked() != "0b1":
            #    i = 0

            #    while i < 4 and (rx.get_init_done() != "0b1" or rx.get_rxprbslocked() != "0b1"):
            #        rx.reset(wait_time = 2)
            #        rx.reset_errors(wait_time = 2)
            #        i += 1
            #    
            #    log.write("Reset MGT and error count %s additional times.\n"%i)
 
            #CHECK_RX_PRBS_LOCKED
            log.write("Value of rxprbslocked: %s\n"%rx.get_rxprbslocked())

            #INIT_DONE
            log.write("Value of init_done: %s\n"%rx.get_init_done())

            #CHECK_RX_PRBSERR_FLG
            log.write("Value of rxprbserr_flg: %s\n"%rx.get_error_flag())

            #CHECK_ERROR_COUNT
            #log.write("Value of error count: %s\n"%rx.get_error_count())
            log.write("Value of error count: %s\n"%rx["rx_count_err"]["rxprbserr_cnt"].read())


def single_command(device, channel, command, arg = ""):

    log.write((70*"-") + "\nDevice: %s\nChannel: %s\n"%(device, channel))

    if command == "mgt_reset":
        log.write("Value of reset_master throughout reset: " +
                  "%s\n"%(device[channel].reset(wait_time = arg),))

    elif command == "set_txprbssel":
        log.write("Value of txprbssel after writing value %s: " +
                   "%s\n"%(arg, device[channel].set_txprbssel(arg)))

    elif command == "set_pre-emphasis":
        log.write("pre-emphasis: %s\n"%device[channel]["tx_setup"]["pre-emphasis"].write(arg))

    elif command == "set_post-emphasis":
        log.write("post-emphasis: %s\n"%device[channel]["tx_setup"]["post-emphasis"].write(arg))

    elif command == "set_swing":
            log.write("swing: %s\n"%device[channel]["tx_setup"]["swing"].write(arg))
 
    elif command == "get_init_done":
            log.write("Value of init_done: %s\n"%device[channel].get_init_done())


    elif command == "inject_error":
            log.write("Value of txprbsforceerr throughout injection: " +
                      "%s\n"%(device[channel].inject_error(),))





